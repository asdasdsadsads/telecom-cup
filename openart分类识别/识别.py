import sensor, image, time, os, tf
from machine import SPI, Pin

sensor.reset()  # Reset and initialize the sensor.
sensor.set_pixformat(sensor.RGB565)  # Set pixel format to RGB565 (or GRAYSCALE)
sensor.set_framesize(sensor.QVGA)  # Set frame size to QVGA (320x240)
sensor.skip_frames(time=2000)  # Let the camera adjust.

netAll = tf.load("all.tflite", load_to_fb=True)
labelsAll = ["animal", "apriltag", "fruit", "num"]
netFruit = tf.load("fruit.tflite", load_to_fb=True)
labelsFruit = ["apple", "banana", "durian", "grape", "orange"]
netAnimal = tf.load("animal.tflite", load_to_fb=True)
labelsAnimal = ["cat", "cow", "dog", "horse", "pig"]
Roi = (80, 40, 110, 110)
spi = SPI(30)
spi.init(1000000, 0, 0, 8, SPI.MSB)
cs = Pin(("B3", 3))
cs.init(Pin.OUT_PP, Pin.PULL_NONE)
clock = time.clock()


def write(data):
    cs.value(0)
    spi.write(data)
    cs.value(1)


def read():
    cs.value(0)
    buf = spi.read(1)
    cs.value(1)
    return buf


def readwrite(writebuf, num):
    cs.value(0)
    readbuf = bytearray(num)
    spi.write_readinto(writebuf, readbuf)
    cs.value(1)
    return readbuf


def isfruit(img):
    result = netFruit.classify(img, roi=Roi)[0].output()
    max_score = max(result)
    max_label = result.index(max_score)
    if max_label == 0:
        print("apple")
    elif max_label == 1:
        print("banana")
    elif max_label == 2:
        print("durain")
    elif max_label == 3:
        print("grape")
    elif max_label == 4:
        print("orange")


def isanimal(img):
    result = netFruit.classify(img, roi=Roi)[0].output()
    max_score = max(result)
    max_label = result.index(max_score)
    if max_label == 0:
        print("cat")
    elif max_label == 1:
        print("cow")
    elif max_label == 2:
        print("dog")
    elif max_label == 3:
        print("horse")
    elif max_label == 4:
        print("pig")


def start():
    result = netAll.classify(img, roi=Roi)[0].output()
    max_score = max(result)
    max_label = result.index(max_score)
    if max_label == 0:
        isanimal(img)
    elif max_label == 1:
        print("apriltag")
    elif max_label == 2:
        isfruit(img)
    elif max_label == 3:
        print("num")


def isblack(rgb):
    if rgb[0] < 0x33 and rgb[1] < 0x33 and rgb[2] < 0x33:
        return 1
    else:
        return 0


while True:
    clock.tick()
    count = 0
    img = sensor.snapshot()
    img.draw_rectangle(Roi, color=(255, 0, 0))
    start()
    for i in range(img.width()):
        b = img.get_pixel(i,int(img.height()-30))
        if (isblack(b)==1):
            count = count + 1
        else:
            count = 0
    if count > img.width() - 30:
        print("OK")

    # print(clock.fps(), "fps")

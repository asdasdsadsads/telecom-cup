import os, tf
import image
from re_spi import readwrite

net_path = "project0319.tflite"     # 定义模型的路径
#labels = [line.rstrip() for line in open("/sd/labels_animal_fruits.txt")]   # 加载标签
net = tf.load(net_path, load_to_fb=True)                                  # 加载模型

yellow_bright = (35, 100, -38, -13, 46, 127) # 黄色阈值
yellow_middle = (35, 100,   1, -49, 41, 127)
yellow_dark   = (39, 100, -48,  46, 19, 127)
#bright_code = 1 # code = 2^0 = 1
middle_code = 1
#dark_code = 4

pixel = 30.5/240 # QVGA下是240
mode_flag = 0
last_label = 0
label_count = 0

def send_data(x,y):
    x = (x-160)*pixel #以上方的摄像头视野为准
    y = (120-y)*pixel

    sendbuff = [0x56,0x64,int(x),int(y),0x55]
    receive = int.from_bytes(readwrite(bytearray(sendbuff),5),"big")

    print((int(x),int(y)))
    return receive

def send_class(max_label,score):
    string = 0
    global mode_flag,last_label,label_count

    if max_label == 1:
        string = "cat"
    elif max_label == 2:
        string = "cow"
    elif max_label == 3:
        string = "dog"
    elif max_label == 4:
        string = "horse"
    elif max_label == 5:
        string = "pig"
    elif max_label == 6:
        string = "apple"
    elif max_label == 7:
        string = "banana"
    elif max_label == 8:
        string = "durain"
    elif max_label == 9:
        string = "grape"
    elif max_label == 10:
        string = "orange"
    elif max_label == 11:
        string = "bus"
    elif max_label == 12:
        string = "car"
    elif max_label == 13:
        string = "plane"
    elif max_label == 14:
        string = "ship"
    elif max_label == 15:
        string = "train"

    #if max_label == last_label:
        #label_count += 1
    #else: label_count = 0
    #last_label = max_label

    while label_count>3:
        sendbuff = [0x33,0x44,0x55,max_label,0xEE]
        receive = int.from_bytes(readwrite(bytearray(sendbuff),5),"big")
        if receive == 1:
            mode_flag = 0
            last_label = 0
            label_count = 0
    return string

def Blob(img):
    for blob in img.find_blobs([yellow_middle],area_threshold=4000):
        if blob[8] == middle_code and blob[2]-blob[3]>-10 and blob[2]-blob[3]<10:
            x = blob[0]+10
            y = blob[1]+10
            width = blob[2]-20 # 色块矩形的宽度
            height = blob[3]-20 # 色块矩形的高度
            img.draw_rectangle([x, y, width, height], color = (255, 0, 0))

            global mode_flag

            if mode_flag == 0: #debug so it is 0 ,real situation is 1
                return [x, y, width, height]

            if mode_flag == 0:
                receive = send_data(blob.cx(),blob.cy())
                if receive == 2:
                    mode_flag = 1

    return 0


def Sorting(img,roi):
    result = net.classify(img, roi=roi)[0].output()
    max_score = max(result)
    max_label = result.index(max_score)+1
    string= send_class(max_label,max_score)
    print(string)
    img.draw_string(20,20,string+":%s"%max_score,\
        color=(0,255,255),scale = 3,mono_space=False)


#def Sorting(img,img1):
    #for obj in tf.classify(net , img1, min_scale=1.0, scale_mul=0.5, x_overlap=0.0, y_overlap=0.0):
        #sorted_list = sorted(zip(labels, obj.output()), key = lambda x: x[1], reverse = True)
        #img.draw_string(20,20,str(sorted_list[0][0])+str(":")+str(sorted_list[0][1]),\
        #color=(0,255,255),scale = 3,mono_space=False)
    #return 0

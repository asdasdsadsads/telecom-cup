import mtx
from re_spi import readwrite
import image

a = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,\
     0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0] #可存储20个坐标+1个0
timecount = 0

# 坐标换算为实际路程 #
def comple(x):
    if (x + 10) % 20 > 9 : return int((int(x / 10 + 1) + 1) / 2)
    else: return int((int(x / 10) + 1) / 2)

#点集换算为坐标#
def transformPoints(r,x,y):
    denominator = r[6] * x + r[7] * y + 1
    transform_x = int((r[0] * x + r[1] * y + r[2]) / denominator)
    transform_y = 500-int((r[3] * x + r[4] * y + r[5]) / denominator)
    #print((comple(transform_x),comple(transform_y)))
    #print((transform_x,transform_y))
    return comple(transform_x),comple(transform_y)

# 发送并接收数据 #
def send_and_receive(j):
    count = 0
    flag = 0
    # stop = 0
    while count != 21*2:
        if count > 1 and count < 41:
            sendbuff=[int(count/2),0x66,a[count-2],a[count-1],0x77,0x88]
            #readwrite(bytearray(sendbuff),6)
            flag = 1
            #if count > 7:
            print(count)
        if flag == 0:
            sendbuff=[int(j/2),0x22,0xAA,0xBB,0x33,0x44]
        count = 2*int.from_bytes(readwrite(bytearray(sendbuff),6), 'big')
        #print(count)
    return 1

# 对图像作透视变换 #
def transformer(x,y,x1,y1,x2,y2,x3,y3,x4,y4):
    Array =[[x1,y1, 1,0, 0, 0, 0,    0],
        [x2,y2, 1,0, 0, 0, -x*x2,-x*y2],
        [x3,y3, 1,0, 0, 0, 0,    0],
        [x4,y4, 1,0, 0, 0, -x*x4,-x*y4],
        [0, 0,  0,x1,y1,1, 0,    0],
        [0, 0,  0,x2,y2,1, 0,    0],
        [0, 0,  0,x3,y3,1, -y*x3,-y*y3],
        [0, 0,  0,x4,y4,1, -y*x4,-y*y4]]

    Aim = [0,x,0,x,0,0,y,y]
    A_factored = mtx.lu(Array)

    return mtx.solve(A_factored, Aim) #返回三阶方阵的前八位

# 坐标主任务 #
def get_coordinate(img):
    corn = [0,0,0,0,0,0,0,0]
    global a
    global timecount
    for r in img.find_rects(threshold = 40000):
        roi=r.rect()
        i = 0
        j = 0
        for p in r.corners():
            # img.draw_circle(p[0], p[1], 5, color = (0, 255, 0))
            corn[i]=p[0]
            corn[i+1]=p[1]
            i+=2
            #print(p[0],p[1]) #debug
        arr = transformer(700,500,corn[6],corn[7],corn[4],corn[5],corn[0],corn[1],corn[2],corn[3])
        print(arr)
        for c in img.find_circles(roi = roi,threshold = 3000,r_max=10):
            img.draw_rectangle(roi, color = (255, 0, 0))
            img.draw_circle(c.circle(),color=(200,0,255))
            img.draw_cross(c.x(),c.y(),color=(200,255,0))
            a[j] = c.x()
            a[j+1] = c.y()
            j += 2

        if j/2 > 5: #检测到n个以上圆开始传输数据，此时n=5，实际比赛时为5
            timecount += 1
            if timecount > 2:
                j = 0
                while a[j] != 0:
                    a[j] , a[j+1] = transformPoints(arr,a[j],a[j+1])
                    print((a[j],a[j+1]))
                    j += 2
                return send_and_receive(j)
    return 0

import sensor, image, time, math
from machine import SPI,Pin
from pyb import LED
import os, tf
import Coordinate
import Sort

sensor.reset()
sensor.set_pixformat(sensor.RGB565)
sensor.set_framesize(sensor.QVGA)
sensor.set_brightness(1000)
sensor.skip_frames(time = 50)

clock = time.clock()
light = LED(4)
light.on()

# 坐标任务 #
while True:
    clock.tick()
    img = sensor.snapshot()
    if Coordinate.get_coordinate(img):
        break
    print(clock.fps())

light.off()

# 识别任务 #
while True:
    clock.tick()
    img = sensor.snapshot()
    img1 = Sort.Blob(img)
    if img1:
        Sort.Sorting(img,img1)
    #print(clock.fps())
    img.draw_string(280,200,str(int(clock.fps())),\
    color=(0,255,0),scale = 3,mono_space=False)
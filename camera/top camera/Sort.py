import os, tf
import image
from re_spi import readwrite

net_path = "latest0319.tflite"                                  # 定义模型的路径
labels = [line.rstrip() for line in open("/sd/labels_animal_fruits.txt")]   # 加载标签
net = tf.load(net_path, load_to_fb=True)                                  # 加载模型

yellow_bright = (35, 100, -38, -13, 46, 127) # 黄色阈值
yellow_middle = (35, 100,   1, -49, 41, 127)
yellow_dark   = (39, 100, -48,  46, 19, 127)
#bright_code = 1 # code = 2^0 = 1
middle_code = 1
#dark_code = 4
pixel = 30.5/240 # QVGA下是240

def send_data(x,y):
    x = (x-160)*pixel #以上方的摄像头视野为准
    y = (120-y)*pixel
    readwrite(bytearray([0x56,0x64,int(x),int(y),0x55]),5)
    print((int(x),int(y)))
    return 0

def Blob(img):
    blobs = img.find_blobs([yellow_middle],area_threshold=4000)
    if blobs:
        for blob in blobs:
            if blob[8] == middle_code and blob[2]-blob[3]>-10 and blob[2]-blob[3]<10:
                x = blob[0]+10
                y = blob[1]+10
                width = blob[2]-20 # 色块矩形的宽度
                height = blob[3]-20 # 色块矩形的高度
                #if blob.density()<0.5:
                img.draw_rectangle([x, y, width, height], color = (255, 0, 0))
                if abs(160-blob.cx())<40:
                    img1 = img.copy([x, y, width, height])
                    return img1
                send_data(blob.cx(),blob.cy())
    return 0

def Sorting(img,img1):
    for obj in tf.classify(net , img1, min_scale=1.0, scale_mul=0.5, x_overlap=0.0, y_overlap=0.0):
        sorted_list = sorted(zip(labels, obj.output()), key = lambda x: x[1], reverse = True)
        img.draw_string(20,20,str(sorted_list[0][0])+str(":")+str(sorted_list[0][1]),\
        color=(0,255,255),scale = 3,mono_space=False)
        #for i in range(1):
            #print("%s = %f" % (sorted_list[i][0], sorted_list[i][1]))# 打印准确率最高的结果
    return 0
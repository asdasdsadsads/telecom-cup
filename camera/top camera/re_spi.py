from machine import SPI,Pin

spi = SPI(30)
spi.init(1000000,0,0,8,SPI.MSB)
cs = Pin(("B3",3))# Create a clock object to track the FPS.
cs.init(Pin.OUT_PP,Pin.PULL_NONE)

def write(data):
    cs.value(0)
    spi.write(data)
    cs.value(1)

def read():
    cs.value(0)
    buf = spi.read(1)
    cs.value(1)
    return buf
    
def readwrite(writebuf,num):
    cs.value(0)
    readbuf = bytearray(num)
    spi.write_readinto(writebuf,readbuf)
    cs.value(1)
    return readbuf
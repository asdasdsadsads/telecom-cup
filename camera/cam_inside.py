import sensor, image, time
from machine import UART

sensor.reset()
sensor.set_pixformat(sensor.GRAYSCALE) # grayscale is faster (160x120 max on OpenMV-M7)
sensor.set_framesize(sensor.QQVGA)
sensor.skip_frames(time = 2000)
clock = time.clock()
uart = UART(1, baudrate=115200)

dark_threshold = (172, 255)

def send_data(x,y):
    trans_x = int((x-80)/4)
    trans_y = int((40-y)/4)
    print((trans_x,trans_y))
    trans = [0x56,0x64,trans_x,trans_y,0x55]
    #debug = [0x56,0x64,0x11,0x11,0x55,num]
    #readwrite(bytearray([0x03,0x11,trans_x,trans_y,0x55]),5)
    uart.write(bytearray(trans))

def find_target(roi):
    for r in img.find_rects(roi = roi,threshold = 10000):
        # if abs(r.w() - r.h())<40:
        img.draw_rectangle(r.rect(), color = (255, 0, 0))
        send_data(r.x()+r.w()/2,r.y()+r.h()/2)
        return 0
    return 1

while(True):
    clock.tick()
    img = sensor.snapshot()
    img.binary([dark_threshold])
    flag = 1
    # if flag:
    flag = find_target(roi = [0,0,50,120])
    # if flag:
    flag = find_target(roi = [80,0,50,120])
    # if flag:
    flag = find_target(roi = [0,0,160,50])

    #for p in r.corners(): img.draw_circle(p[0], p[1], 5, color = (0, 255, 0))
    #print(r)
    print("FPS %f" % clock.fps())

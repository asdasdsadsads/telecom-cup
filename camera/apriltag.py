import sensor, image, time, math
from machine import SPI,Pin
spi = SPI(30)
spi.init(1000000, 0, 0, 8, SPI.MSB)
cs = Pin(("B3", 3))
cs.init(Pin.OUT_PP, Pin.PULL_NONE)

def write(data):
    cs.value(0)
    spi.write(data)
    cs.value(1)

def read():
    cs.value(0)
    buf = spi.read(1)
    cs.value(1)
    return buf

def readwrite(writebuf,num):
    cs.value(0)
    readbuf = bytearray(num)
    spi.write_readinto(writebuf,readbuf)
    cs.value(1)
    return readbuf

sensor.reset()
sensor.set_pixformat(sensor.RGB565)
sensor.set_framesize(sensor.VGA) # we run out of memory if the resolution is much bigger...
sensor.set_windowing((160,120))
sensor.skip_frames(time=1000)
sensor.set_auto_gain(False)  # must turn this off to prevent image washout...
sensor.set_auto_whitebal(False)  # must turn this off to prevent image washout...
clock = time.clock()

# 注意！与find_qrcodes不同，find_apriltags 不需要软件矫正畸变就可以工作。
# 注意，输出的姿态的单位是弧度，可以转换成角度，但是位置的单位是和你的大小有关，需要等比例换算
# f_x 是x的像素为单位的焦距。对于标准的OpenMV，应该等于2.8/3.984*656，这个值是用毫米为单位的焦距除以x方向的感光元件的长度，乘以x方向的感光元件的像素（OV7725）
# f_y 是y的像素为单位的焦距。对于标准的OpenMV，应该等于2.8/2.952*488，这个值是用毫米为单位的焦距除以y方向的感光元件的长度，乘以y方向的感光元件的像素（OV7725）
# c_x 是图像的x中心位置
# c_y 是图像的y中心位置

f_x = (2.8 / 3.984) * 640 # 默认值
f_y = (2.8 / 2.952) * 480 # 默认值
c_x = 160 * 0.5 # 默认值(image.w * 0.5)
c_y = 120 * 0.5 # 默认值(image.h * 0.5)
last_fz = 0
i = 0

def comple(x):
    if (x + 10) % 20 > 9 : return int((int(x / 10 + 1) - 1) / 2)
    else: return int((int(x / 10) - 1) / 2)

def degrees(radians):
    return (180 * radians) / math.pi

while(True):
    img = sensor.snapshot()
    fn = 10
    fx = 0
    fz = 0
    for tag in img.find_apriltags(fx=f_x, fy=f_y, cx=c_x, cy=c_y,families=image.TAG25H9): # 默认为TAG36H11
        z=tag.z_translation()*(-5.34)+2-5.75*math.sin(tag.y_rotation())*tag.z_translation()/11.9
        y=tag.y_translation()
        x=z/241.7*(80-tag.cx()) #241.7是测量的焦距，初步调整参数

        if c_x - tag.cx() > -10 and c_x - tag.cx() < 10:
            img.draw_rectangle(tag.rect(), color = (255, 0, 0))
            img.draw_cross(tag.cx(), tag.cy(), color = (0, 255, 0))
            print_args = (x, y, z, \
                degrees(tag.x_rotation()), degrees(tag.y_rotation()), degrees(tag.z_rotation()))# 位置的单位是未知的，旋转的单位是角度

            #print("Tx: %f, Ty %d, Tz %f, Rx %f, Ry %f, Rz %f" % print_args)
            #print("id is %d" % tag.id())
            fz=z*math.cos(tag.y_rotation())#+x*math.sin(tag.y_rotation())
            fx=z*math.sin(tag.y_rotation())-x*math.cos(tag.y_rotation()) #根据tag姿态换算
            fn=tag.id()

            img.draw_string(tag.x(),10,str(int(z)),color=(0,200,255),scale = 2,mono_space=False)
            coon=(fx,fz,fn,degrees(tag.y_rotation()))
            print("相对位置(%d,%d),序号(%d),角度:%d" % coon)
            #if int(fz) ==last_fz: i+=1
            #else : i = 0
            #if i > 0 :  print(comple(fz)); i = 0
            #last_fz = int(fz)
            readwrite(bytearray([0x11,0x22,char(int(fx)>>8),int(fx>>8),int(fz),int(fn),0x66]),6)
    #print(clock.fps())




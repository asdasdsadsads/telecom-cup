#include "headfile.h"
#include "font.h"
#include "isr.h"

//大部分的宏定义放在了define.h文件中

int pointsarray[20][2]={{3,3},{30,20},{15,16},{24,5},{6,20},{12,19},{15,10},{25,12}};
//int pointsarray[18][2]={{34,2},{32,15},{31,8},{30,20},{30,3},{25,12},{24,5},{20,20},{20,14},{15,16},{15,10},{12,19},{11,6},{10,10},{6,20},{5,13},{3,22},{3,3}};
//int pointsarray[20][2]={{30,20},{15,16},{24,5},{6,20},{12,19},{15,10},{25,12},{11,11},{21,2},{12,31},{12,41},{6,15},{34,11},{12,4},{43,12},{12,12},{32,4},{24,16}};
int main(void)
{
    DisableGlobalIRQ();
    board_init();   //务必保留，本函数用于初始化MPU 时钟 调试串口
	#ifdef NewMainBoard
		gpio_init(D26,GPO,1,GPIO_PIN_CONFIG);//外设开关初始化
//		spi1_slave_init(B21,B14,B15,B16);//与摄像头通信的spi1,CS为B21的通道初始化
//		spi1_slave_init(C20,C22,C21,C23);//与底部摄像头通信的spi1,CS为C8的通道初始化
//		NVIC_SetPriority(GPIO1_Combined_16_31_IRQn,6); //设置顶部摄像头通信的中断优先级
//		NVIC_SetPriority(GPIO2_Combined_0_15_IRQn,6); //设置底部摄像头通信的中断优先级
		gpio_init(D4,GPI,1,GPIO_PIN_CONFIG);//驱动使能
		gpio_init(C14,GPO,0,GPIO_PIN_CONFIG);//电磁铁2初始化
		gpio_init(C18,GPO,0,GPIO_PIN_CONFIG);//电磁铁3初始化
	#else
		gpio_init(C4,GPO,1,GPIO_PIN_CONFIG);//外设开关初始化
		spi1_slave_init(C17,C28,C29,C16);//与摄像头通信的spi1,CS为C27的通道初始化
		spi1_slave_init(C27,C20,C21,C22);//与摄像头通信的spi1,CS为C27的通道初始化	
		NVIC_SetPriority(GPIO2_Combined_16_31_IRQn,6); //设置与摄像头通信的中断优先级
	#endif	
    motol_init();//电机初始化
		steer_init();//舵机初始化
		oled_init();
		oled_print_chinese(3,3,16,*font1,7);//正在校准陀螺仪
	#ifdef ICM20602
				icm20602_init_spi();
	#else
				mpu6050_init_hardware();
	#endif	
		ins_calibration();//陀螺仪校准（需水平放置1s）
		final_points = (Target_points*)malloc(sizeof(Target_points) * (21));
		gpio_init(B9,GPO,1,GPIO_PIN_CONFIG);//led初始化
		gpio_init(C15,GPO,0,GPIO_PIN_CONFIG);//电磁铁1初始化
		oled_fill(0);
		pit_init();//定时器中断初始化

//		turn_Angle(thirdsteer,90,180);//舵机三归位
//		turn_Angle(firststeer,220,270);//舵机一归位		C30
//		turn_Angle(secondsteer,128,270);//舵机二归位		C31
		Target_points *points;	
//调试专区记得删去
		communication_Init();
//		PID_FORWORD_INNER_TEXT();
		pit_interrupt_ms(PIT_CH2,100);//串口数据初始化
		pit_interrupt_ms(PIT_CH1,10);//icm20602的定时器中断，如果没有接icm20602就开启定时器的话会导致mcu锁死
		pit_interrupt_ms(PIT_CH0,5);//电机的编码器计数+启动中断初始化	
//		turn_angle(180);
		NVIC_SetPriority(PIT_IRQn,7); //设置定时器中断通信的中断优先级7
		Ready = true;	
//		pointsNum = 8; 
		SmallClass = 0;
		gpio_init(C29,GPI,0,GPIO_PIN_CONFIG);
		systick_delay_ms(100);
		EnableGlobalIRQ(0);
		char str1[10] = "";  
		char str2[20] = "";  
    while(1)
    {	
			if(Ready&&!Task_finish&&SmallClass!=0&&SmallClass!=15)
			{
				switch(SmallClass)
				{
					case 1:	{sprintf(str1,"orange",SmallClass);}
					case 2: {sprintf(str1,"apple",SmallClass);}
					case 3: {sprintf(str1,"banana",SmallClass);}
					case 4: {sprintf(str1,"grape",SmallClass);}
				}
				while(gpio_get(C29));
				Top_camera_sendData(2);systick_delay_ms(50);
				Top_camera_sendData(2);systick_delay_ms(50);
				switch(SmallClass)
				{
					case 1:{
								time_Stop = false;
								oled_p8x16str(1,2,str1);
								oled_p8x16str(1,0,"transit");
								int distance = Centimeter_to_encoder(75);
								motol_Settarget_point_move(0,distance,100);
								while(!go_End);
								turn_angle(90);
								while(turn);
						
								distance = Centimeter_to_encoder(55);
								motol_Settarget_point_move(0,distance,100);
								while(!go_End);
								
								oled_fill(0);
								oled_p8x16str(1,2,str1);
								oled_p8x16str(1,0,"wait uninstall");
								sprintf(str2,"time = %5d",time1);
								oled_p8x16str(1,4,str2);	
						
								Top_camera_sendData(1);systick_delay_ms(50);
								Top_camera_sendData(1);systick_delay_ms(50);
								time_Stop = true;
								while(!gpio_get(C29))
									systick_delay_ms(50);
								turn_angle(-90);
								time_Stop = false;
								Top_camera_sendData(2);systick_delay_ms(50);
								Top_camera_sendData(2);systick_delay_ms(50);
								oled_fill(0);
								oled_p8x16str(1,4,str2);	
								oled_p8x16str(1,2,str1);
								oled_p8x16str(1,0,"transit");
								
								while(turn);
								distance = Centimeter_to_encoder(55);
								motol_Settarget_point_move(0,distance,100);
								while(!go_End);
								turn_angle(182);
								while(turn);
								
								distance = Centimeter_to_encoder(75);
								motol_Settarget_point_move(0,distance,100);
								while(!go_End);
								turn_angle(0);
								while(turn);
								
								sprintf(str2,"time = %5d",time1);
								oled_fill(0);
								oled_p8x16str(1,2,str1);
								oled_p8x16str(1,4,str2);
								oled_p8x16str(1,0,"finish");
								time_Stop = true;
								Task_finish = true;
								gpio_set(D4,0);
								break;
							}
					case 2:
							{
								time_Stop = false;
								oled_p8x16str(1,2,str1);
								oled_p8x16str(1,0,"transit");
								int distance = Centimeter_to_encoder(75);
								motol_Settarget_point_move(0,distance,100);
								while(!go_End);
								turn_angle(-90);
								while(turn);
						
								distance = Centimeter_to_encoder(55);
								motol_Settarget_point_move(0,distance,100);
								while(!go_End);
								
								oled_fill(0);
								oled_p8x16str(1,2,str1);
								oled_p8x16str(1,0,"wait uninstall");
								sprintf(str2,"time = %5d",time1);
								oled_p8x16str(1,4,str2);	
						
								Top_camera_sendData(1);systick_delay_ms(50);
								Top_camera_sendData(1);systick_delay_ms(50);
								time_Stop = true;
								while(!gpio_get(C29))
									systick_delay_ms(50);
								turn_angle(90);
								time_Stop = false;
								Top_camera_sendData(2);systick_delay_ms(50);
								Top_camera_sendData(2);systick_delay_ms(50);
								oled_fill(0);
								oled_p8x16str(1,4,str2);	
								oled_p8x16str(1,2,str1);
								oled_p8x16str(1,0,"transit");
								
								while(turn);
								distance = Centimeter_to_encoder(55);
								motol_Settarget_point_move(0,distance,100);
								while(!go_End);
								turn_angle(182);
								while(turn);
								
								distance = Centimeter_to_encoder(75);
								motol_Settarget_point_move(0,distance,100);
								while(!go_End);
								turn_angle(0);
								while(turn);
								
								sprintf(str2,"time = %5d",time1);
								oled_fill(0);
								oled_p8x16str(1,2,str1);
								oled_p8x16str(1,4,str2);
								oled_p8x16str(1,0,"finish");
								time_Stop = true;
								Task_finish = true;
								gpio_set(D4,0);
								break;
							}
					case 3:
					{
								time_Stop = false;
								oled_p8x16str(1,2,str1);
								oled_p8x16str(1,0,"transit");
								int distance = Centimeter_to_encoder(175);
								motol_Settarget_point_move(0,distance,100);
								while(!go_End);
								turn_angle(90);
								while(turn);
						
								distance = Centimeter_to_encoder(55);
								motol_Settarget_point_move(0,distance,100);
								while(!go_End);
								
								oled_fill(0);
								oled_p8x16str(1,2,str1);
								oled_p8x16str(1,0,"wait uninstall");
								sprintf(str2,"time = %5d",time1);
								oled_p8x16str(1,4,str2);	
						
								Top_camera_sendData(1);systick_delay_ms(50);
								Top_camera_sendData(1);systick_delay_ms(50);
								time_Stop = true;
								while(!gpio_get(C29))
									systick_delay_ms(50);
								turn_angle(-90);
								time_Stop = false;
								Top_camera_sendData(2);systick_delay_ms(50);
								Top_camera_sendData(2);systick_delay_ms(50);
								oled_fill(0);
								oled_p8x16str(1,4,str2);	
								oled_p8x16str(1,2,str1);
								oled_p8x16str(1,0,"transit");
								
								while(turn);
								distance = Centimeter_to_encoder(55);
								motol_Settarget_point_move(0,distance,100);
								while(!go_End);
								turn_angle(182);
								while(turn);
								
								distance = Centimeter_to_encoder(175);
								motol_Settarget_point_move(0,distance,100);
								while(!go_End);
								turn_angle(0);
								while(turn);
								
								sprintf(str2,"time = %5d",time1);
								oled_fill(0);
								oled_p8x16str(1,2,str1);
								oled_p8x16str(1,4,str2);
								oled_p8x16str(1,0,"finish");
								time_Stop = true;
								Task_finish = true;
								gpio_set(D4,0);
								break;
					}
					
					case 4:
					{
								time_Stop = false;
								oled_p8x16str(1,2,str1);
								oled_p8x16str(1,0,"transit");
								int distance = Centimeter_to_encoder(175);
								motol_Settarget_point_move(0,distance,100);
								while(!go_End);
								turn_angle(-90);
								while(turn);
						
								distance = Centimeter_to_encoder(55);
								motol_Settarget_point_move(0,distance,100);
								while(!go_End);
								
								oled_fill(0);
								oled_p8x16str(1,2,str1);
								oled_p8x16str(1,0,"wait uninstall");
								sprintf(str2,"time = %5d",time1);
								oled_p8x16str(1,4,str2);	
						
								Top_camera_sendData(1);systick_delay_ms(50);
								Top_camera_sendData(1);systick_delay_ms(50);
								time_Stop = true;
								while(!gpio_get(C29))
									systick_delay_ms(50);
								turn_angle(90);
								time_Stop = false;
								Top_camera_sendData(2);systick_delay_ms(50);
								Top_camera_sendData(2);systick_delay_ms(50);
								oled_fill(0);
								oled_p8x16str(1,4,str2);	
								oled_p8x16str(1,2,str1);
								oled_p8x16str(1,0,"transit");
								
								while(turn);
								distance = Centimeter_to_encoder(55);
								motol_Settarget_point_move(0,distance,100);
								while(!go_End);
								turn_angle(182);
								while(turn);
								
								distance = Centimeter_to_encoder(175);
								motol_Settarget_point_move(0,distance,100);
								while(!go_End);
								turn_angle(0);
								while(turn);
								
								sprintf(str2,"time = %5d",time1);
								oled_fill(0);
								oled_p8x16str(1,2,str1);
								oled_p8x16str(1,4,str2);
								oled_p8x16str(1,0,"finish");
								time_Stop = true;
								Task_finish = true;
								gpio_set(D4,0);
								break;
					}
						}
			}		
		}   
}

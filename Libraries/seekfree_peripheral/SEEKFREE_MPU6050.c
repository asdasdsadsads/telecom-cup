/*********************************************************************************************************************
 * COPYRIGHT NOTICE
 * Copyright (c) 2018,逐飞科技
 * All rights reserved.
 * 技术讨论QQ群：一群：179029047(已满)  二群：244861897
 *
 * 以下所有内容版权均属逐飞科技所有，未经允许不得用于商业用途，
 * 欢迎各位使用并传播本程序，修改内容时必须保留逐飞科技的版权声明。
 *
 * @file       		MPU6050
 * @company	   		成都逐飞科技有限公司
 * @author     		逐飞科技(QQ3184284598)
 * @version    		查看doc内version文件 版本说明
 * @Software 		IAR 8.3 or MDK 5.28
 * @Taobao   		https://seekfree.taobao.com/
 * @date       		2019-04-30
 * @note		
					接线定义：
					------------------------------------ 
						软件IIC
                        SCL                 查看SEEKFREE_IIC文件内的SEEKFREE_SCL宏定义
						SDA                 查看SEEKFREE_IIC文件内的SEEKFREE_SDA宏定义
                            
                        硬件IIC
                        SCL                 查看init_mpu6050_hardware函数内IIC初始化时所带参数
						SDA                 查看init_mpu6050_hardware函数内IIC初始化时所带参数    
					------------------------------------ 
 ********************************************************************************************************************/

#include "zf_iic.h"
#include "zf_systick.h"
#include "SEEKFREE_IIC.h"
#include "SEEKFREE_MPU6050.h"
#include "headfile.h"

int16 mpu_gyro_x,mpu_gyro_y,mpu_gyro_z;
int16 mpu_acc_x,mpu_acc_y,mpu_acc_z;
int16 mpu_mag_x,mpu_mag_y,mpu_mag_z;

float  mag_sensadj[3] = {1, 1, 1};



//-------------------------------------------------------------------------------------------------------------------
//  @brief      MPU6050自检函数
//  @param      NULL
//  @return     void					
//  @since      v1.0
//  Sample usage:				调用该函数前，请先调用模拟IIC的初始化
//-------------------------------------------------------------------------------------------------------------------
void mpu6050_self1_check(void)
{
    simiic_write_reg(MPU6050_DEV_ADDR, PWR_MGMT_1, 0x00);	//解除休眠状态
    simiic_write_reg(MPU6050_DEV_ADDR, SMPLRT_DIV, 0x07);   //125HZ采样率
		uint8 id = simiic_read_reg(MPU6050_DEV_ADDR, SMPLRT_DIV,SIMIIC);
    while(0x07 != id)
    {
        //卡在这里原因有以下几点
        //1 MPU6050坏了，如果是新的这样的概率极低
        //2 接线错误或者没有接好
        //3 可能你需要外接上拉电阻，上拉到3.3V
		//4 可能没有调用模拟IIC的初始化函数
    }
}


//-------------------------------------------------------------------------------------------------------------------
//  @brief      初始化MPU6050
//  @param      NULL
//  @return     void					
//  @since      v1.0
//  Sample usage:				调用该函数前，请先调用模拟IIC的初始化
//-------------------------------------------------------------------------------------------------------------------
void mpu6050_init(void)
{
    systick_delay_ms(100);                                   //上电延时

    mpu6050_self1_check();
    simiic_write_reg(MPU6050_DEV_ADDR, PWR_MGMT_1, 0x00);	//解除休眠状态
    simiic_write_reg(MPU6050_DEV_ADDR, SMPLRT_DIV, 0x07);   //125HZ采样率
    simiic_write_reg(MPU6050_DEV_ADDR, MPU6050_CONFIG, 0x04);       //
    simiic_write_reg(MPU6050_DEV_ADDR, GYRO_CONFIG, 0x18);  //2000
    simiic_write_reg(MPU6050_DEV_ADDR, ACCEL_CONFIG, 0x10); //8g
		simiic_write_reg(MPU6050_DEV_ADDR, User_Control, 0x00);
    simiic_write_reg(MPU6050_DEV_ADDR, INT_PIN_CFG, 0x02);
}



//-------------------------------------------------------------------------------------------------------------------
//  @brief      获取MPU6050加速度计数据
//  @param      NULL
//  @return     void
//  @since      v1.0
//  Sample usage:				执行该函数后，直接查看对应的变量即可
//-------------------------------------------------------------------------------------------------------------------
void get_accdata(void)
{
    uint8 dat[6];

    simiic_read_regs(MPU6050_DEV_ADDR, ACCEL_XOUT_H, dat, 6, SIMIIC);  
    mpu_acc_x = (int16)(((uint16)dat[0]<<8 | dat[1]));
    mpu_acc_y = (int16)(((uint16)dat[2]<<8 | dat[3]));
    mpu_acc_z = (int16)(((uint16)dat[4]<<8 | dat[5]));
}


//-------------------------------------------------------------------------------------------------------------------
//  @brief      获取MPU6050陀螺仪数据
//  @param      NULL
//  @return     void
//  @since      v1.0
//  Sample usage:				执行该函数后，直接查看对应的变量即可
//-------------------------------------------------------------------------------------------------------------------
void get_gyro(void)
{
    uint8 dat[6];

    simiic_read_regs(MPU6050_DEV_ADDR, GYRO_XOUT_H, dat, 6, SIMIIC);  
    mpu_gyro_x = (int16)(((uint16)dat[0]<<8 | dat[1]));
    mpu_gyro_y = (int16)(((uint16)dat[2]<<8 | dat[3]));
    mpu_gyro_z = (int16)(((uint16)dat[4]<<8 | dat[5]));
}

//-------------------------------------------------------------------------------------------------------------------
//  以上函数是使用软件IIC通信，相比较硬件IIC，软件IIC引脚更加灵活，可以使用任意普通IO
//-------------------------------------------------------------------------------------------------------------------





//-------------------------------------------------------------------------------------------------------------------
//  以下函数是使用硬件IIC通信，相比较软件IIC，硬件IIC速度可以做到更快。
//-------------------------------------------------------------------------------------------------------------------

#define IIC_NUM         IIC_1
#define IIC_SDA_PIN     IIC1_SDA_B17
#define IIC_SCL_PIN     IIC1_SCL_B16
//-------------------------------------------------------------------------------------------------------------------
//  @brief      MPU6050自检函数
//  @param      NULL
//  @return     void					
//  @since      v1.0
//  Sample usage:				
//-------------------------------------------------------------------------------------------------------------------
void mpu6050_self2_check(void)
{
    uint8 dat=0;
    
    iic_write_reg(IIC_NUM, MPU6050_DEV_ADDR, PWR_MGMT_1, 0x00);	//解除休眠状态
    iic_write_reg(IIC_NUM, MPU6050_DEV_ADDR, SMPLRT_DIV, 0x07); //125HZ采样率
    while(0x07 != dat)
    {
        iic_read_reg(IIC_NUM, MPU6050_DEV_ADDR, SMPLRT_DIV, &dat);
        systick_delay_ms(10);
        //卡在这里原因有以下几点
        //1 MPU6050坏了，如果是新的这样的概率极低
        //2 接线错误或者没有接好
        //3 可能你需要外接上拉电阻，上拉到3.3V
    }
}

//-------------------------------------------------------------------------------------------------------------------
//  @brief      初始化MPU6050
//  @param      NULL
//  @return     void					
//  @since      v1.0
//  Sample usage:				
//-------------------------------------------------------------------------------------------------------------------
void mpu6050_init_hardware(void)
{
    systick_delay_ms(100);                                      //上电延时
    iic_init(IIC_NUM, IIC_SDA_PIN, IIC_SCL_PIN,400*1000);       //硬件IIC初始化     波特率400K
    
    mpu6050_self2_check();
    iic_write_reg(IIC_NUM,MPU6050_DEV_ADDR, PWR_MGMT_1, 0x00);	//解除休眠状态
    iic_write_reg(IIC_NUM,MPU6050_DEV_ADDR, SMPLRT_DIV, 0x07);    //125HZ采样率
    iic_write_reg(IIC_NUM,MPU6050_DEV_ADDR, MPU6050_CONFIG, 0x04);      //
    iic_write_reg(IIC_NUM,MPU6050_DEV_ADDR, GYRO_CONFIG, 0x18);   //2000
    iic_write_reg(IIC_NUM,MPU6050_DEV_ADDR, ACCEL_CONFIG, 0x10);  //8g
//		iic_write_reg(IIC_NUM,MPU6050_DEV_ADDR, User_Control, 0x00);
//    iic_write_reg(IIC_NUM,MPU6050_DEV_ADDR, INT_PIN_CFG, 0x02);
		
		MPU92_AUX_AK8963_Init();//磁力计校准
}

//-------------------------------------------------------------------------------------------------------------------
//  @brief      获取MPU6050加速度计数据
//  @param      NULL
//  @return     void
//  @since      v1.0
//  Sample usage:				执行该函数后，直接查看对应的变量即可
//-------------------------------------------------------------------------------------------------------------------
void get_accdata_hardware(void)
{
    uint8 dat[6];
    
    iic_read_reg_bytes(IIC_NUM,MPU6050_DEV_ADDR, ACCEL_XOUT_H, dat, 6);
    mpu_acc_x = (int16)(((uint16)dat[0]<<8 | dat[1]));
    mpu_acc_y = (int16)(((uint16)dat[2]<<8 | dat[3]));
    mpu_acc_z = (int16)(((uint16)dat[4]<<8 | dat[5]));
}

//-------------------------------------------------------------------------------------------------------------------
//  @brief      获取MPU6050陀螺仪数据
//  @param      NULL
//  @return     void
//  @since      v1.0
//  Sample usage:				执行该函数后，直接查看对应的变量即可
//-------------------------------------------------------------------------------------------------------------------
void get_gyro_hardware(void)
{
    uint8 dat[6];

    iic_read_reg_bytes(IIC_NUM,MPU6050_DEV_ADDR, GYRO_XOUT_H, dat, 6);  
    mpu_gyro_x = (int16)(((uint16)dat[0]<<8 | dat[1]));
    mpu_gyro_y = (int16)(((uint16)dat[2]<<8 | dat[3]));
    mpu_gyro_z = (int16)(((uint16)dat[4]<<8 | dat[5]));
}

void get_mag_hardware(void)
{		
		unsigned char dat[6];
		static uint8 i = 0;
//		if(i==0)
//		{
//			iic_write_reg(IIC_NUM,MPU6050_DEV_ADDR,0x37,0x02);
//			i++;
//		}
//		else if(i==1)
//		{
//			iic_write_reg(IIC_NUM,MAG_ADDRESS,0x0A,0x01);
//			i++;
//		}
//		else if(i==2)
//		{
//			iic_read_reg_bytes(IIC_NUM,MAG_ADDRESS,MAG_XOUT_L, dat, 6);  
//			mpu_mag_x = (int16)(((uint16)dat[1]<<8 | dat[0]));
//			mpu_mag_y = (int16)(((uint16)dat[3]<<8 | dat[2]));
//			mpu_mag_z = (int16)(((uint16)dat[5]<<8 | dat[4]));
//			i=0;
//		}
			iic_write_reg(IIC_NUM,MPU6050_DEV_ADDR,0x37,0x02);
			systick_delay_ms(5);
			iic_write_reg(IIC_NUM,MAG_ADDRESS,0x0A,0x01);
			systick_delay_ms(10);;  // at 100 Hz ODR, new mag data is available every 10 ms
			iic_write_reg(IIC_NUM,MAG_ADDRESS,0x0A,0x01);
			iic_read_reg_bytes(IIC_NUM,MAG_ADDRESS,MAG_XOUT_L, dat, 6);  
			mpu_mag_x = (int16)(((uint16)dat[1]<<8 | dat[0]));
			mpu_mag_y = (int16)(((uint16)dat[3]<<8 | dat[2]));
			mpu_mag_z = (int16)(((uint16)dat[5]<<8 | dat[4]));
		
}

/* Read-only Reg ( ROM ) */ //芯片内置地磁仪校正值
#define AK8963_ASAX               0x10
#define AK8963_ASAY               0x11
#define AK8963_ASAZ               0x12

#define AK8963_CNTL1             0x0A
#define AK8963_CNTL2             0x0B


//读取厂家芯片内地磁校正值
//磁力计校准
void MPU92_AUX_AK8963_Init( void )
{
  uint8_t res;
	uint8 asa[3];
	float  AK8963_ASA[3] = {0};
	iic_write_reg(IIC_NUM,MAG_ADDRESS,AK8963_CNTL2,0x01);
   systick_delay_ms(10);  
	iic_write_reg(IIC_NUM,MAG_ADDRESS, AK8963_CNTL1,0x00);
  systick_delay_ms(1);  
	iic_write_reg(IIC_NUM,MAG_ADDRESS, AK8963_CNTL1,0x1F);   // Fuse ROM access mode, Read sensitivity adjustment
   systick_delay_ms(10);  
	iic_read_reg_bytes(IIC_NUM,MAG_ADDRESS,AK8963_ASAX,&asa[0], 1);  
   systick_delay_ms(1);  
	iic_read_reg_bytes(IIC_NUM,MAG_ADDRESS,AK8963_ASAY,&asa[1], 1);  
   systick_delay_ms(1);  
	iic_read_reg_bytes(IIC_NUM,MAG_ADDRESS,AK8963_ASAZ,&asa[2], 1);  
   systick_delay_ms(1);  
	
	iic_write_reg(IIC_NUM,MAG_ADDRESS, AK8963_CNTL1,0x00);
  systick_delay_ms(10); 
  iic_write_reg(IIC_NUM,MAG_ADDRESS, AK8963_CNTL1,0x16);    // Continuous measurement mode 2 & 16-bit
  systick_delay_ms(10); 

  AK8963_ASA[0] = (asa[0] - 128) * 0.5 / 128 + 1;
  AK8963_ASA[1] = (asa[1] - 128) * 0.5 / 128 + 1;
  AK8963_ASA[2] = (asa[2] - 128) * 0.5 / 128 + 1;
	
	iic_read_reg_bytes(IIC_NUM,MAG_ADDRESS,AK8963_CNTL1,&res, 1);  
	res&=0x10;
  switch (res) {
    case 0x00:  mag_sensadj[0] = mag_sensadj[1] = mag_sensadj[2] = 0.6;   break;
    case 0x10:  mag_sensadj[0] = mag_sensadj[1] = mag_sensadj[2] = 0.15;  break;
  }

  mag_sensadj[0] *= AK8963_ASA[0];
  mag_sensadj[1] *= AK8963_ASA[1];
  mag_sensadj[2] *= AK8963_ASA[2];

}



//8字校准，参数为保存的校准地址
void magcalMPU9250(float * dest1) 
{
  uint16_t ii = 0, sample_count = 1500;
	unsigned char dat[6];
  int32_t mag_bias[3] = {0, 0, 0}, mag_scale[3] = {0, 0, 0};
  int16_t mag_max[3] = {-32767, -32767, -32767}, mag_min[3] = {32767, 32767, 32767}, mag_temp[3] = {0, 0, 0};

  PRINTF("Mag Calibration: Wave device in a figure eight until done!\r\n");
  
    for(ii = 0; ii < sample_count; ii++) {
			iic_write_reg(IIC_NUM,MPU6050_DEV_ADDR,0x37,0x02);
			systick_delay_ms(5);
			iic_write_reg(IIC_NUM,MAG_ADDRESS,0x0A,0x01);
			systick_delay_ms(10);;  // at 100 Hz ODR, new mag data is available every 10 ms
			iic_write_reg(IIC_NUM,MAG_ADDRESS,0x0A,0x01);
			iic_read_reg_bytes(IIC_NUM,MAG_ADDRESS,MAG_XOUT_L, dat, 6);  
			mag_temp[0] = (int16)(((uint16)dat[1]<<8 | dat[0]));
			mag_temp[1] = (int16)(((uint16)dat[3]<<8 | dat[2]));
			mag_temp[2] = (int16)(((uint16)dat[5]<<8 | dat[4]));
			for (int jj = 0; jj < 3; jj++) {
      if(mag_temp[jj] > mag_max[jj]) mag_max[jj] = mag_temp[jj];
      if(mag_temp[jj] < mag_min[jj]) mag_min[jj] = mag_temp[jj];
				PRINTF("%d  %d  %d  \r\n",mag_max[0],mag_max[1],mag_max[2]);
    }
    }


    // Get hard iron correction
    mag_bias[0]  = (mag_max[0] + mag_min[0])/2;  // get average x mag bias in counts
    mag_bias[1]  = (mag_max[1] + mag_min[1])/2;  // get average y mag bias in counts
    mag_bias[2]  = (mag_max[2] + mag_min[2])/2;  // get average z mag bias in counts
    
    dest1[0] = (float) mag_bias[0]*mag_sensadj[0];  // save mag biases in G for main program
    dest1[1] = (float) mag_bias[1]*mag_sensadj[1];   
    dest1[2] = (float) mag_bias[2]*mag_sensadj[2]; 
		PRINTF("Mag Calibration done!\r\n");
		PRINTF("%3f  %3f  %3f  \r\n",dest1[0],dest1[1],dest1[2]);
}






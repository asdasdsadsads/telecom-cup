#ifndef _STEER_CONTROL_h
#define _STEER_CONTROL_h

#include "headfile.h"

#define steer_one   PWM4_MODULE2_CHA_C30
#define steer_two   PWM4_MODULE3_CHA_C31
#ifdef NewMainBoard
#define steer_three PWM1_MODULE2_CHA_D16
#else
#define steer_three PWM4_MODULE1_CHA_B25
#endif

typedef enum{firststeer,secondsteer,thirdsteer}MySteer;

void turn_Angle(MySteer steer,int angle,int maxAngle);
void steer_init();
void getImg();
void putImg();









#endif
#ifndef _MOTOL_CONTROL_h
#define _MOTOL_CONTROL_h

#include "headfile.h"

typedef struct point{
int x_point;
int y_point;}encoder_point;//这个结构体记录的是目前车处在的点位

typedef struct
{
	int L_S;
	int L_X;
	int R_S;
	int R_X;
}vector3_int;

extern encoder_point position_point;

void motol_init();
void motol_allSetpwm(int left_front,int left_rear,int right_front,int right_rear,int max);
void motol_start();
void NimingOutput_Position(int32 L_SE,int32 L_XE,int32 R_SE,int32 R_XE);
void NimingOutput_Speed(int32 L_SE,int32 L_XE,int32 R_SE,int32 R_XE);
void clear_All_pid(uint8 choose);
int get_forward_signle(uint8_t choose);
int get_forward_doublepid_signle(uint8_t choose);
int get_forward_average();
int get_forward_average_abs();

#endif
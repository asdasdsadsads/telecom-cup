
#include "mpu9250.h"


#define SPI_NUM2         SPI_3           
#define SPI_SCK_PIN2     SPI3_SCK_B0   //接模块SPC
#define SPI_MOSI_PIN2    SPI3_MOSI_B1  //接模块SDI
#define SPI_MISO_PIN2    SPI3_MISO_B2  //接模块SDO
#define SPI_CS_PIN2      SPI3_CS0_B3   //接模块CS
#define delayms(n)				systick_delay_ms(n)		
#define delay_us(n)				systick_delay_us(n)
 
int16 Mag[3];

/*LLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLL
【函数名】
【返回值】无 0：初始化成功   1：失败
【参数值】无 
QQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQ*/
uint8_t MPU9250_Init(void)
{
    uint8_t res;
    (void)spi_init(SPI_NUM2, SPI_SCK_PIN2, SPI_MOSI_PIN2, SPI_MISO_PIN2, SPI_CS_PIN2, 3, 10*1000*1000);//硬件SPI初始化
    delayms(100);
    res=MPU9250_Read_Byte(WHO_AM_I);                        //读取ICM20602的ID
    res = 0;
    MPU9250_Write_Byte(ICM_PWR_MGMT1_REG,0X80);//复位
    delayms(100);  //延时100ms
//    MPU9250_Write_Byte(ICM_PWR_MGMT1_REG,0X00);//唤醒
//    delayms(100);  //延时100ms

    MPU9250_Set_Gyro_Fsr(3);			       //陀螺仪传感器,±2000dps   
    MPU9250_Set_Accel_Fsr(1);				   //加速度传感器,±4g
    MPU9250_Set_Rate(1000);					   //设置采样率1000Hz
    MPU9250_Write_Byte(ICM_CFG_REG,0x02);      //设置数字低通滤波器   98hz
		MPU9250_Write_Byte(INT_PIN_CFG ,0x32);// INT Pin / Bypass Enable Configuration  磁力计使能
		MPU9250_Write_Byte(I2C_MST_CTRL,0x4d);//I2C MAster mode and Speed 400 kHz
		MPU9250_Write_Byte(ICM_USER_CTRL_REG,0x20); // I2C_MST _EN 
		MPU9250_Write_Byte(I2C_MST_DELAY_CTRL ,0x01);//延时使能I2C_SLV0 _DLY_ enable 	
		MPU9250_Write_Byte(I2C_SLV0_CTRL ,0x81); //enable IIC	and EXT_SENS_DATA==1 Byte
    MPU9250_Write_Byte(ICM_PWR_MGMT1_REG,0X01);//设置CLKSEL,PLL X轴为参考
    MPU9250_Write_Byte(ICM_PWR_MGMT2_REG,0X00);//加速度与陀螺仪都工作

    i2c_Mag_write(AK8963_CNTL2_REG,AK8963_CNTL2_SRST); // Reset AK8963
    i2c_Mag_write(AK8963_CNTL1_REG,0x12); // use i2c to set AK8963 working on Continuous measurement mode1 & 16-bit output	
    return 0;
}

//设置MPU9250陀螺仪传感器满量程范围
//fsr:0,±250dps;1,±500dps;2,±1000dps;3,±2000dps
//返回值:0,设置成功
//    其他,设置失败 
void MPU9250_Set_Gyro_Fsr(uint8_t fsr)
{
	MPU9250_Write_Byte(ICM_GYRO_CFG_REG,fsr<<3);//设置陀螺仪满量程范围  
}
//设置MPU6050加速度传感器满量程范围
//fsr:0,±2g;1,±4g;2,±8g;3,±16g
//返回值:0,设置成功
//    其他,设置失败 
void MPU9250_Set_Accel_Fsr(uint8_t fsr)
{
	MPU9250_Write_Byte(ICM_ACCEL_CFG_REG,fsr<<3);//设置加速度传感器满量程范围  
}

//设置MPU6050的数字低通滤波器
//lpf:数字低通滤波频率(Hz)
//返回值:0,设置成功
//    其他,设置失败 
void MPU9250_Set_LPF(uint16_t lpf)
{
	uint8_t data=0;
	if(lpf>=188)data=1;
	else if(lpf>=98)data=2;
	else if(lpf>=42)data=3;
	else if(lpf>=20)data=4;
	else if(lpf>=10)data=5;
	else data=6; 
	MPU9250_Write_Byte(ICM_CFG_REG,data);//设置数字低通滤波器  
}

//设置MPU9250的采样率(假定Fs=1KHz)
//rate:4~1000(Hz)
//返回值:0,设置成功
//    其他,设置失败 
void MPU9250_Set_Rate(uint16_t rate)
{
	uint8_t data;
	if(rate>1000)rate=1000;
	if(rate<4)rate=4;
	data=1000/rate-1;
	MPU9250_Write_Byte(ICM_SAMPLE_RATE_REG,data);	//设置数字低通滤波器
 	MPU9250_Set_LPF(rate/2);	//自动设置LPF为采样率的一半
}

//得到温度值
//返回值:温度值(扩大了100倍)
short MPU9250_Get_Temperature(void)
{
    uint8_t buf[3]; 
    short raw;
	float temp;
	MPU9250_Read_Len(ICM_TEMP_OUTH_REG,2,buf); 
    raw=((uint16_t)buf[0]<<8)|buf[1];  
    temp=21+((double)raw)/333.87;  
    return (short)temp*100;
}
//得到陀螺仪值(原始值)
//gx,gy,gz:陀螺仪x,y,z轴的原始读数(带符号)
//返回值:0,成功
//    其他,错误代码
void MPU9250_Get_Gyroscope(short *gx,short *gy,short *gz)
{
    uint8_t buf[7]; 
	MPU9250_Read_Len(ICM_GYRO_XOUTH_REG,6,buf);
    
    *gx=((uint16_t)buf[1]<<8)|buf[2];  
    *gy=((uint16_t)buf[3]<<8)|buf[4];  
    *gz=((uint16_t)buf[5]<<8)|buf[6];
    
}
//得到加速度值(原始值)
//gx,gy,gz:陀螺仪x,y,z轴的原始读数(带符号)
//返回值:0,成功
//    其他,错误代码
void MPU9250_Get_Accelerometer(short *ax,short *ay,short *az)
{
    uint8_t buf[7];  
	MPU9250_Read_Len(ICM_ACCEL_XOUTH_REG,6,buf);
    
    *ax=((uint16_t)buf[1]<<8)|buf[2];  
    *ay=((uint16_t)buf[3]<<8)|buf[4];  
    *az=((uint16_t)buf[5]<<8)|buf[6];
    
}

//得到加计值、温度值、角速度值(原始值)
//gx,gy,gz:陀螺仪x,y,z轴的原始读数(带符号)
//返回值:0,成功
//    其他,错误代码
void MPU9250_Get_Raw_data(short *ax,short *ay,short *az,short *gx,short *gy,short *gz)
{
    uint8_t buf[15];  
		MPU9250_Read_Len(ICM_ACCEL_XOUTH_REG,14,buf);
    
    *ax=((uint16_t)buf[1]<<8)|buf[2];  
    *ay=((uint16_t)buf[3]<<8)|buf[4];  
    *az=((uint16_t)buf[5]<<8)|buf[6];
    *gx=((uint16_t)buf[9]<<8)|buf[10];  
    *gy=((uint16_t)buf[11]<<8)|buf[12];  
    *gz=((uint16_t)buf[13]<<8)|buf[14];
	
}



/**
  * @brief    SPI 连续读
  *
  * @param    reg   读的寄存器地址
  * @param    len   长度
  * @param    buf   存放读出数据 的地址

  */

void MPU9250_Read_Len(uint8_t reg,uint8_t len,uint8_t *buf)
{   
    buf[0] = reg | 0x80;
    /* 写入要读的寄存器地址 */
     spi_mosi(SPI_NUM2,SPI_CS_PIN2,buf,buf,len+1,1);

}


/**
  * @brief    SPI 写
  *
  * @param    reg   写的寄存器地址
  * @param    value 要写的值

  */
void MPU9250_Write_Byte(uint8_t reg,uint8_t value)
{
    uint8_t buff[2];

    buff[0] = reg;          //先发送寄存器
    buff[1] = value;        //再发送数据

    spi_mosi(SPI_NUM2,SPI_CS_PIN2,buff,buff,2,1);
}

/**
  * @brief    SPI 读
  *
  * @param    reg   读的寄存器地址
  *
  * @return   读出的数据
  *

  */
uint8_t MPU9250_Read_Byte(uint8_t reg)
{
    uint8 buff[2];
    buff[0] = reg | 0x80;          //先发送寄存器

    spi_mosi(SPI_NUM2,SPI_CS_PIN2,buff,buff,2,1);   

    return buff[1];
}


/***************************************************************/
// MPU内部i2c 写入
//I2C_SLVx_ADDR:  MPU9250_AK8963_ADDR
//I2C_SLVx_REG:   reg
//I2C_SLVx_Data out:  value
/***************************************************************/
static void i2c_Mag_write(uint8 reg,uint8 value)
{
	MPU9250_Write_Byte(I2C_SLV0_ADDR ,MPU9250_AK8963_ADDR);//设置磁力计地址,mode: write
	delay_us(50);
	MPU9250_Write_Byte(I2C_SLV0_REG ,reg);//set reg addr
	delay_us(50);
	MPU9250_Write_Byte(I2C_SLV0_DO ,value);//send value	
	delay_us(50);//此处因为MPU内部I2C读取速度较慢，延时等待内部写完毕
}
/***************************************************************/
// MPU内部i2c 读取
//I2C_SLVx_ADDR:  MPU9250_AK8963_ADDR
//I2C_SLVx_REG:   reg
//return value:   EXT_SENS_DATA_00 register value
/***************************************************************/
static uint8 i2c_Mag_read(uint8 reg)
{
	uint8 a;
	MPU9250_Write_Byte(I2C_SLV0_ADDR ,MPU9250_AK8963_ADDR|0x80); //设置磁力计地址，mode：read
	delay_us(50);
	MPU9250_Write_Byte(I2C_SLV0_REG ,reg);// set reg addr
	delay_us(50);
	MPU9250_Write_Byte(I2C_SLV0_DO ,0xff);//read
	delay_us(50);//此处因为MPU内部I2C读取速度较慢，必须延时等待内部读取完毕
	return MPU9250_Read_Byte(EXT_SENS_DATA_00);
}

void READ_MPU9250_MAG(void)
{ 	
	uint8 x_axis,y_axis,z_axis; 
	uint16 BUF[6]; 
	x_axis=i2c_Mag_read(AK8963_ASAX);// X轴灵敏度调整值
	y_axis=i2c_Mag_read(AK8963_ASAY);
	z_axis=i2c_Mag_read(AK8963_ASAZ);
	
	if((i2c_Mag_read(AK8963_ST1_REG)&AK8963_ST1_DOR)==0)//data ready
	{

			//读取计算X轴数据
		 BUF[0]=i2c_Mag_read(MAG_XOUT_L); //Low data	
		 if((i2c_Mag_read(AK8963_ST2_REG)&AK8963_ST2_HOFL)==1)// data reading end register & check Magnetic sensor overflow occurred 
		 {
			 BUF[0]=i2c_Mag_read(MAG_XOUT_L);//reload data
		 } 
		 BUF[1]=i2c_Mag_read(MAG_XOUT_H); //High data	
		 if((i2c_Mag_read(AK8963_ST2_REG)&AK8963_ST2_HOFL)==1)// data reading end register
		 {
			 BUF[1]=i2c_Mag_read(MAG_XOUT_H);
		 }
		 Mag[0]=((BUF[1]<<8)|BUF[0])*(((x_axis-128)>>8)+1);		//灵敏度纠正 公式见/RM-MPU-9250A-00 PDF/ 5.13	
		 
		//读取计算Y轴数据
			BUF[2]=i2c_Mag_read(MAG_YOUT_L); //Low data	
		 if((i2c_Mag_read(AK8963_ST2_REG)&AK8963_ST2_HOFL)==1)// data reading end register
		 {
			 BUF[2]=i2c_Mag_read(MAG_YOUT_L);
		 }		 
		 BUF[3]=i2c_Mag_read(MAG_YOUT_H); //High data	
		 if((i2c_Mag_read(AK8963_ST2_REG)&AK8963_ST2_HOFL)==1)// data reading end register
		 {
			 BUF[3]=i2c_Mag_read(MAG_YOUT_H);
		 }
		  Mag[1]=((BUF[3]<<8)|BUF[2])*(((y_axis-128)>>8)+1);	
		 
		//读取计算Z轴数据
		 BUF[4]=i2c_Mag_read(MAG_ZOUT_L); //Low data	
		 if((i2c_Mag_read(AK8963_ST2_REG)&AK8963_ST2_HOFL)==1)// data reading end register
		 {
			 BUF[4]=i2c_Mag_read(MAG_ZOUT_L);
		 }	 
		 BUF[5]=i2c_Mag_read(MAG_ZOUT_H); //High data	
		 if((i2c_Mag_read(AK8963_ST2_REG)&AK8963_ST2_HOFL)==1)// data reading end register
		 {
			 BUF[5]=i2c_Mag_read(MAG_ZOUT_H);
		 }
		  Mag[2]=((BUF[5]<<8)|BUF[4])*(((z_axis-128)>>8)+1);	
	}					       
}

#include "motol_control_user.h"


//位置的目标初始值（如果使用增量式式pid则忽略此项）
int target_left_front_position = 0;
int target_left_rear_position = 0;
int target_right_front_position = 0;
int target_right_rear_position = 0;

bool turn = false;						//转弯标志位
bool can_adjust = false;			//能静茹调整状态标志位
bool go_End = true;						//到达目标点标志位
bool adjust = false;			//调整标志位

uint8_t choose;					//参数为0，为位置式PID；参数为1，为串环PID

int Tar_x,Tar_y;							//目标点(单环)
int Tar_LS_RX = 0,Tar_LX_RS = 0;  //目标点(串环)
int Con_LS_RX = 0,Con_LX_RS = 0;  //目标点限制值
float Target_angle;					//目标角度

int x_Error = 0;
int y_Error = 0;

/**************************************************************************
函数功能：提供电机目标的位置编码值
入口参数：左前，左后，右前，右后
返回  值：无
使用说明：该函数在需要修改电机速度的地方使用
**************************************************************************/
void motol_Settarget_position(int left_front,int left_rear,int right_front,int right_rear)
{
	target_left_front_position = left_front;
	target_left_rear_position = left_rear;
	target_right_front_position = right_front;
	target_right_rear_position = right_rear;
}

/**************************************************************************
函数功能：提供电机目标的速度编码值
入口参数：左前，左后，右前，右后
返回  值：无
使用说明：该函数在需要修改电机速度的地方使用
**************************************************************************/
void motol_Settarget_speed(int left_front,int left_rear,int right_front,int right_rear)
{
	L_S_forward_Spd = left_front;
	L_X_forward_Spd = left_rear;
	R_S_forward_Spd = right_front;
	R_X_forward_Spd = right_rear;
}

/**************************************************************************
函数功能：车身转弯
入口参数：车身转弯角度
返回  值：无
**************************************************************************/
void turn_angle(float Target)
{
	turn = true;
	Target_angle = Target;
}

/**************************************************************************
函数功能：提供电机目标的位置串环使用(直行)
入口参数：左前，左后，右前，右后
返回  值：无
使用说明：用户实际操作的函数
**************************************************************************/
void motol_Settarget_point_Spd(int x,int y,int distance)
{
	forward_Distance = distance;
	motol_Settarget_position(forward_Distance,forward_Distance,forward_Distance,forward_Distance);
	choose = 0;
	go_End = false;
}

/**************************************************************************
函数功能：提供电机目标的位置(解算的)
入口参数：左前，左后，右前，右后
返回  值：无
使用说明：用户实际操作的函数
**************************************************************************/
void motol_Settarget_point_move(int x,int y,int distance)
{
	switch(final_points[now_point].direction)
	{
		case 0:
		{break;}
		case 1:
		{x=-x;y=-y;break;}
		case 2:
		{int temp = x; x=y;y=-temp; break;}
		case 3:
		{int temp = x; x=-y;y=temp; break;}
		default:;
		break;
	}
	Tar_LS_RX = y+x;
	Tar_LX_RS = y-x;
	Con_LS_RX = 1100*abs(Tar_LS_RX)/(abs(Tar_LX_RS)+abs(Tar_LS_RX));
	Con_LX_RS = 1100*abs(Tar_LX_RS)/(abs(Tar_LX_RS)+abs(Tar_LS_RX));
	choose = 1;
	go_End = false;
}

/**************************************************************************
函数功能：车的首次转弯(直走之前转向目标)
入口参数：无
返回  值：无
使用说明：用户不必理会
**************************************************************************/
void first_turn()
{
	int x_angle = Centimeter_to_encoder(final_points[now_point].x_point)-position_point.x_point;
	int y_angle =	Centimeter_to_encoder(final_points[now_point].y_point)-position_point.y_point;
	if(y_angle==0)
		y_angle = 1;
	float angle_target = RadtoDeg*atan2(x_angle,y_angle);
	if(angle_target<0)
		angle_target+=360;
	turn_angle(angle_target);
//	PRINTF("first=%3f  count=%d\r\n",angle_target,now_point);
}

/**************************************************************************
函数功能：车的第二次转弯(直走之后正对目标)
入口参数：无
返回  值：无
使用说明：用户不必理会
**************************************************************************/
void second_turn()
{
	float angle_target = closeToImg_turn(final_points);
	turn_angle(angle_target);			//转到对应角度
//	PRINTF("first=%3f  count=%d\r\n",angle_target,now_point);
}








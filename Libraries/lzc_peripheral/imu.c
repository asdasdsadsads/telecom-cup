#include "imu.h"


#ifdef pitch_roll 
filter_t  low_filter_acc_x;
filter_t  low_filter_acc_y;
filter_t  low_filter_gyro_x;
filter_t  low_filter_gyro_y;

#endif
filter_t  low_filter_acc_z;
filter_t  low_filter_gyro_z;

vector3f_t ahrs_angle;

void Lowpassfitter_init()
{
#ifdef pitch_roll 
	  LowPassFilter_Init(&low_filter_acc_x,100,30);
    LowPassFilter_Init(&low_filter_acc_y,100,30);
    LowPassFilter_Init(&low_filter_gyro_x,100,30);
    LowPassFilter_Init(&low_filter_gyro_y,100,30);
    LowPassFilter_Init(&low_filter_acc_z,100,30);
#endif	
    LowPassFilter_Init(&low_filter_gyro_z,100,30); 

}


/* 传感器校准标志位 */
bool flag_ins_calibration = true;
vector3f_t acc_vector_offset;   //加计零偏值
vector3f_t gyro_vector_offset;  //角速度计零偏值
float  mag_bias[3] = {6.075f,6.900f,1.650f};//储存磁力计的误差值（根据场地做8字校准）

/**
  * @brief    传感器校准 以当前静止面为水平面
  *
  * @param    
  *
  * @return   
  *
  * @note     
  *
  * @example  
  */
void ins_calibration(void)
{  
    Lowpassfitter_init();
			vector3f_t _acc_var;           //存放加计方差
			vector3i_t _acc_vector[600];   //存放加计读取的原始数据
			vector3i_t _gyro_vector[600];  //存放角速度计读取的原始数据
			vector3f_t _gyro_average;      //存放角速度计平均值
			vector3f_t _acc_average;       //存放加计平均值
			vector3f_t _gyro_var;          //存放角速度计方差
			_acc_average.x = 0;
			_acc_average.y = 0;
			_acc_average.z = 0;
			_gyro_average.x = 0;
			_gyro_average.y = 0;
			_gyro_average.z = 0;
	
    do{
		for(int i = 0; i < 500; i++)//采样500次以此获得初始的零飘
		{			
				#ifdef ICM20602
				get_icm20602_accdata_spi();
				get_icm20602_gyro_spi();
				_acc_vector[i].x = icm_acc_x;
				_acc_vector[i].y = icm_acc_y;
				_acc_vector[i].z = icm_acc_z;
				_gyro_vector[i].x = icm_gyro_x;
				_gyro_vector[i].y = icm_gyro_y;
				_gyro_vector[i].z = icm_gyro_z;
			
				#else

				get_accdata_hardware();
				get_gyro_hardware();
				_acc_vector[i].x = mpu_acc_x;
				_acc_vector[i].y = mpu_acc_y;
				_acc_vector[i].z = mpu_acc_z;
				_gyro_vector[i].x = mpu_gyro_x;
				_gyro_vector[i].y = mpu_gyro_y;
				_gyro_vector[i].z = mpu_gyro_z;
			
			#endif

				_acc_average.x  += _acc_vector[i].x/500.0f;
				_acc_average.y  += _acc_vector[i].y/500.0f;
				_acc_average.z  += (_acc_vector[i].z - 4196)/500.0f; 
				_gyro_average.x += _gyro_vector[i].x/500.0f;
				_gyro_average.y += _gyro_vector[i].y/500.0f;
				_gyro_average.z += _gyro_vector[i].z/500.0f; 
				systick_delay_ms(2);
		}
				acc_vector_offset.x   = _acc_average.x;          //保存静止时的零偏值
				acc_vector_offset.y   = _acc_average.y;
				acc_vector_offset.z   = _acc_average.z;
				gyro_vector_offset.x  = _gyro_average.x;
				gyro_vector_offset.y  = _gyro_average.y;
				gyro_vector_offset.z  = _gyro_average.z;
				flag_ins_calibration = false;                            //校准标志位清零
    }while(flag_ins_calibration);
}


/* 全局变量 存放初步处理后的加速度计 陀螺仪数据 */
vector3f_t gyro_vector;     
vector3f_t acc_vector;      
vector3f_t mag_vector;    

/**
  * @brief    更新传感器数值
  *
  * @param    
  *
  * @return   
  *
  * @note     
  *
  * @example  
  *
  * @date     2019/6/17 星期一
  */
void ins_update(void)
{
    /* 保存最近三次的数据 */
    static vector3i_t gyro_vector_last[3];
    static vector3i_t acc_vector_last[3];
    static uint8_t num = 0;
    if(num > 2) num = 0;
    
//		/*  数据获取 */
#ifdef ICM20602
		get_icm20602_gyro_spi();
#ifdef pitch_roll 
		get_icm20602_accdata_spi();
    acc_vector_last[num].x = icm_acc_x;
		acc_vector_last[num].y = icm_acc_y;
		acc_vector_last[num].z = icm_acc_z;
		gyro_vector_last[num].x = icm_gyro_x;
		gyro_vector_last[num].y = icm_gyro_y;
#endif
		gyro_vector_last[num].z = icm_gyro_z;
	
#else
		get_accdata_hardware();
		get_gyro_hardware();
		get_mag_hardware();
		
    acc_vector_last[num].x = mpu_acc_x;
		acc_vector_last[num].y = mpu_acc_y;
		acc_vector_last[num].z = mpu_acc_z;
		gyro_vector_last[num].x = mpu_gyro_x;
		gyro_vector_last[num].y = mpu_gyro_y;
		gyro_vector_last[num].z = mpu_gyro_z;
		
#endif


    /* 去零偏 */
#ifdef pitch_roll 
    acc_vector_last[num].x -= acc_vector_offset.x;   
    acc_vector_last[num].y -= acc_vector_offset.y;   
    acc_vector_last[num].z -= acc_vector_offset.z;   
    gyro_vector_last[num].x -= gyro_vector_offset.x; 
    gyro_vector_last[num].y -= gyro_vector_offset.y; 
#endif
//    gyro_vector_last[num].z -= gyro_vector_offset.z; 
				
//    /* 平均 低通滤波 */
#ifdef pitch_roll 
		acc_vector.x = LowPassFilter_apply(&low_filter_acc_x, (acc_vector_last[0].x + acc_vector_last[1].x + acc_vector_last[2].x)/3);
    acc_vector.y = LowPassFilter_apply(&low_filter_acc_y, (acc_vector_last[0].y + acc_vector_last[1].y + acc_vector_last[2].y)/3);
    acc_vector.z = LowPassFilter_apply(&low_filter_acc_z, (acc_vector_last[0].z + acc_vector_last[1].z + acc_vector_last[2].z)/3);
    
    gyro_vector.x = LowPassFilter_apply(&low_filter_gyro_x, (gyro_vector_last[0].x + gyro_vector_last[1].x + gyro_vector_last[2].x)/3);
    gyro_vector.y = LowPassFilter_apply(&low_filter_gyro_y, (gyro_vector_last[0].y + gyro_vector_last[1].y + gyro_vector_last[2].y)/3);

 #endif 
		gyro_vector.z = LowPassFilter_apply(&low_filter_gyro_z, (gyro_vector_last[0].z + gyro_vector_last[1].z + gyro_vector_last[2].z)/3);
//    gyro_vector.x = (gyro_vector_last[0].x + gyro_vector_last[1].x + gyro_vector_last[2].x)/3;
//    gyro_vector.y = (gyro_vector_last[0].y + gyro_vector_last[1].y + gyro_vector_last[2].y)/3;
//    gyro_vector.z = (gyro_vector_last[0].z + gyro_vector_last[1].z + gyro_vector_last[2].z)/3;
    
//		PRINTF("%3f   %3f   %3f \r\n",gyro_vector.x,gyro_vector.y,gyro_vector.z);
//		PRINTF("%3f   %3f   %3f \r\n",acc_vector.x,acc_vector.y,acc_vector.z);
#ifdef Complementary
#ifdef pitch_roll 
  gyro_vector.x *=  Gyro_Gain; 
	gyro_vector.y *=  Gyro_Gain;

#endif
	gyro_vector.z *=  Gyro_Gain;
	
#else	
	//加速度AD值 转换成 米/平方秒 
	acc_vector.x *=  Acc_Gain * G;
	acc_vector.y *=  Acc_Gain * G;
	acc_vector.z *=  Acc_Gain * G;
	gyro_vector.x *=  Gyro_Gr;  
	gyro_vector.y *=  Gyro_Gr;
	gyro_vector.z *=  Gyro_Gr;

#ifdef ICM20602

#else
	mag_vector.x = mpu_mag_x*mag_sensadj[0]-mag_bias[0];
	mag_vector.y = mpu_mag_x*mag_sensadj[1]-mag_bias[1];
	mag_vector.z = mpu_mag_x*mag_sensadj[2]-mag_bias[2];
	
#endif

#endif	
	//陀螺仪AD值 转换成 弧度/秒    
    num++;
}


//Complementary滤波
const float dt = 0.01f;									 //时间周期(此次我10ms在中断中调用一次该函数,在主函数初始化中设置)
float angle[3] = {0};										//0、1、2对应pitch、roll、yaw
const float R = 0.98f;											//互补系数，越低跟随度越低，滤波效果越低
/**************************************************************************
函数功能：将mpu6050读取的角加速度、角速度转化为欧拉角
入口参数：无
返回  值：无
使用说明：此函数在定时器1中断中使用，如需外界获取角度，请调用angle[i]
**************************************************************************/
void ImuCalculate_Complementary()//互补滤波算法
{  
		ins_update();
    static float angle_last[3]={0};
		float increase[3] = {0,0,0};
#ifdef pitch_roll 
		increase[0] = gyro_vector.x*dt;
		if(fabs(increase[0])<0.02) increase[0]=0;
    angle[0] = R*(angle_last[0]+increase[0]) 
              + (1-R)*RadtoDeg*atan(acc_vector.x/acc_vector.z);
    angle_last[0] = angle[0];										//处理pitch
    
		increase[1] = gyro_vector.y*dt;
		if(fabs(increase[1])<0.0) increase[1]=0;
		angle[1] = R*(angle_last[1]+increase[1])
              + (1-R)*RadtoDeg*atan(acc_vector.y/acc_vector.z);
    angle_last[1] = angle[1];											//处理roll
#endif		
		increase[2] = -gyro_vector.z*dt;
//		increase[2] = -icm_gyro_z*dt*Gyro_Gain;
		if(fabs(increase[2])<0.02) increase[2]=0;
    angle[2] = angle_last[2]+increase[2];
    angle_last[2] = angle[2];				//处理yaw
		
		//控制转弯角度范围在0-360°以内
		if(angle_last[2]>360)
			angle_last[2]-=360;
		else if(angle_last[2]<0)
			angle_last[2]+=360;
		
#ifdef OLED_ANGLE
		char PitchStr[20],RollStr[20];
		char YawStr[20];
		sprintf(PitchStr,"pitch = %.2f",angle[0]);
		sprintf(RollStr,"roll = %.2f",angle[1]);
		sprintf(YawStr,"yaw = %.2f",angle[2]);
		oled_p8x16str(2,0,PitchStr);
		oled_p8x16str(2,2,RollStr);
		oled_p8x16str(2,4,YawStr);
#endif

//		NimingOutput_Angle();
}






//MahonyAHRS滤波
const float ahrs_kp = 1.08f; //PI控制器，修正机体坐标系
const float ahrs_ki = 0.05f;

#define sampleFreq	100.0f			// sample frequency in Hz
#define twoKpDef	1.08f	// 2 * proportional gain
#define twoKiDef	0.05f// 2 * integral gain

//---------------------------------------------------------------------------------------------------
// Variable definitions

volatile float twoKp = twoKpDef;											// 2 * proportional gain (Kp)
volatile float twoKi = twoKiDef;											// 2 * integral gain (Ki)
volatile float q0 = 1.0f, q1 = 0.0f, q2 = 0.0f, q3 = 0.0f;					// quaternion of sensor frame relative to auxiliary frame
volatile float integralFBx = 0.0f,  integralFBy = 0.0f, integralFBz = 0.0f;	// integral error terms scaled by Ki

//四元数更新（不需要磁力计）
void MahonyAHRSupdateIMU(float gx, float gy, float gz, float ax, float ay, float az) {
	float recipNorm;
	float halfvx, halfvy, halfvz;
	float halfex, halfey, halfez;
	float qa, qb, qc;

	// Compute feedback only if accelerometer measurement valid (avoids NaN in accelerometer normalisation)
	if(!((ax == 0.0f) && (ay == 0.0f) && (az == 0.0f))) {

		// Normalise accelerometer measurement
		recipNorm = invSqrt(ax * ax + ay * ay + az * az);
		ax *= recipNorm;
		ay *= recipNorm;
		az *= recipNorm;        

		// Estimated direction of gravity and vector perpendicular to magnetic flux
		halfvx = q1 * q3 - q0 * q2;
		halfvy = q0 * q1 + q2 * q3;
		halfvz = q0 * q0 - 0.5f + q3 * q3;
	
		// Error is sum of cross product between estimated and measured direction of gravity
		halfex = (ay * halfvz - az * halfvy);
		halfey = (az * halfvx - ax * halfvz);
		halfez = (ax * halfvy - ay * halfvx);

		// Compute and apply integral feedback if enabled
		if(twoKi > 0.0f) {
			integralFBx += twoKi * halfex * (1.0f / sampleFreq);	// integral error scaled by Ki
			integralFBy += twoKi * halfey * (1.0f / sampleFreq);
			integralFBz += twoKi * halfez * (1.0f / sampleFreq);
			gx += integralFBx;	// apply integral feedback
			gy += integralFBy;
			gz += integralFBz;
		}
		else {
			integralFBx = 0.0f;	// prevent integral windup
			integralFBy = 0.0f;
			integralFBz = 0.0f;
		}

		// Apply proportional feedback
		gx += twoKp * halfex;
		gy += twoKp * halfey;
		gz += twoKp * halfez;
	}
	
	// Integrate rate of change of quaternion
	gx *= (0.5f * (1.0f / sampleFreq));		// pre-multiply common factors
	gy *= (0.5f * (1.0f / sampleFreq));
	gz *= (0.5f * (1.0f / sampleFreq));
	qa = q0;
	qb = q1;
	qc = q2;
	q0 += (-qb * gx - qc * gy - q3 * gz);
	q1 += (qa * gx + qc * gz - q3 * gy);
	q2 += (qa * gy - qb * gz + q3 * gx);
	q3 += (qa * gz + qb * gy - qc * gx); 
	
	// Normalise quaternion
	recipNorm = invSqrt(q0 * q0 + q1 * q1 + q2 * q2 + q3 * q3);
	q0 *= recipNorm;
	q1 *= recipNorm;
	q2 *= recipNorm;
	q3 *= recipNorm;
}

//四元数更新（需要磁力计）
void MahonyAHRSupdate(float gx, float gy, float gz, float ax, float ay, float az, float mx, float my, float mz) {
	float recipNorm;
  float q0q0, q0q1, q0q2, q0q3, q1q1, q1q2, q1q3, q2q2, q2q3, q3q3;  
	float hx, hy, bx, bz;
	float halfvx, halfvy, halfvz, halfwx, halfwy, halfwz;
	float halfex, halfey, halfez;
	float qa, qb, qc;

	// Use IMU algorithm if magnetometer measurement invalid (avoids NaN in magnetometer normalisation)
	if((mx == 0.0f) && (my == 0.0f) && (mz == 0.0f)) {
		MahonyAHRSupdateIMU(gx, gy, gz, ax, ay, az);
		return;
	}

	// Compute feedback only if accelerometer measurement valid (avoids NaN in accelerometer normalisation)
	if(!((ax == 0.0f) && (ay == 0.0f) && (az == 0.0f))) {

		// Normalise accelerometer measurement
		recipNorm = invSqrt(ax * ax + ay * ay + az * az);
		ax *= recipNorm;
		ay *= recipNorm;
		az *= recipNorm;     

		// Normalise magnetometer measurement
		recipNorm = invSqrt(mx * mx + my * my + mz * mz);
		mx *= recipNorm;
		my *= recipNorm;
		mz *= recipNorm;   

        // Auxiliary variables to avoid repeated arithmetic
		q0q0 = q0 * q0;
		q0q1 = q0 * q1;
		q0q2 = q0 * q2;
		q0q3 = q0 * q3;
		q1q1 = q1 * q1;
		q1q2 = q1 * q2;
		q1q3 = q1 * q3;
		q2q2 = q2 * q2;
		q2q3 = q2 * q3;
		q3q3 = q3 * q3;   

        // Reference direction of Earth's magnetic field
		hx = 2.0f * (mx * (0.5f - q2q2 - q3q3) + my * (q1q2 - q0q3) + mz * (q1q3 + q0q2));
		hy = 2.0f * (mx * (q1q2 + q0q3) + my * (0.5f - q1q1 - q3q3) + mz * (q2q3 - q0q1));
		bx = sqrt(hx * hx + hy * hy);
		bz = 2.0f * (mx * (q1q3 - q0q2) + my * (q2q3 + q0q1) + mz * (0.5f - q1q1 - q2q2));

		// Estimated direction of gravity and magnetic field
		halfvx = q1q3 - q0q2;
		halfvy = q0q1 + q2q3;
		halfvz = q0q0 - 0.5f + q3q3;
		halfwx = bx * (0.5f - q2q2 - q3q3) + bz * (q1q3 - q0q2);
		halfwy = bx * (q1q2 - q0q3) + bz * (q0q1 + q2q3);
		halfwz = bx * (q0q2 + q1q3) + bz * (0.5f - q1q1 - q2q2);  

		// Error is sum of cross product between estimated direction and measured direction of field vectors
		halfex = (ay * halfvz - az * halfvy) + (my * halfwz - mz * halfwy);
		halfey = (az * halfvx - ax * halfvz) + (mz * halfwx - mx * halfwz);
		halfez = (ax * halfvy - ay * halfvx) + (mx * halfwy - my * halfwx);

		// Compute and apply integral feedback if enabled
		if(twoKi > 0.0f) {
			integralFBx += twoKi * halfex * (1.0f / sampleFreq);	// integral error scaled by Ki
			integralFBy += twoKi * halfey * (1.0f / sampleFreq);
			integralFBz += twoKi * halfez * (1.0f / sampleFreq);
			gx += integralFBx;	// apply integral feedback
			gy += integralFBy;
			gz += integralFBz;
		}
		else {
			integralFBx = 0.0f;	// prevent integral windup
			integralFBy = 0.0f;
			integralFBz = 0.0f;
		}

		// Apply proportional feedback
		gx += twoKp * halfex;
		gy += twoKp * halfey;
		gz += twoKp * halfez;
	}
	
		// Integrate rate of change of quaternion
		gx *= (0.5f * (1.0f / sampleFreq));		// pre-multiply common factors
		gy *= (0.5f * (1.0f / sampleFreq));
		gz *= (0.5f * (1.0f / sampleFreq));
		qa = q0;
		qb = q1;
		qc = q2;
		q0 += (-qb * gx - qc * gy - q3 * gz);
		q1 += (qa * gx + qc * gz - q3 * gy);
		q2 += (qa * gy - qb * gz + q3 * gx);
		q3 += (qa * gz + qb * gy - qc * gx); 
		
		// Normalise quaternion
		recipNorm = invSqrt(q0 * q0 + q1 * q1 + q2 * q2 + q3 * q3);
		q0 *= recipNorm;
		q1 *= recipNorm;
		q2 *= recipNorm;
		q3 *= recipNorm;
}


/****函数  AHRS_quat_to_angle
	*作用  更新姿态角
	*参数
	*返回值
	***/
void AHRS_quat_to_angle(void)
{
	float conv_x = 2.0f * (q0 * q2 - q1 * q3);  
	float conv_y = 2.0f * (q0 * q1 + q2 * q3);
	float conv_z = q0 * q0 - q1 * q1 - q2 * q2 + q3 * q3;
/*******  姿态解算  ********/
	ahrs_angle.x = fast_atan(conv_y * invSqrt(conv_x * conv_x + conv_z * conv_z)) * 57.2958f;
	ahrs_angle.y = asin(2 * (q0 * q2 - q3 * q1)) * 57.2958f;
	ahrs_angle.z = atan2(2 * (q0 * q3 + q1 * q2), 1 - 2 * (q2 * q2 + q3 * q3)) * 57.2958f;   
    
    ahrs_angle.x = constrain_float(ahrs_angle.x, -90, 90);
    ahrs_angle.y = constrain_float(ahrs_angle.y, -90, 90);
    
/*******  根据实际情况进行角度微调  ********/
    //    static float offset = 0; 
    //        ahrs_angle.x -= 2;
    //        ahrs_angle.y += 2; 
    //          
    //        offset += 0.0011765;         //补偿yaw 根据自己解算速度 自行补偿 
    //        ahrs_angle.z -= offset;      //补偿yaw 根据自己解算速度 自行补偿
    	
}

/*姿态更新*/
void ahrs_update()
{
	// 读取惯性传感器数据
		ins_update();
	
    // quat update
#ifdef ICM20602
			MahonyAHRSupdateIMU(gyro_vector.x,gyro_vector.y,gyro_vector.z,acc_vector.x,acc_vector.y, acc_vector.z);
#else
	    MahonyAHRSupdate(gyro_vector.x, gyro_vector.y, gyro_vector.z,acc_vector.x,acc_vector.y,acc_vector.z,mag_vector.x,mag_vector.y,mag_vector.z);
#endif

    // quat to angle
//    AHRS_quat_to_angle();
//		NimingOutput_AHRS();

}

//float _motol_ins_update(void)
//{
//    /* 保存最近三次的数据 */
//		float Acce_y_now = 0;
//    static float Acce_y_last[3];
//    static uint8_t num = 0;
//    if(num > 2) num = 0;
//    
////		/*  数据获取与去零偏 */
//		get_icm20602_accdata_spi();
//		Acce_y_last[num] = icm_acc_y-acc_vector_offset.y;
//				
////    /* 平均 低通滤波 */
//    Acce_y_now = LowPassFilter_apply(&low_filter_acc_y, (Acce_y_last[0] + Acce_y_last[1] + Acce_y_last[2])/3);
//		Acce_y_now *=  Acc_Gain * G;
//    num++;
//		if(Acce_y_now<0.05)
//			return 0;
//		else
//			return Acce_y_now;
//}












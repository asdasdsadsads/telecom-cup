#ifndef __IMU_h
#define __IMU_h


#include "lzc_lowpassFilter.h"
#include "headfile.h"



extern vector3f_t gyro_vector;
extern vector3f_t acc_vector;
extern vector3f_t ahrs_angle;

extern float angle[3];

void Lowpassfitter_init();
void ins_calibration(void);
void ins_update(void);
void ImuCalculate_Complementary();
void MahonyAHRSupdateIMU(float gx, float gy, float gz, float ax, float ay, float az);
void MahonyAHRSupdate(float gx, float gy, float gz, float ax, float ay, float az, float mx, float my, float mz);
void ahrs_update();
float _motol_ins_update(void);




#endif
#include "find_img.h"

void Top_camera_handle()
{
    uint8_t data = Top_camera_getData();
    static int count = 0;
    	if(can_adjust&&adjust)
		{
            num = 0;
            x_Error = (char)readBuf[2];
            y_Error = (char)readBuf[3];
            priority = 1;
            if(!Within_angle(25))
            {
                turn_angle(closeToImg_turn(final_points));
            }
            if(abs(x_Error)<5&&abs(y_Error)<5)
            {
                count++;
            }
            else 
                count =0;
            if(count>10)
            {
                writeBuf[4] = 22;
                int refresh_x;
                int refresh_y;
                count = 0;
                adjust = false;
                switch(final_points[now_point].direction)
                {
                    case 0:
                    {	refresh_x = final_points[now_point].x_point+x_Error;
                        refresh_y = final_points[now_point].y_point+y_Error;;
                        break;}
                    case 1:
                    {	refresh_x = final_points[now_point].x_point-x_Error;
                        refresh_y = final_points[now_point].y_point-y_Error;
                        break;}
                    case 2:
                    {		
                        refresh_x = final_points[now_point].x_point-y_Error;
                        refresh_y = final_points[now_point].y_point+x_Error;
                        break;}
                    case 3:
                    {	refresh_x = final_points[now_point].x_point+y_Error;
                        refresh_y = final_points[now_point].y_point-x_Error; 
                        break;}
                    default:
                    {	refresh_x = final_points[now_point].x_point;
                        refresh_y = final_points[now_point].y_point;
                        break;}
                }
                position_point.x_point = Centimeter_to_encoder(refresh_x);
                position_point.y_point = Centimeter_to_encoder(refresh_y);
                clear_All_pid(1);
                Encoder_reinit();
                x_Error = 0;y_Error = 0;
                if(now_point>pointsNum)
                {
                    now_point = pointsNum;
                }	

                can_adjust = false;
                adjust = false;
                x_Error = 0;
                y_Error = 0;
                clear_All_pid(1);
                Encoder_reinit();
				}
		}
		else return;
}
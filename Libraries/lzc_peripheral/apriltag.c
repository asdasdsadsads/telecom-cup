#include "apriltag.h"

//标志位，0处于未寻找状态，1属于寻找状态，大于2则属于已经找到并可以利用的Apriltag
unsigned char front_Tag = 0;
unsigned char left_Tag = 0;
unsigned char right_Tag = 0;

bool right_tag = false;
bool left_tag = false;
bool front_tag = false;

unsigned int Apr_coord[3]={0};//存放从摄像头上传的apriltag数据，依次是{相对x坐标，相对z坐标，tag序号}，以tag法线方向为z轴方向

//对接收到的数据进行处理
void Detect_apriltag()
{
		unsigned char readBuf[6] = {0};
		unsigned char writeBuf[6] = {0x11,0x11,0x11,0x11,0x11,0x11};
	//	spislaveReadWrite(C26,readBuf,writeBuf,6);
		if(readBuf[0]==0x11&&readBuf[1]==0x22&&readBuf[5]==0x66)
		{
			apriltag_fitter((char)readBuf[2],readBuf[3],readBuf[4]);
		}
		else //数据错误
			return;
#ifdef OLED_APRILTAG
		char str1[20],str2[20],str3[20],str4[20];
		sprintf(str1,"x = %5d",Apr_coord[0]);
		sprintf(str2,"z = %5d",Apr_coord[1]);
		sprintf(str3,"tag = %5d",Apr_coord[2]);
		sprintf(str4,"angle = %.2f",angle[2]);
		oled_p8x16str(1,0,str1);
		oled_p8x16str(1,2,str2);
		oled_p8x16str(1,4,str3);
		oled_p8x16str(1,6,str4);
#endif
//		PRINTF("%d  %d  %d  %d  %d\r\n",readBuf[0],readBuf[1],readBuf[2],readBuf[3],readBuf[4]);
}

//均值滤波
void apriltag_fitter(char x,unsigned char z,unsigned char order)
{
	static unsigned char num =0;
	static unsigned char last_order;
	static vector2_apriltag apriltag_vector[3];
	if(num>2)
		num=0;

	if(last_order!=order&&order<10)//判断是否与上一个Apriltag相同，不相同就清零
	{
		for(int i=0;i<3;i++)
		{
			apriltag_vector[i].x = 0;
			apriltag_vector[i].z = 0;
		}
		if(front_Tag>=1&&right_Tag==0&&left_Tag==0)
		{
			front_Tag=1;
		}
		else if(front_Tag==0&&right_Tag>=1&&left_Tag==0)
		{
			right_Tag=1;
		}
		else if(front_Tag==0&&right_Tag==0&&left_Tag>=1)
		{
			left_Tag=1;
		}
		last_order=order;
		num = 0;
	}
	else if(order>=10)//数据错误
		return;
	apriltag_vector[num].x = x;
	apriltag_vector[num].z = z;
	Apr_coord[0] = (apriltag_vector[0].x+apriltag_vector[1].x+apriltag_vector[2].x)/3;
	Apr_coord[1] = (apriltag_vector[0].z+apriltag_vector[1].z+apriltag_vector[2].z)/3;
	
	//多次读取数据，数据大于10则已经可以利用
	if(front_Tag>=1&&right_Tag==0&&left_Tag==0)
	{
		front_Tag++;
	}
	else if(front_Tag==0&&right_Tag>=1&&left_Tag==0)
	{
		right_Tag++;
	}
	else if(front_Tag==0&&right_Tag==0&&left_Tag>=1)
	{
		left_Tag++;
	}
	num++;
}


//寻找物体状态
void find_Apriltag()
{
	left_Tag = 0;
	front_Tag = 0;
	right_Tag = 0;
	right_tag = false;
	left_tag = false;
	front_tag = false;
	static int count=0;
		//寻找与270，90，0°之间最小的转动度数数去读取Apriltag
		if(angle[2]<=315&&angle[2]>180)  //首先左转
		{
			left_find_Apriltag();
			front_find_Apriltag();
			right_find_Apriltag();
		}
		else if(angle[2]>315||angle[2]<=45)	//首先前转
		{
			front_find_Apriltag();
			right_find_Apriltag();
			left_find_Apriltag();
		}
		else if(angle[2]>45&&angle[2]<=180)	//首先右转
		{
			right_find_Apriltag();
			front_find_Apriltag();
			left_find_Apriltag();
		}
}



void left_find_Apriltag()
{
		static unsigned char count=0;
		turn_angle(270);
		left_Tag = 1;
		front_Tag = 0;
		right_Tag = 0;
		while(turn);
		while(left_Tag<10&&count<100)
		{
			systick_delay_ms(10);//等待1s时间
			count++;
		}
		if(left_Tag>4)
			left_tag = true;
		count = 0;
}

void front_find_Apriltag()
{
		static unsigned char count=0;
		turn_angle(0);
		front_Tag = 1;
		right_Tag = 0;
		left_Tag = 0;
		while(turn);
		while(front_Tag<10&&count<100)
		{
			systick_delay_ms(10);//等待1s时间
			count++;
		}
		if(front_Tag>4)
			front_tag = true;
		count = 0;
}

void right_find_Apriltag()
{
		static unsigned char count=0;
		turn_angle(90);
		right_Tag = 1;
		left_Tag = 0;
		front_Tag = 0;		
		while(turn);
		while(right_Tag<10&&count<100)
		{
			systick_delay_ms(10);//等待1s时间
			count++;
		}
		if(right_Tag>4)
			right_tag = true;
		count = 0;
}

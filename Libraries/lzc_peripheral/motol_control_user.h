#ifndef _MOTOL_CONTROL_USER_h
#define _MOTOL_CONTROL_USER_h
#include "headfile.h"

extern uint8_t choose;

extern int target_left_front_position;
extern int target_left_rear_position;
extern int target_right_front_position;
extern int target_right_rear_position;

extern int Tar_x,Tar_y;
extern int Tar_LX_RS,Tar_LS_RX;
extern int Con_LX_RS,Con_LS_RX;
extern float Target_angle;

extern bool can_adjust;
extern bool turn;
extern bool go_End;	
extern bool adjust;

extern int x_Error;
extern int y_Error;

void turn_angle(float Target_angle);
void motol_Settarget_position(int left_front,int left_rear,int right_front,int right_rear);
void motol_Settarget_speed(int left_front,int left_rear,int right_front,int right_rear);
void motol_Settarget_point_move(int x,int y,int distance);
void motol_Settarget_point_Spd(int x,int y,int distance);
void second_turn();
void first_turn();

#endif
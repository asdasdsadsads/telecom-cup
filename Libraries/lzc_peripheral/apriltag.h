#ifndef __APRILTAG_h
#define __APRILTAG_h
#include "headfile.h"

extern unsigned int Apr_coord[3];
extern unsigned char front_Tag;
extern unsigned char left_Tag;
extern unsigned char right_Tag;
extern bool right_tag;
extern bool left_tag;
extern bool front_tag;

void Detect_apriltag();
void apriltag_fitter(char x,unsigned char z,unsigned char order);
void find_Apriltag();
void left_find_Apriltag();
void front_find_Apriltag();
void right_find_Apriltag();
	
#endif

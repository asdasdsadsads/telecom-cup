#include "find_img.h"

unsigned char priority = 0;
unsigned char example_rx_buffer;
lpuart_transfer_t   example_receivexfer;
lpuart_handle_t     example_g_lpuartHandle;
unsigned char top_rx_buffer;
lpuart_transfer_t   top_receivexfer;
lpuart_handle_t     top_lpuartHandle;
uint8_t BigClass = 0;
uint8_t SmallClass = 0;

void bottom_receive_spi_callback_io()
{
	uint8_t readBuf[5];
	static uint8_t writeBuf[5] = {0,0,0,0,21};
#ifdef NewMainBoard
	spislaveReadWrite(C8,C5,C6,C7,readBuf,writeBuf,5);
#else
	spislaveReadWrite(C17,C28,C29,C16,readBuf,writeBuf,5);
#endif

//	PRINTF("%d %d %d %d %d \r\n",readBuf[0],readBuf[1],readBuf[2],readBuf[3],readBuf[4]);
	if(readBuf[0]==0x66&&readBuf[1]==0x64&&readBuf[4]==0x55)
	{
		if(!Within_angle(25))
		{
			turn_angle(closeToImg_turn(final_points));
		}
		x_Error = (char)readBuf[2];
		y_Error = (char)readBuf[3];
//		PRINTF("x_Err=%d y_Err=%d\r\n",x_Error,y_Error);
	}
}

void example_uart_callback(LPUART_Type *base, lpuart_handle_t *handle, status_t status, void *userData)
{
		static unsigned char num = 0;  
		static unsigned char readBuf[5];
//	  handle->rxDataSize = example_receivexfer.dataSize;  //还原缓冲区长度
//    handle->rxData = example_receivexfer.data;          //还原缓冲区地址
//    if(kStatus_LPUART_RxIdle == status)
//    {  
				if(pointsNum == now_point)
					return;
				if(!priority&&can_adjust)
				{
				if(example_rx_buffer==0x66&&num == 0){
					readBuf[0] = 0x66;
					num++;
				}
				else if(example_rx_buffer==0x64&&num == 1){
					readBuf[1] = 0x64;
					num++;
				}
				else if(num == 2){
					readBuf[2] = example_rx_buffer;
					num++;
				}
				else if(num == 3){
					readBuf[3] = example_rx_buffer;
					num++;
				}
				else if(example_rx_buffer==0x55&&num == 4)
				{
					num = 0;
						if(!Within_angle(15))
						{
							turn_angle(closeToImg_turn(final_points));
						}
						x_Error = (char)readBuf[2];
						y_Error = (char)readBuf[3];
					for(int i = 0;i<5;i++){
						readBuf[i] = 0;
					}
				}
				else 
				{
					num = 0;
					for(int i = 0;i<5;i++){
						readBuf[i] = 0;
					}
				}
			}
			else
			{
				num = 0;
				for(int i = 0;i<5;i++){
					readBuf[i] = 0;
				}
			}		
//    } 
    handle->rxDataSize = example_receivexfer.dataSize;  //还原缓冲区长度
    handle->rxData = example_receivexfer.data;          //还原缓冲区地址
}


void top_receive_uart_callback(LPUART_Type *base, lpuart_handle_t *handle, status_t status, void *userData)
{
		static unsigned char num = 0;  
		static unsigned char readBuf[6];
		static int count = 0;
		static uint8 label = 0;
			if(pointsNum == now_point)
					return;
			if(can_adjust)
			{
//				PRINTF("%d\r\n",top_rx_buffer);
				if(top_rx_buffer==0x56&&num == 0){
					readBuf[0] = 0x56;
					num++;
				}
				else if(top_rx_buffer==0x64&&num == 1){
					readBuf[1] = 0x64;
					num++;
				}
				else if(num == 2){
					readBuf[2] = top_rx_buffer;
					num++;
				}
				else if(num == 3){
					readBuf[3] = top_rx_buffer;
					num++;
				}
				else if(top_rx_buffer==0x55&&num == 4)
				{
					readBuf[4] = top_rx_buffer;
					num++;
				}
				else if(top_rx_buffer==0x78&&num == 5)		//接受位置帧
				{
					num = 0;
					x_Error = (char)readBuf[2];
					y_Error = (char)readBuf[3];
//  				adjust = true;
					priority = 1;
//					PRINTF("%d  %d\r\n",x_Error,y_Error);
					if(!Within_angle(25))
					{
						turn_angle(closeToImg_turn(final_points));
					}
					if(abs(x_Error)<5&&abs(y_Error)<5)
					{
						count++;
					}
					else 
						count =0;
					if(count>10)
					{
						int refresh_x;
						int refresh_y;
						count = 0;
						adjust = false;
						switch(final_points[now_point].direction)
						{
							case 0:
							{	refresh_x = final_points[now_point].x_point+x_Error;
								refresh_y = final_points[now_point].y_point+y_Error;
								break;}
							case 1:
							{	refresh_x = final_points[now_point].x_point-x_Error;
								refresh_y = final_points[now_point].y_point-y_Error;
								break;}
							case 2:
							{		
								refresh_x = final_points[now_point].x_point-y_Error;
								refresh_y = final_points[now_point].y_point+x_Error;
								break;}
							case 3:
							{	refresh_x = final_points[now_point].x_point+y_Error;
								refresh_y = final_points[now_point].y_point-x_Error; 
								break;}
							default:
							{	refresh_x = final_points[now_point].x_point;
								refresh_y = final_points[now_point].y_point;
								break;}
						}
						position_point.x_point = Centimeter_to_encoder(refresh_x);
						position_point.y_point = Centimeter_to_encoder(refresh_y);
						clear_All_pid(1);
						Encoder_reinit();
						x_Error = 0;y_Error = 0;
						if(now_point>pointsNum)
						{
							now_point = pointsNum;
						}
					}
						for(int i = 0;i<5;i++){
								readBuf[i] = 0;
							}
						}
						else if(top_rx_buffer==0x80&&num == 5)		//接收动物帧
						{
								can_adjust = false;
								adjust = false;
								x_Error = 0;
								y_Error = 0;
								clear_All_pid(1);
								Encoder_reinit();
//								PRINTF("animal = %d %d\r\n",readBuf[2],readBuf[3]);
						}
						else 
						{
							num = 0;
							for(int i = 0;i<5;i++){
								readBuf[i] = 0;
							}
						}
					}
					else
					{
						num = 0;
						for(int i = 0;i<5;i++){
							readBuf[i] = 0;
							}
						}		
	handle->rxDataSize = top_receivexfer.dataSize;  //还原缓冲区长度
    handle->rxData = top_receivexfer.data;          //还原缓冲区地址
}

void find_block_init()
{
	  uart_init (USART_4, 115200,UART4_TX_C16,UART4_RX_C17);	
    NVIC_SetPriority(LPUART4_IRQn,8);         //设置串口中断优先级 范围0-15 越小优先级越高
//	  NVIC_SetPriority(LPUART5_IRQn,5);         //设置串口中断优先级 范围0-15 越小优先级越高
//		uart_init (USART_5, 115200,UART5_TX_C28,UART5_RX_C29);		
    uart_rx_irq(USART_4,1);
//		uart_rx_irq(USART_5,1);
    //设置中断函数及其参数
		example_receivexfer.dataSize = 1;
		example_receivexfer.data = &example_rx_buffer;
//		top_receivexfer.dataSize = 1;
//		top_receivexfer.data = &top_rx_buffer;
    uart_set_handle(USART_4, &example_g_lpuartHandle, example_uart_callback, NULL, 0, example_receivexfer.data, 1);
//		uart_set_handle(USART_5, &top_lpuartHandle, top_receive_uart_callback, NULL, 0, top_receivexfer.data, 1);
}

/**************************************************************************
函数功能：接收首次摄像头传来的点的坐标并且进行处理
入口参数：无
返回  值：true（接收完整），false（接收不完整）
*************************************************************************/
bool receive_Points_Position_single(){
	 static uint8 label = 0;
	 uint8 readBuf[5] = {0};
	 uint8 writeBuf[5] = {0,0,0,0,0};
			writeBuf[4] = label;
#ifdef NewMainBoard
		spislaveReadWrite(B21,B14,B15,B16,readBuf,writeBuf,5);
#else
		spislaveReadWrite(C27,C20,C21,C22,readBuf,writeBuf,5);
#endif
//	 PRINTF("%d %d %d %d %d \r\n",readBuf[0],readBuf[1],readBuf[2],readBuf[3],readBuf[4]);
	 if(pointsNum==0 && readBuf[1]==0x22 && readBuf[2]==0xAA && readBuf[3]==0xBB && readBuf[4]==0x33){
			pointsNum = readBuf[0];
			label = 1;
			char str[20];
			sprintf(str,"%d",pointsNum);
			oled_p8x16str(1,0,str);
	 }
	 if(label <= pointsNum+1)
	{
		if(readBuf[0]==label && readBuf[1]==0x66 && readBuf[4]==0x77)
		{ // need to be change
			 pointsArray[label-1][0] = readBuf[2];
			 pointsArray[label-1][1] = readBuf[3];
			 PRINTF("(%d,%d)\r\n",pointsArray[label-1][0],pointsArray[label-1][1]);
			 label++;
		}
	}
	 if(label == pointsNum+1 && pointsNum != 0){
		return true;
	 }
	 else 
		return false;
	}


void top_receive_spi_callback_io()
{
		uint8_t readBuf[5];
		static uint8_t writeBuf[5] = {0,0,0,0,21};
		static int count = 0;
//		if(pointsNum == now_point)
//				return;
	#ifdef NewMainBoard
		spislaveReadWrite(B21,B14,B15,B16,readBuf,writeBuf,5);
#else
		spislaveReadWrite(C27,C20,C21,C22,readBuf,writeBuf,5);
#endif
//		PRINTF("%d %d %d %d %d \r\n",readBuf[0],readBuf[1],readBuf[2],readBuf[3],readBuf[4]);
		if(can_adjust)
		{
			if(readBuf[0]==0x56&&readBuf[1]==0x64&&readBuf[4]==0x78)
			{
					x_Error = (char)readBuf[2];
					y_Error = (char)readBuf[3];
					priority = 1;
					if(!Within_angle(25))
					{
						turn_angle(closeToImg_turn(final_points));
					}
					if(abs(x_Error)<5&&abs(y_Error)<5)
					{
						count++;
					}
					else 
						count =0;
					if(count>10)
					{
						writeBuf[4] = 22;
						int refresh_x;
						int refresh_y;
						count = 0;
						adjust = false;
						switch(final_points[now_point].direction)
						{
							case 0:
							{	refresh_x = final_points[now_point].x_point+x_Error;
								refresh_y = final_points[now_point].y_point+y_Error;;
								break;}
							case 1:
							{	refresh_x = final_points[now_point].x_point-x_Error;
								refresh_y = final_points[now_point].y_point-y_Error;
								break;}
							case 2:
							{		
								refresh_x = final_points[now_point].x_point-y_Error;
								refresh_y = final_points[now_point].y_point+x_Error;
								break;}
							case 3:
							{	refresh_x = final_points[now_point].x_point+y_Error;
								refresh_y = final_points[now_point].y_point-x_Error; 
								break;}
							default:
							{	refresh_x = final_points[now_point].x_point;
								refresh_y = final_points[now_point].y_point;
								break;}
						}
						position_point.x_point = Centimeter_to_encoder(refresh_x);
						position_point.y_point = Centimeter_to_encoder(refresh_y);
						clear_All_pid(1);
						Encoder_reinit();
						x_Error = 0;y_Error = 0;
						if(now_point>pointsNum)
						{
							now_point = pointsNum;
						}
					}
			}		
//			else if(readBuf[0]==0x56&&readBuf[1]==0x64&&readBuf[4]==0x55)		//接收动物帧
//				{
//						writeBuf[4]=21;
//						can_adjust = false;
//						adjust = false;
//						x_Error = 0;
//						y_Error = 0;
//						clear_All_pid(1);
//						Encoder_reinit();
//				}
		}
		else return;
	}

	
void XY_Camera_handle()
{	
	static int count = 0;
	if(!(can_adjust&&adjust))
		return;
	else
	{
			char x_Err=0,y_Err=0;
			x_Err<<=1;
			if(fast_gpio_get(C5))	x_Err+=1;
			x_Err<<=1;
			if(fast_gpio_get(C6))	x_Err+=1;
			x_Err<<=1;
			if(fast_gpio_get(C7))	x_Err+=1;
			x_Err<<=1;
			if(fast_gpio_get(C8))	x_Err+=1;
//			if(x_Err>7)
//				x_Err-=8;
			//x_Err = XY_data_handle(x_Err);
			y_Err<<=1;
			if(fast_gpio_get(C9))	y_Err+=1;
			y_Err<<=1;
			if(fast_gpio_get(C10))	y_Err+=1;
			y_Err<<=1;
			if(fast_gpio_get(C11))	y_Err+=1;
			y_Err<<=1;
			if(fast_gpio_get(C12))	y_Err+=1;
//		if(y_Err>7)
//			y_Err-=8;
			//y_Err = XY_data_handle(y_Err);
			x_Error = x_Err;
			y_Error = y_Err;
//			PRINTF("x=%d  y=%d \r\n",x_Error,y_Error);
			if(!Within_angle(25))
			{
				turn_angle(closeToImg_turn(final_points));
			}
			if(abs(x_Error)<5&&abs(y_Error)<5)
			{
				count++;
			}
			else 
				count =0;
			if(count>10)
			{
				int refresh_x;
				int refresh_y;
				count = 0;
				adjust = false;
				switch(final_points[now_point].direction)
				{
					case 0:
					{	refresh_x = final_points[now_point].x_point+x_Error;
						refresh_y = final_points[now_point].y_point+y_Error;;
						break;}
					case 1:
					{	refresh_x = final_points[now_point].x_point-x_Error;
						refresh_y = final_points[now_point].y_point-y_Error;
						break;}
					case 2:
					{		
						refresh_x = final_points[now_point].x_point-y_Error;
						refresh_y = final_points[now_point].y_point+x_Error;
						break;}
					case 3:
					{	refresh_x = final_points[now_point].x_point+y_Error;
						refresh_y = final_points[now_point].y_point-x_Error; 
						break;}
					default:
					{	refresh_x = final_points[now_point].x_point;
						refresh_y = final_points[now_point].y_point;
						break;}
				}
				position_point.x_point = Centimeter_to_encoder(refresh_x);
				position_point.y_point = Centimeter_to_encoder(refresh_y);
				clear_All_pid(1);
				Encoder_reinit();
				x_Error = 0;y_Error = 0;
				if(now_point>pointsNum)
				{
					now_point = pointsNum;
				}
			}
	}
}









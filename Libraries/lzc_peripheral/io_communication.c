#include "io_communication.h"

char  XY_data_handle(char data){
	if(data<4)
		return data;
	else if(data==4)
		return 5;
	else if(data==5)
		return 8;
	else if(data==6)
		return 15;
	else if(data==7)
		return 25;
	return 0;
}

uint8 Top_camera_getData()
{
	uint8 data = 0;
	data<<=1;
	if(gpio_get(B14))	data+=1;
	data<<=1;
	if(gpio_get(B15))	data+=1;
	data<<=1;
	if(gpio_get(B16))	data+=1;
	data<<=1;
//	if(gpio_get(B21))	data+=1;
//	data<<=1;
	if(gpio_get(B22))	data+=1;
	return data;
}

void Top_camera_In_config()
{
	gpio_dir(B23,GPI);
	gpio_dir(B24,GPI);
}

void Top_camera_Out_config()
{
	gpio_dir(B23,GPO);
	gpio_dir(B24,GPO);
}


	
void Top_camera_sendData(uint8 data)
{
	gpio_set(B25,1);
	systick_delay_ms(50);
	if(data==1)
	{
		gpio_set(B23,1);
		gpio_set(B24,0);
	}
	else if(data==2)
	{
		gpio_set(B23,0);
		gpio_set(B24,1);
	}
	else if(data==3)
	{
		gpio_set(B23,1);
		gpio_set(B24,1);
	}
	else
	{
		gpio_set(B23,0);
		gpio_set(B24,0);
	}
	gpio_set(B25,0);
}

char *bottom_camera_getX_Y()
{
	char *array;
	char data = 0;
	data<<=1;
	if(gpio_get(C5))	data+=1;
	data<<=1;
	if(gpio_get(C6))	data+=1;
	data<<=1;
	if(gpio_get(C7))	data+=1;
	data<<=1;
	if(gpio_get(C8))	data+=1;
	if(data>7)
		data-=8;
	data = XY_data_handle(data);
	*array = data;
	data = 0;
	data<<=1;
	if(gpio_get(C9))	data+=1;
	data<<=1;
	if(gpio_get(C10))	data+=1;
	data<<=1;
	if(gpio_get(C11))	data+=1;
	data<<=1;
	if(gpio_get(C12))	data+=1;
	if(data>7)
		data-=8;
	data = XY_data_handle(data);
	*array = data;
	return array;
}




void communication_Init()
{
		gpio_init(B14,GPI,0,GPIO_PIN_CONFIG);//顶部摄像头引脚初始化
		gpio_init(B15,GPI,0,GPIO_PIN_CONFIG);
		gpio_init(B16,GPI,0,GPIO_PIN_CONFIG);
		gpio_init(B21,GPI,0,GPIO_PIN_CONFIG);
		gpio_init(B22,GPI,0,GPIO_PIN_CONFIG);
		gpio_init(B23,GPO,0,GPIO_PIN_CONFIG);
		gpio_init(B24,GPO,0,GPIO_PIN_CONFIG);
		gpio_init(B25,GPO,0,GPIO_PIN_CONFIG);
		gpio_interrupt_init(B26,RISING,GPIO_INT_CONFIG);
		
		//底部摄像头引脚初始化
		fast_gpio_init(C5,GPI,1,FAST_GPIO_PIN_CONFIG);
		fast_gpio_init(C6,GPI,1,FAST_GPIO_PIN_CONFIG);
		fast_gpio_init(C7,GPI,1,FAST_GPIO_PIN_CONFIG);
		fast_gpio_init(C8,GPI,1,FAST_GPIO_PIN_CONFIG);
		fast_gpio_init(C9,GPI,1,FAST_GPIO_PIN_CONFIG);
		fast_gpio_init(C10,GPI,1,FAST_GPIO_PIN_CONFIG);
		fast_gpio_init(C11,GPI,1,FAST_GPIO_PIN_CONFIG);
		fast_gpio_init(C12,GPI,1,FAST_GPIO_PIN_CONFIG);
		gpio_interrupt_init(C13,FALLING,GPIO_INT_CONFIG);
	
		NVIC_SetPriority(GPIO1_Combined_16_31_IRQn,6); //设置顶部摄像头通信的中断优先级
		NVIC_SetPriority(GPIO2_Combined_0_15_IRQn,6); //设置底部摄像头通信的中断优先级
}	
	




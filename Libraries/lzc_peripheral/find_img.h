#ifndef FIND_IMG_H
#define FIND_IMG_H

#include "headfile.h"

extern unsigned char priority;
extern char classify_status;
extern uint8_t SmallClass;
extern uint8_t BigClass;

void find_block_init();
bool receive_Points_Position_single();
void top_receive_spi_callback_io();
void bottom_receive_spi_callback_io();
void XY_Camera_handle();
	
#endif
#ifndef _IO_COMMUNICATION_h
#define _IO_COMMUNICATION_h
#include "headfile.h"

void communication_Init();
uint8 Top_camera_getData();
void Top_camera_In_config();
void Top_camera_Out_config();
void Top_camera_sendData(uint8 data);
void Bottom_canera_In_config();
void Bottom_canera_Out_config();
void Bottom_camera_sendData(uint8 data);
char *bottom_camera_getX_Y();
char XY_data_handle(char data);

#endif
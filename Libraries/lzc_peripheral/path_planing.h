#ifndef __PATH_PLANING_H
#define __PATH_PLANING_H

#include "headfile.h"

extern unsigned char pointsNum;
extern int pointsArray[20][2];
extern bool Task_finish;
extern Target_points *final_points;
extern bool Ready;
extern unsigned char now_point;
extern bool time_Stop;
extern uint32_t time1;

void pointsInit(Target_points *point);
bool go_to_pointsArray(Target_points points[],int n);
int Centimeter_to_encoder(float raw);
float Encoder_to_centimeter(int raw);
void OLED_display();

#endif
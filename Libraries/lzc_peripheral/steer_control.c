#include "steer_control.h"


/**************************************************************************
函数功能：提供舵机的运作的初始化
入口参数：无
返回  值：无
使用说明：在main函数里面调用一次即可
**************************************************************************/
void steer_init()
{
		pwm_init(steer_one,50,0);
		pwm_init(steer_two,50,0);
		pwm_init(steer_three,50,0);//舵机初始化
}

//-------------------------------------------------------------------------------------------------------------------
//  @brief      舵机旋转角度设置
//  @param      whichsteer      选择舵机：firststeer,secondsteer,thirdsteer
//  @param      angle           设置的旋转角度
//  @param      maxAngle    		舵机的最大角度（由舵机本身参数决定，只能选择180°和270°）
//  @return     void
//  Sample usage:              turn_Angle(firststeer,90,270)   270°舵机旋转到90°位置
//-------------------------------------------------------------------------------------------------------------------
void turn_Angle(MySteer Whichsteer,int angle,int maxAngle)
{
	switch(Whichsteer)
	{
			
		case firststeer:
		{
			if(maxAngle==180)
			{
				if(angle<181&&angle>-1)
				{
					pwm_duty(steer_one,(angle*10/9+50)*25);
				}
			}
			else if(maxAngle==270)
			{
				if(angle<271&&angle>-1)
				{
					pwm_duty(steer_one,(angle*20/27+50)*25);
				}
			}
			else return;
		}
		break;	
		case secondsteer:
		{
			if(maxAngle==180)
			{
				if(angle<181&&angle>-1)
				{
					pwm_duty(steer_two,(angle*10/9+50)*25);
				}
			}
			else if(maxAngle==270)
			{
				if(angle<271&&angle>-1)
				{
					pwm_duty(steer_two,(angle*20/27+50)*25);
				}
			}
			else return;
		}
		break;	
		case thirdsteer:
		{
			if(maxAngle==180)
			{
				if(angle<181&&angle>-1)
				{
					pwm_duty(steer_three,(angle*10/9+50)*25);
				}
			}
			else if(maxAngle==270)
			{
				if(angle<271&&angle>-1)
				{
					pwm_duty(steer_three,(angle*20/27+50)*25);
				}
			}
			else return;
		}
		break;
		default: assert(0);
			break;
	}
}

		//吸附
void getImg()
{
		gpio_set(C18,1);
		turn_Angle(secondsteer,100,270);//舵机二归位	C31
		systick_delay_ms(500);
		turn_Angle(firststeer,40,270);//舵机一归位		C30
		turn_Angle(secondsteer,120,270);//舵机二归位	C31
		systick_delay_ms(700);
		turn_Angle(secondsteer,145,270);//舵机二归位	C31
}

void putImg(){
		turn_Angle(secondsteer,60,270);//舵机二归位		C31
		turn_Angle(firststeer,215,270);//舵机一归位		C30
		systick_delay_ms(400);
		turn_Angle(secondsteer,128,270);//舵机二归位		C31
		systick_delay_ms(800);
		gpio_set(C18,0);
}


#include "path_planing.h"


unsigned char pointsNum = 0;
unsigned char now_point = 0;
int pointsArray[20][2];
bool Task_finish = false;
bool Ready = false;
Target_points *final_points;


bool time_Stop = true;
uint32_t time1 = 0;


/**************************************************************************
函数功能：Target_points结构体初始化
入口参数：目标点结构体数组
返回  值：无
**************************************************************************/
void pointsInit(Target_points *point){
	for(int i = 0;i<20;i++)				
	{
		point[i].x_point = 0;
		point[i].y_point = 0;
		point[i].direction = -1;
		point[i].status = false;
		point[i].distance = 0;
		point[i].angle = 0;
	}
}

/**************************************************************************
函数功能：依照数组的顺序依次走向目标点
入口参数：目标点数组,点个数
返回  值：无
使用说明：float pointsArray[4][2]={{10000,10000},{40000,30000},{20000,40000},{10000,20000}};
					points = transform_pointArray(pointsArray,4);
					此时points及为结构体数组，可以直接带入其他路径规划的函数中
**************************************************************************/
bool go_to_pointsArray(Target_points points[],int n)
{
	
	//暴力求解最优算法
	int tar_x[20] = {0};
	int tar_y[20] = {0};
	
	//在最后添加上原点
	final_points[pointsNum].x_point = 0;
	final_points[pointsNum].y_point = 0;
	final_points[pointsNum].direction = 0;
	final_points[pointsNum].distance = 0;
	
	for(int i = 0;i<pointsNum+1;i++)
	{
	PRINTF("x=%d y=%d\r\n",final_points[i].x_point,final_points[i].y_point);
		switch(final_points[i].direction)
		{
			case 0:
			{final_points[i].y_point =  final_points[i].y_point*20 - CameraMiddle_to_axis;
				final_points[i].x_point *= 20;break;}
			case 1:
			{final_points[i].y_point = final_points[i].y_point*20 + CameraMiddle_to_axis;
				final_points[i].x_point *= 20;break;}
			case 2:
			{final_points[i].x_point = final_points[i].x_point*20 + CameraMiddle_to_axis;
			final_points[i].y_point *= 20;break;}
			case 3:
			{final_points[i].x_point = final_points[i].x_point*20 - CameraMiddle_to_axis;
			final_points[i].y_point *= 20;break;}
			default:
				break;
		}	
//		PRINTF("x=%d y=%d dir=%d\r\n",final_points[i].x_point,final_points[i].y_point,final_points[i].direction);
		tar_x[i] = Centimeter_to_encoder(final_points[i].x_point);
		tar_y[i] = Centimeter_to_encoder(final_points[i].y_point);
		if(i==0)
			final_points[i].distance = sqrt_32_plus(tar_x[0],tar_y[0]);
		else
			final_points[i].distance = sqrt_32_plus(tar_x[i]-tar_x[i-1],tar_y[i]-tar_y[i-1]);
	}
		//开启定时器
		pit_interrupt_ms(PIT_CH2,100);//串口数据初始化
		pit_interrupt_ms(PIT_CH1,10);//icm20602的定时器中断，如果没有接icm20602就开启定时器的话会导致mcu锁死
		pit_interrupt_ms(PIT_CH0,5);//电机的编码器计数+启动中断初始化
		NVIC_SetPriority(PIT_IRQn,7); //设置定时器中断通信的中断优先级7
		communication_Init();
	uint8_t writeBuf = 0;
	//主任务
	for(int i = 0;i<pointsNum+1;i++)
	{
		if(i==pointsNum)			//返回原点
		{			
			first_turn();
			while(turn);
			motol_Settarget_point_Spd(tar_x[i],tar_y[i],final_points[i].distance);
			while(!go_End);
			second_turn();
			while(turn);					//等待转弯结束
		}
		else									//逐个点任务
		{
			if(now_point==0&&final_points[0].direction==0)
			{
				motol_Settarget_point_move(tar_x[i],tar_y[i],final_points[i].distance);
				while(!go_End);
			}
			else if(now_point!=0&&final_points[i].direction==final_points[i-1].direction)
			{
				motol_Settarget_point_move(tar_x[i]-tar_x[i-1],tar_y[i]-tar_y[i-1],final_points[i].distance);
				while(!go_End);
			}
			else
			{
					first_turn();
					while(turn);
					motol_Settarget_point_Spd(tar_x[i],tar_y[i],final_points[i].distance);
					while(!go_End);
					second_turn();
					while(turn);																			//等待转弯结束	
			}
				can_adjust = true;				//能够进入调整状态（用于判断中断是否正常接收数据）
				adjust = true;						//进入调整状态
				while(adjust);			//等待调整结束
				gpio_set(D4,0);			//关电机
				Top_camera_In_config();
				Top_camera_sendData(1);
				while(can_adjust);
				gpio_set(D4,1);			//打开电机
				now_point++;
		}
	}
	return true;
}



/**************************************************************************
函数功能：将编码值转化为厘米单位坐标、将厘米转化为编码值
入口参数：无
返回  值：无
*************************************************************************/
//将编码值转化为厘米单位坐标
float Encoder_to_centimeter(int raw)
{
	return raw*slope+intercept;
}

//将厘米转化为编码值
int Centimeter_to_encoder(float raw)
{
	return (raw-intercept)/slope;
}

/**************************************************************************
函数功能：oled显示状态
入口参数：无
返回  值：无
*************************************************************************/
void OLED_display()
{
	oled_fill(0);
	if(!go_End)
	{
		if(choose)		
			oled_p8x16str(1,0,"move");	
		else
			oled_p8x16str(1,0,"forward");
	}
	else if(turn)
		oled_p8x16str(1,0,"turn");
	else if(adjust&&can_adjust)
	{
		if(priority)
			oled_p8x16str(1,0,"top_adjust");
		else
			oled_p8x16str(1,0,"down_adjust");
	}
	else if(can_adjust&&!adjust)
		oled_p8x16str(1,0,"search");
	else 
		oled_p8x16str(1,0,"no State");
	char str2[20],str3[20],str4[20];
	sprintf(str2,"x_p = %5d",position_point.x_point);
	sprintf(str3,"y_p = %5d",position_point.y_point);
//	sprintf(str4,"ang = %3f",angle[2]);
	oled_p8x16str(1,2,str2);
	oled_p8x16str(1,4,str3);
//	oled_p8x16str(1,2,str4);
}






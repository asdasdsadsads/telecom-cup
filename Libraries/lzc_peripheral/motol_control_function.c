#include "motol_control_function.h"

my_pid_handle Adj_left_front_pid;			//调整外环		
my_pid_handle Adj_left_rear_pid;
my_pid_handle Adj_right_rear_pid;
my_pid_handle Adj_right_front_pid;			
my_pid_handle angle_position_pid;		//角度外环
my_pid_handle Forword_left_rear_pid;//直走外环
my_pid_handle Forword_left_front_pid;
my_pid_handle Forword_right_rear_pid;
my_pid_handle Forword_right_front_pid; 

float angle_err;
int angle_speed = 0;   //角度内环速度
int L_S_Spd = 0;				//调整内环速度
int L_X_Spd = 0;
int R_S_Spd = 0;
int R_X_Spd = 0;
int L_S_forward_Spd = 0;		//直走速度
int L_X_forward_Spd = 0;
int R_S_forward_Spd = 0;
int R_X_forward_Spd = 0;
int forward_Distance = 0;

/**************************************************************************
函数功能：提供电机的运作的初始化(外环)
入口参数：无
返回  值：无
使用说明：在main函数里面调用一次即可
**************************************************************************/
void Outring_PID_init()
{
	PID_init(&Adj_left_rear_pid,0.015,0,2); 
	PID_init(&Adj_right_front_pid,0.015,0,2);
	PID_init(&Adj_right_rear_pid,0.015,0,2);
	PID_init(&angle_position_pid,60,0,1150);								//角度外环句柄	
	PID_init(&Adj_left_front_pid,0.015,0,2);							//调整外环句柄						
	PID_init(&Forword_left_front_pid,0.037,0,2);							//直走外环句柄
	PID_init(&Forword_left_rear_pid,0.015,0,1.5);
	PID_init(&Forword_right_front_pid,0.037,0,2);	
	PID_init(&Forword_right_rear_pid,0.015,0,1.5);	
}


/**************************************************************************
函数功能：走到某一角度点
入口参数：定点x,y的值
返回  值：无
使用说明：该函数在motol_start中的位置PID中调用,无需理会
**************************************************************************/
void Position_position_handle()
{
	static unsigned char count = 0;
	int average = get_forward_average();
	if((abs(average-4*forward_Distance)<abs(forward_Distance)/10))//30，误差为30分之一
		count++;
	else
		count = 0;
	if(count>25)
	{		
		go_End = true;						//车结束点标志位
		motol_Settarget_position(0,0,0,0);
		x_Error = 0;
		y_Error = 0;
		priority = 0;
		clear_All_pid(0);
	}
}

/**************************************************************************
函数功能：角度控制的外环
入口参数：定点x,y的值
返回  值：无
使用说明：该函数在motol_start中的位置PID中调用,无需理会
**************************************************************************/
void Angle_position_handle()
{
	static int count = 0;
	angle_err = (angle[2]-Target_angle);
	if(angle_err>180)
		angle_err = (angle[2]-360-Target_angle);
	else if(angle_err<-180)
		angle_err = (angle[2]+360-Target_angle);
	angle_speed = f_Position_PID_bias(&angle_position_pid,-angle_err);				//角度外环计算结果反馈到内环中
	angle_speed = Motolxianfu(angle_speed,500);
//	PRINTF("%3f,tar=%3f\r\n",-angle_err,Target_angle);
}

/**************************************************************************
函数功能：调整控制的外环
入口参数：定点x,y的值
返回  值：无
使用说明：该函数在motol_start中的位置PID中调用,无需理会
**************************************************************************/
void Adj_position_handle()
{
		int error = (angle[2]-Target_angle)*6;
		if(error>1040)
			error = (angle[2]-360-Target_angle)*6;
		else if(error<-1040)
			error = (angle[2]+360-Target_angle)*6;
		int Tar_adj_L_SE = Centimeter_to_encoder(y_Error + x_Error);
		int Tar_adj_L_XE = Centimeter_to_encoder(y_Error - x_Error);
		int Tar_adj_R_SE = Centimeter_to_encoder(y_Error - x_Error);
		int Tar_adj_R_XE = Centimeter_to_encoder(y_Error + x_Error);
		L_S_Spd = Position_PID_bias(&Adj_left_front_pid,Tar_adj_L_SE-error);
		L_X_Spd = Position_PID_bias(&Adj_left_rear_pid,Tar_adj_L_XE-error);
		R_S_Spd = Position_PID_bias(&Adj_right_front_pid,Tar_adj_R_SE+error);
		R_X_Spd = Position_PID_bias(&Adj_right_rear_pid,Tar_adj_R_XE+error);
		L_S_Spd = Motolxianfu(L_S_Spd,110);
		L_X_Spd = Motolxianfu(R_S_Spd,110);
		R_S_Spd = Motolxianfu(L_X_Spd,110);
		R_X_Spd = Motolxianfu(R_X_Spd,110);	
}

/**************************************************************************
函数功能：直走控制的外环
入口参数：定点x,y的值
返回  值：无
使用说明：该函数在motol_start中的位置PID中调用,无需理会
**************************************************************************/
void Forward_position_handle()
{
//		char L_Xstr[20],L_Sstr[20],R_Sstr[20],R_Xstr[20];
//		sprintf(L_Xstr,"L_S = %5d",get_forward_doublepid_signle(0));
//		sprintf(L_Sstr,"L_X = %5d",get_forward_doublepid_signle(1));
//		sprintf(R_Sstr,"R_S = %5d",get_forward_doublepid_signle(2));
//		sprintf(R_Xstr,"R_X = %5d",get_forward_doublepid_signle(3));
//		oled_p8x16str(1,0,L_Sstr);
//		oled_p8x16str(1,2,L_Xstr);
//		oled_p8x16str(1,4,R_Sstr);
//		oled_p8x16str(1,6,R_Xstr);
	static unsigned char count = 0;	
	int absaverage = abs(Tar_LS_RX)+abs(Tar_LX_RS);
	if(abs(get_forward_average_abs()-absaverage*2)<absaverage/15)//30，误差为30分之一
		count++;
	else
		count = 0;
//	PRINTF("ave=%d end=%d con%d\r\n",abs(get_forward_average_abs()-absaverage*2),absaverage/30,count);
	if(count>25)
	{		
		go_End = true;						//车结束点标志位
		x_Error = 0;
		y_Error = 0;
		priority = 0;
		clear_All_pid(3);
	}
	L_S_forward_Spd = Position_PID(&Forword_left_front_pid,get_forward_doublepid_signle(0),Tar_LS_RX);
	L_X_forward_Spd = Position_PID(&Forword_left_rear_pid,get_forward_doublepid_signle(1),Tar_LX_RS);
	R_S_forward_Spd = Position_PID(&Forword_right_front_pid,get_forward_doublepid_signle(2),Tar_LX_RS);
	R_X_forward_Spd = Position_PID(&Forword_right_rear_pid,get_forward_doublepid_signle(3),Tar_LS_RX);
	L_S_forward_Spd = Motolxianfu(L_S_forward_Spd,Con_LS_RX);
	L_X_forward_Spd = Motolxianfu(L_X_forward_Spd,Con_LX_RS);
	R_S_forward_Spd = Motolxianfu(R_S_forward_Spd,Con_LX_RS);
	R_X_forward_Spd = Motolxianfu(R_X_forward_Spd,Con_LS_RX);

//	NimingOutput_PosSped_pid(get_forward_doublepid_signle(0),get_forward_doublepid_signle(1),get_forward_doublepid_signle(2),get_forward_doublepid_signle(3));
}


/**************************************************************************
函数功能：提供PWM设定（速度或者位置的编码值）
入口参数：四个轮子的PWM值，限幅的最大值
返回  值：choose，为1代表使用速度编码值，为0代表使用位置编码值
使用说明：该函数在定时器0中使用（暂时设置每隔5ms使用一次,可以根据情况调整）
**************************************************************************/
void motol_allSetpwm(int left_front,int left_rear,int right_front,int right_rear,int max)
{
	
	left_front = Motolxianfu(left_front,max);
	right_front = Motolxianfu(right_front,max);
	left_rear = Motolxianfu(left_rear,max);
	right_rear = Motolxianfu(right_rear,max);
		
#ifdef	DIR_MOTOL
	if(left_front>0) 	{gpio_set(D0,1);}
	else             	{gpio_set(D0,0);}
	if(left_rear>0)  	{gpio_set(D2,1);}
	else         			{gpio_set(D2,0);}
  if(right_front<0)	{gpio_set(D12,1);}
	else            	{gpio_set(D12,0);}	
	if(right_rear<0)  {gpio_set(D14,1);}
	else            	{gpio_set(D14,0);}
	pwm_duty(PWM1_MODULE3_CHB_D1,abs(left_front));
	pwm_duty(PWM2_MODULE3_CHB_D3,abs(left_rear));
	pwm_duty(PWM1_MODULE0_CHB_D13,abs(right_front));
	pwm_duty(PWM1_MODULE1_CHB_D15,abs(right_rear));
#else
	if(left_front>0) 	{pwm_duty(PWM1_MODULE3_CHB_D1,abs(left_front));pwm_duty(PWM1_MODULE3_CHA_D0,0);}
	else             	{pwm_duty(PWM1_MODULE3_CHA_D0,abs(left_front));pwm_duty(PWM1_MODULE3_CHB_D1,0);}
	
	if(left_rear>0)  	{pwm_duty(PWM2_MODULE3_CHB_D3,abs(left_rear));pwm_duty(PWM2_MODULE3_CHA_D2,0);}
	else         			{pwm_duty(PWM2_MODULE3_CHA_D2,abs(left_rear));pwm_duty(PWM2_MODULE3_CHB_D3,0);}
	
  if(right_front<0)	{pwm_duty(PWM1_MODULE0_CHB_D13,abs(right_front));pwm_duty(PWM1_MODULE0_CHA_D12,0);}
	else            	{pwm_duty(PWM1_MODULE0_CHA_D12,abs(right_front));pwm_duty(PWM1_MODULE0_CHB_D13,0);}
	
	if(right_rear<0)  {pwm_duty(PWM1_MODULE1_CHB_D15,abs(right_rear));pwm_duty(PWM1_MODULE1_CHA_D14,0);}
	else            	{pwm_duty(PWM1_MODULE1_CHA_D14,abs(right_rear));pwm_duty(PWM1_MODULE1_CHB_D15,0);}
#endif

}

/**************************************************************************
函数功能：判断车身在目标角度附近的某个角度之内
入口参数：需要在内的角度
返回  值：无
使用说明：用户不必理会
**************************************************************************/
bool Within_angle(float within_angle)
{
	if((angle[2]-Target_angle)>180)
	{
		if(abs(abs(angle[2]-Target_angle-360)-within_angle)){
			return true;
		}
		else 
			return false;
	}
	else if((angle[2]-Target_angle)<-180)
	{
		if(abs(abs(angle[2]-Target_angle+360)-within_angle)){
			return true;
		}
		else 
			return false;
	}
	else 
	{
		if(abs(angle[2]-Target_angle)<within_angle)
		{
			return true;
		}
		else 
			return false;
	}
}

/**************************************************************************
函数功能：计算转弯角度
入口参数：结构体数组点集
返回  值：返回的角度
使用说明：用户不必理会
**************************************************************************/
float closeToImg_turn(Target_points *points)
{
	switch(final_points[now_point].direction)
	{
		case 0:
		{return 0;break;}
		case 1:
		{return 180;break;}
		case 2:
		{return 270; break;}
		case 3:
		{return 90; break;}
		default:
			return 0;
		break;
	}
}

/**************************************************************************
函数功能：提供电机驱动需要的下限PWM值
入口参数：,原始数据，下限值
返回  值：无
**************************************************************************/
int lower_limit(int raw_data,int lower_data)
{
	int data;
	if(raw_data<0)
		data = raw_data-lower_data;
	else if(raw_data>0)
		data = raw_data+lower_data;
	else 
		data = 0;
	return data;
}


/**************************************************************************
函数功能：限幅函数
入口参数：原始数据，限幅范围
返回  值：限幅后的值
**************************************************************************/
int Motolxianfu(int num,int max)
{
	if(num>max)
		return max;
	else if(num<-max)
		return -max;
	else 
		return num;
}

/**************************************************************************
函数功能：清除四个轮子句柄中的编码器值(外环)
入口参数：无
返回  值：无
使用说明：用户不必理会
**************************************************************************/
void clear_All_Outer_pid(uint8 choose)
{
	if(choose == 0)				
	{
		pid_reinit(&Forword_left_front_pid);
		pid_reinit(&Forword_left_rear_pid);
		pid_reinit(&Forword_right_front_pid);
		pid_reinit(&Forword_right_rear_pid);
	}
	else if(choose == 1)
	{
		pid_reinit(&Adj_left_front_pid);
		pid_reinit(&Adj_right_front_pid);
		pid_reinit(&Adj_left_rear_pid);
		pid_reinit(&Adj_right_rear_pid);
	}
	else if(choose == 2)//角度环清除
	{
		pid_reinit(&angle_position_pid);
	}
}


void PID_FORWORD_INNER_TEXT()
{
	motol_Settarget_point_move(150000,150000,150000);
}

#include "motol_control.h"

my_pid_handle left_front_inner_pid;
my_pid_handle left_rear_inner_pid;
my_pid_handle right_rear_inner_pid;
my_pid_handle right_front_inner_pid;			//直走速度内环
my_pid_handle left_front_pid;
my_pid_handle left_rear_pid;
my_pid_handle right_rear_pid;
my_pid_handle right_front_pid;			
my_pid_handle Adj_left_front_Spd_pid;
my_pid_handle Adj_left_rear_Spd_pid;
my_pid_handle Adj_right_rear_Spd_pid;
my_pid_handle Adj_right_front_Spd_pid;			//调整速度内环
my_pid_handle angle_left_front_pid;
my_pid_handle angle_left_rear_pid;	
my_pid_handle angle_right_front_pid;	
my_pid_handle angle_right_rear_pid;	//角度速度内环
my_pid_handle position_pid;

encoder_point position_point;//车身所在的编码值位置点（单位位编码值）

//编码器低通滤波器
filter_t low_filter_L_S;
filter_t low_filter_R_S;
filter_t low_filter_L_X;
filter_t low_filter_R_X;


/**************************************************************************
函数功能：提供电机的运作的初始化
入口参数：无
返回  值：无
使用说明：在main函数里面调用一次即可
**************************************************************************/
void motol_init()
{
#ifdef DIR_MOTOL
	gpio_init(D0,GPO,0,GPIO_PIN_CONFIG);
	gpio_init(D2,GPO,0,GPIO_PIN_CONFIG);
	gpio_init(D12,GPO,0,GPIO_PIN_CONFIG);
	gpio_init(D14,GPO,0,GPIO_PIN_CONFIG);
#else
		pwm_init(PWM1_MODULE3_CHA_D0,17000,0);
		pwm_init(PWM2_MODULE3_CHA_D2,17000,0);
		pwm_init(PWM1_MODULE0_CHA_D12,17000,0);
		pwm_init(PWM1_MODULE1_CHA_D14,17000,0);
#endif
	//pwm定时器初始化
	pwm_init(PWM1_MODULE3_CHB_D1,17000,0);//左前
	pwm_init(PWM2_MODULE3_CHB_D3,17000,0);//左后
	pwm_init(PWM1_MODULE0_CHB_D13,17000,0);//右前
	pwm_init(PWM1_MODULE1_CHB_D15,17000,0);//右后
	
	//编码器初始化
  qtimer_quad_init(QTIMER_1,QTIMER1_TIMER0_C0,QTIMER1_TIMER1_C1);//左前
  qtimer_quad_init(QTIMER_1,QTIMER1_TIMER2_C2,QTIMER1_TIMER3_C24);//左后
  qtimer_quad_init(QTIMER_2,QTIMER2_TIMER0_C3,QTIMER2_TIMER3_C25);//右前
  qtimer_quad_init(QTIMER_3,QTIMER3_TIMER2_B18,QTIMER3_TIMER3_B19);//右后
	
	//低通滤波器初始化
	LowPassFilter_Init(&low_filter_L_S,200,35);
	LowPassFilter_Init(&low_filter_R_S,200,35);
	LowPassFilter_Init(&low_filter_R_X,200,35);
	LowPassFilter_Init(&low_filter_L_X,200,35);
	
	//pid各个参数初始化，可以在这里设置各个参数的值 左到右数字依次是kp，ki，kd
	PID_init(&left_front_inner_pid,25,0.3,0);										//直走内环句柄
	PID_init(&left_rear_inner_pid,25,0.3,0); 
	PID_init(&right_front_inner_pid,25,0.3,0);
	PID_init(&right_rear_inner_pid,25,0.3,0);

	PID_init(&left_front_pid,13,0,430);							//直走PID句
	PID_init(&left_rear_pid,10,0,150); 
	PID_init(&right_front_pid,12,0,470);
	PID_init(&right_rear_pid,10,0,150);
#ifdef Two_angle_pid
	PID_init(&angle_left_front_pid,60,0.4,0);							//转弯内环句柄
	PID_init(&angle_left_rear_pid,50,0.35,0);												
	PID_init(&angle_right_front_pid,80,0.5,0);												
	PID_init(&angle_right_rear_pid,50,0.3,0);				
#else
	PID_init(&angle_left_front_pid,340,0,11000);							//转弯句柄
	PID_init(&angle_left_rear_pid,280,0,8000);												
	PID_init(&angle_right_front_pid,280,0,8000);												
	PID_init(&angle_right_rear_pid,220,0,7000);		
	
#endif
	PID_init(&Adj_left_front_Spd_pid,7,0.9,0);							//调整内环句柄
	PID_init(&Adj_left_rear_Spd_pid,10,0.85,0);												
	PID_init(&Adj_right_front_Spd_pid,8,0.85,0);												
	PID_init(&Adj_right_rear_Spd_pid,7,0.9,0);				
	Outring_PID_init();
	
	position_point.x_point = 0;
	position_point.y_point = 0;
}

/**************************************************************************
函数功能：提供电机的运作（速度或者位置的编码值）
入口参数：无
返回  值：choose，为1代表使用速度编码值，为0代表使用位置编码值
使用说明：该函数在定时器0中使用（暂时设置每隔5ms使用一次,可以根据情况调整）
**************************************************************************/
void motol_start()
{
	int L_S=0,R_S=0,L_X=0,R_X=0;
	int L_SE=0,L_XE=0,R_SE=0,R_XE=0;
	int average=0,error=0;		//平均值和车身矫正直线偏差
	float angle_PI = 0;
	static uint8 count = 0;
  static uint8_t num = 0;
	static vector3_int encoder_vector[3];
  if(num > 2) num = 0;
	if(Task_finish)		
	{
		x_Error = 0;
		y_Error = 0;
		can_adjust = false;
		adjust = false;
		pwm_duty(PWM1_MODULE3_CHB_D1,0);pwm_duty(PWM1_MODULE3_CHA_D0,0);
		pwm_duty(PWM2_MODULE3_CHB_D3,0);pwm_duty(PWM2_MODULE3_CHA_D2,0);
		pwm_duty(PWM1_MODULE0_CHB_D13,0);pwm_duty(PWM1_MODULE0_CHA_D12,0);
		pwm_duty(PWM1_MODULE1_CHB_D15,0);pwm_duty(PWM1_MODULE1_CHA_D14,0);
		motol_Settarget_position(0,0,0,0);
		return;
	}
	else if(!Ready)
		return;
	if(turn) 							//判断是否处于转弯状态
	{	
#ifdef Two_angle_pid
			Angle_position_handle();				//角度的位置环
			if(fabs(angle_err)<1)
				count++;
			else 
				count = 0;
			if(count>10)
			{		
				turn = false;
				clear_All_pid(2);
			}
			L_SE = getEncoder(&angle_left_front_pid,left_front_encoder,1);
			L_XE = getEncoder(&angle_left_rear_pid,left_rear_encoder,1);
			R_SE = getEncoder(&angle_right_front_pid,right_front_encoder,1);
			R_XE = getEncoder(&angle_right_rear_pid,right_rear_encoder,1);
			L_S = Incremental_PI(&angle_left_front_pid,L_SE,angle_speed);
			L_X = Incremental_PI(&angle_left_rear_pid,L_XE,angle_speed);
			R_S = Incremental_PI(&angle_right_front_pid,R_SE,-angle_speed);
			R_X = Incremental_PI(&angle_right_rear_pid,R_XE,-angle_speed);
			motol_allSetpwm(L_S,L_X,R_S,R_X,10000);	
//			NimingOutput_AngleSped_pid(L_SE,L_XE,R_SE,R_XE,angle_speed);//发送给匿名上位机的通信协议
#else
		static int count = 0;
		float angle_err = (angle[2]-Target_angle);
		if(angle_err>180)
			angle_err = (angle[2]-360-Target_angle);
		else if(angle_err<-180)
			angle_err = (angle[2]+360-Target_angle);
		if(fabs(angle_err)<1)
			count++;
		else 
			count = 0;
		if(count>20)
		{		
			turn = false;
			clear_All_pid(2);
			return;
		}
		L_S = f_Position_PID_bias(&angle_left_front_pid,-angle_err);
		L_X = f_Position_PID_bias(&angle_left_rear_pid,-angle_err);
		R_S = f_Position_PID_bias(&angle_right_front_pid,angle_err);
		R_X = f_Position_PID_bias(&angle_right_rear_pid,angle_err);
		motol_allSetpwm(L_S+2000,L_X+2000,R_S+1800,R_X+1800,25000);	
		NimingOutput_Angle();
#endif	
	}
	else if(adjust)				//判断是否处于调整状态（寻找物体状态）
	{
			Adj_position_handle();						//调整状态的位置环
			L_SE = getEncoder(&Adj_left_front_Spd_pid,left_front_encoder,1);
			L_XE = getEncoder(&Adj_left_rear_Spd_pid,left_rear_encoder,1);
			R_SE = getEncoder(&Adj_right_front_Spd_pid,right_front_encoder,1);
			R_XE = getEncoder(&Adj_right_rear_Spd_pid,right_rear_encoder,1);
			L_S = Incremental_PI(&Adj_left_front_Spd_pid,L_SE,L_S_Spd);
			L_X = Incremental_PI(&Adj_left_rear_Spd_pid,L_XE,L_X_Spd);
			R_S = Incremental_PI(&Adj_right_front_Spd_pid,R_SE,R_S_Spd);
			R_X = Incremental_PI(&Adj_right_rear_Spd_pid,R_XE,R_X_Spd);
			motol_allSetpwm(L_S,L_X,R_S,R_X,20000);	
//			NimingOutput_AdjSped_pid(L_SE,L_XE,R_SE,R_XE,L_S_Spd,L_X_Spd,R_S_Spd,R_X_Spd);//发送给匿名上位机的通信协议
	}
	else if(!go_End)		//处于走直线状态
	{					
		angle_PI = angle[2]/RadtoDeg;
		if(choose)
		{	
			Forward_position_handle();
			L_SE = getEncoder(&left_front_inner_pid,left_front_encoder,0);
			L_XE = getEncoder(&left_rear_inner_pid,left_rear_encoder,0);
			R_SE = getEncoder(&right_front_inner_pid,right_front_encoder,0);
			R_XE = getEncoder(&right_rear_inner_pid,right_rear_encoder,0);
		
//			encoder_vector[num].L_S = L_SE;									 //取出编码器值并且进行低通滤波+均值滤波
//			encoder_vector[num].L_X = L_XE;
//			encoder_vector[num].R_S = R_SE;
//			encoder_vector[num].R_X = R_XE;
//			L_SE = (int)LowPassFilter_apply(&low_filter_L_S,(encoder_vector[0].L_S+encoder_vector[1].L_S+encoder_vector[2].L_S)/3); 
//			L_XE = (int)LowPassFilter_apply(&low_filter_L_X,(encoder_vector[0].L_X+encoder_vector[1].L_X+encoder_vector[2].L_X)/3); 
//			R_SE = (int)LowPassFilter_apply(&low_filter_R_S,(encoder_vector[0].R_S+encoder_vector[1].R_S+encoder_vector[2].R_S)/3); 
//			R_XE = (int)LowPassFilter_apply(&low_filter_R_X,(encoder_vector[0].R_X+encoder_vector[1].R_X+encoder_vector[2].R_X)/3); 

			//通过角度计算出xy编码坐标
//			average = (L_SE+L_XE+R_SE+R_XE)/4;
//			position_point.x_point += average*sinf(angle_PI);
//			position_point.y_point += average*cosf(angle_PI);	
			
			//车身偏差修正
			error = (angle[2]-Target_angle)*50;
			if(error>50*180)
				error = (angle[2]-360-Target_angle)*50;
			else if(error<50*-180)
				error = (angle[2]+360-Target_angle)*50;
			L_S=Incremental_PI(&left_front_inner_pid,L_SE,L_S_forward_Spd-error);
			L_X=Incremental_PI(&left_rear_inner_pid,L_XE,L_X_forward_Spd-error);
			R_S=Incremental_PI(&right_front_inner_pid,R_SE,R_S_forward_Spd+error);
			R_X=Incremental_PI(&right_rear_inner_pid,R_XE,R_X_forward_Spd+error);
		
			motol_allSetpwm(L_S+3000,L_X+3000,R_S+3000,R_X+3000,25000);	
//		char L_Xstr[20],L_Sstr[20],R_Sstr[20],R_Xstr[20];
//		sprintf(L_Xstr,"L_X = %5d",L_XE);
//		sprintf(L_Sstr,"L_S = %5d",L_SE);
//		sprintf(R_Sstr,"R_S = %5d",R_SE);
//		sprintf(R_Xstr,"R_X = %5d",R_XE);
//		oled_p8x16str(1,0,L_Sstr);
//		oled_p8x16str(1,2,L_Xstr);
//		oled_p8x16str(1,4,R_Sstr);
//		oled_p8x16str(1,6,R_Xstr);
//		NimingOutput_Speed(L_SE,L_XE,R_SE,R_XE);//发送给匿名上位机的通信协议
		}
		else
		{	
			Position_position_handle();
			//定位计算
			average = (getEncoder(&left_front_pid,left_front_encoder,0)+getEncoder(&left_rear_pid,left_rear_encoder,0)+getEncoder(&right_front_pid,right_front_encoder,0)+getEncoder(&right_rear_pid,right_rear_encoder,0))/4;
			position_point.x_point += average*sinf(angle_PI);
			position_point.y_point += average*cosf(angle_PI);	
					
			encoder_vector[num].L_S = target_left_front_position-left_front_pid.encoder;									 //取出编码器值并且进行低通滤波+均值滤波
			encoder_vector[num].L_X = target_left_rear_position-left_rear_pid.encoder;
			encoder_vector[num].R_S = target_right_front_position-right_front_pid.encoder;
			encoder_vector[num].R_X = target_right_rear_position-right_rear_pid.encoder;
			L_SE = (int)LowPassFilter_apply(&low_filter_L_S,(encoder_vector[0].L_S+encoder_vector[1].L_S+encoder_vector[2].L_S)/3); 
			L_XE = (int)LowPassFilter_apply(&low_filter_L_X,(encoder_vector[0].L_X+encoder_vector[1].L_X+encoder_vector[2].L_X)/3); 
			R_SE = (int)LowPassFilter_apply(&low_filter_R_S,(encoder_vector[0].R_S+encoder_vector[1].R_S+encoder_vector[2].R_S)/3); 
			R_XE = (int)LowPassFilter_apply(&low_filter_R_X,(encoder_vector[0].R_X+encoder_vector[1].R_X+encoder_vector[2].R_X)/3); 

			//计算pid
			L_S=Position_PID_bias(&left_front_pid,L_SE);
			L_X=Position_PID_bias(&left_rear_pid,L_XE);
			R_S=Position_PID_bias(&right_front_pid,R_SE);
			R_X=Position_PID_bias(&right_rear_pid,R_XE);	
			
			//车身偏差修正
			error = (angle[2]-Target_angle)*100;
			if(error>	18000)
				error = (angle[2]-360-Target_angle)*100;
			else if(error<-18000)
				error = (angle[2]+360-Target_angle)*100;

			int left_front = Motolxianfu(L_S,11000);
			int right_front = Motolxianfu(R_S,11000);
			int left_rear = Motolxianfu(L_X,11000);
			int right_rear = Motolxianfu(R_X,11000);
			
			if(left_front<abs(error)||right_front<abs(error)||left_rear<abs(error)||right_rear<abs(error))
				error = 0;			

		#ifdef	DIR_MOTOL
			if(left_front>0) 	{gpio_set(D0,1);}
			else             	{gpio_set(D0,0);}
			if(left_rear>0)  	{gpio_set(D2,1);}
			else         			{gpio_set(D2,0);}
			if(right_front<0)	{gpio_set(D12,1);}
			else            	{gpio_set(D12,0);}	
			if(right_rear<0)  {gpio_set(D14,1);}
			else            	{gpio_set(D14,0);}
			pwm_duty(PWM1_MODULE3_CHB_D1,abs(left_front-error));
			pwm_duty(PWM2_MODULE3_CHB_D3,abs(left_rear-error));
			pwm_duty(PWM1_MODULE0_CHB_D13,abs(right_front+error));
			pwm_duty(PWM1_MODULE1_CHB_D15,abs(right_rear+error));
		#else
			if(left_front>0) 	{pwm_duty(PWM1_MODULE3_CHB_D1,lower_limit(abs(left_front-error),0));pwm_duty(PWM1_MODULE3_CHA_D0,0);}
			else             	{pwm_duty(PWM1_MODULE3_CHA_D0,lower_limit(abs(left_front-error),0));pwm_duty(PWM1_MODULE3_CHB_D1,0);}
			
			if(left_rear>0)  	{pwm_duty(PWM2_MODULE3_CHB_D3,lower_limit(abs(left_rear-error),0));pwm_duty(PWM2_MODULE3_CHA_D2,0);}
			else         			{pwm_duty(PWM2_MODULE3_CHA_D2,lower_limit(abs(left_rear-error),0));pwm_duty(PWM2_MODULE3_CHB_D3,0);}
			
			if(right_front<0)	{pwm_duty(PWM1_MODULE0_CHB_D13,lower_limit(abs(right_front+error),0));pwm_duty(PWM1_MODULE0_CHA_D12,0);}
			else            	{pwm_duty(PWM1_MODULE0_CHA_D12,lower_limit(abs(right_front+error),0));pwm_duty(PWM1_MODULE0_CHB_D13,0);}
			
			if(right_rear<0)  {pwm_duty(PWM1_MODULE1_CHB_D15,lower_limit(abs(right_rear+error),0));pwm_duty(PWM1_MODULE1_CHA_D14,0);}
			else            	{pwm_duty(PWM1_MODULE1_CHA_D14,lower_limit(abs(right_rear+error),0));pwm_duty(PWM1_MODULE1_CHB_D15,0);}
			#endif
//			NimingOutput_Position(left_front_pid.encoder,left_rear_pid.encoder,right_front_pid.encoder,right_rear_pid.encoder);//发送给匿名上位机的通信协议
		}
		    num++;
	}
	else
	{
		pwm_duty(PWM1_MODULE3_CHB_D1,0);pwm_duty(PWM1_MODULE3_CHA_D0,0);
		pwm_duty(PWM2_MODULE3_CHB_D3,0);pwm_duty(PWM2_MODULE3_CHA_D2,0);
		pwm_duty(PWM1_MODULE0_CHB_D13,0);pwm_duty(PWM1_MODULE0_CHA_D12,0);
		pwm_duty(PWM1_MODULE1_CHB_D15,0);pwm_duty(PWM1_MODULE1_CHA_D14,0);
	}
#ifdef OLED_ENCODER    //OLED显示
		char L_Xstr[20],L_Sstr[20],R_Sstr[20],R_Xstr[20];
		sprintf(L_Xstr,"L_X = %5d",L_XE);
		sprintf(L_Sstr,"L_S = %5d",L_SE);
		sprintf(R_Sstr,"R_S = %5d",R_SE);
		sprintf(R_Xstr,"R_X = %5d",R_XE);
		oled_p8x16str(1,0,L_Sstr);
		oled_p8x16str(1,2,L_Xstr);
		oled_p8x16str(1,4,R_Sstr);
		oled_p8x16str(1,6,R_Xstr);
#endif
}

/**************************************************************************
函数功能：获取直走时四轮的编码值总和
入口参数：无
返回  值：无
使用说明：用户不必理会
**************************************************************************/
int get_forward_average()
{
	return left_front_pid.encoder+left_rear_pid.encoder+right_front_pid.encoder+right_rear_pid.encoder;
}

int get_forward_average_abs()
{
	return abs(left_front_inner_pid.encoder)+abs(left_rear_inner_pid.encoder)+abs(right_front_inner_pid.encoder)+abs(right_rear_inner_pid.encoder);
}

/**************************************************************************
函数功能：获取直走时四轮的单轮编码值(0，1，2，3分别代表左上，左下，右上，右下)
入口参数：无
返回  值：无
使用说明：用户不必理会
**************************************************************************/
int get_forward_signle(uint8_t choose)
{
	switch(choose)
	{
		case 0:
		{return left_front_pid.encoder;break;}
		case 1:
		{return left_rear_pid.encoder;break;}
		case 2:
		{return right_front_pid.encoder;break;}		
		case 3:
		{return right_rear_pid.encoder;break;}
		default :
			return 0;
	}		
	return 0;
}

int get_forward_doublepid_signle(uint8_t choose)
{
	switch(choose)
	{
		case 0:
		{return left_front_inner_pid.encoder;break;}
		case 1:
		{return left_rear_inner_pid.encoder;break;}
		case 2:
		{return right_front_inner_pid.encoder;break;}		
		case 3:
		{return right_rear_inner_pid.encoder;break;}
		default :
			return 0;
	}		
	return 0;
}

/**************************************************************************
函数功能：清除四个轮子句柄中的编码器值
入口参数：无
返回  值：无
使用说明：用户不必理会
**************************************************************************/
void clear_All_pid(uint8 choose)
{
	Encoder_reinit();
	if(choose == 0)				
	{
		pid_reinit(&left_front_pid);
		pid_reinit(&right_front_pid);
		pid_reinit(&left_rear_pid);
		pid_reinit(&right_rear_pid);
	}
	else if(choose == 1)
	{
		pid_reinit(&Adj_left_front_Spd_pid);
		pid_reinit(&Adj_right_front_Spd_pid);
		pid_reinit(&Adj_left_rear_Spd_pid);
		pid_reinit(&Adj_right_rear_Spd_pid);
		clear_All_Outer_pid(1);
	}
	else if(choose == 2)//角度环清除
	{
		pid_reinit(&angle_left_front_pid);
		pid_reinit(&angle_right_front_pid);
		pid_reinit(&angle_left_rear_pid);
		pid_reinit(&angle_right_rear_pid);
		clear_All_Outer_pid(2);
	}
	else if(choose == 3)//位置串环环清除
	{
		pid_reinit(&left_front_inner_pid);
		pid_reinit(&right_front_inner_pid);
		pid_reinit(&left_rear_inner_pid);
		pid_reinit(&right_rear_inner_pid);
		clear_All_Outer_pid(0);
	}
}






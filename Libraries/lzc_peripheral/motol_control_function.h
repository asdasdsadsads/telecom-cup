#ifndef _MOTOL_CONTROL_FUNCTION_h
#define _MOTOL_CONTROL_FUNCTION_h

#include "headfile.h"

extern int angle_speed;   //角度内环速度
extern int L_S_Spd;				//调整内环速度
extern int L_X_Spd;
extern int R_S_Spd;
extern int R_X_Spd;
extern int L_S_forward_Spd;		//直走速度
extern int L_X_forward_Spd;
extern int R_S_forward_Spd;
extern int R_X_forward_Spd;
extern int forward_Distance;
extern float angle_err;

void Outring_PID_init();
int Motolxianfu(int num,int max);
int lower_limit(int raw_data,int lower_data);
bool Within_angle(float within_angle);
float closeToImg_turn(Target_points *points);
void Position_position_handle();
void Angle_position_handle();
void Adj_position_handle();
void Forward_position_handle();
void clear_All_Outer_pid(uint8 choose);
void PID_FORWORD_INNER_TEXT();

#endif
#include "lzc_EKF.h"

/**
  * @brief    卡尔曼滤波器初始化
  *
  * @param    param    ： 卡尔曼参数结构体
  * @param    dt       ： 两次卡尔曼之间的时间间隔  单位S
  * @param    Q        ： 卡尔曼参数   
  * @param    R        ： 卡尔曼参数 
  *
  * @return   
  *
  * @note     其实是模型误差与测量误差的大小，是模型预测值与测量值的加权。
  *           举例而言，R固定，Q越大，代表越信任侧量值，Q无穷代表只用测量值；
  *           反之，Q越小代表越信任模型预测值，Q为零则是只用模型预测。
  *           状态延时高，说明收敛速度慢。
  *           估计参数Q越大，收敛的越快。
  *           测量误差R越小，收敛的越快。
  *           调整这两个参数即可，从状态更新上说，测量误差越小，估计参数误差越大，说明我们越相信测量值，
  *           自然收敛的快。缺点就是会让系统变化过快，如果测量值更加不准，则精度会下降，系统不够稳定。
  *
  * @example  
  *
  * @date     2019/6/19 星期三
  */
void KalmanFilter_Init(ekf_param_t *param, float dt, float Q, float R)
{
    param->angle = 0;
    param->dt = dt;
    param->PP[0][0] = 1;
    param->PP[0][1] = 0;
    param->PP[1][0] = 0;
    param->PP[1][1] = 1;
    param->Q_angle = Q;
    param->Q_bias = 0;
    param->Q_gyro = 0.0001;
    param->R_angle = R;
}

/**
  * @brief    卡尔曼滤波
  *
  * @param    param    ： 卡尔曼参数结构体
  * @param    acc_angle： 加速度计 计算出的角度
  * @param    gyro：   ： 角速度
  *
  * @return   
  *
  * @note     仅供参考
  *
  * @example  
  *
  * @date     2019/6/19 星期三
  */
float KalmanFilter_apply(ekf_param_t *param, float acc_angle, float gyro)
{
    /* 陀螺仪的预测值 */
    float gyro_angle = param->angle + (gyro - param->Q_bias) * param->dt;

    /* 获取先验估计误差协方差的微分 */
    float Pdot[4];
    Pdot[0] = param->Q_angle - param->PP[0][1] - param->PP[1][0];     
		Pdot[1] = -param->PP[1][1];
		Pdot[2] = -param->PP[1][1];
		Pdot[3] = param->Q_gyro;
    
    /* 对先验估计误差协方差的微分进行积分 获得误差协方差 */
    param->PP[0][0] += Pdot[0] * param->dt;   
    param->PP[0][1] += Pdot[1] * param->dt;   
    param->PP[1][0] += Pdot[2] * param->dt;
    param->PP[1][1] += Pdot[3] * param->dt;
    
    /* 计算卡尔曼增益 */
    float E = param->R_angle + param->PP[0][0];
    
    float k_0 = param->PP[0][0] / E;
    float k_1 = param->PP[1][0] / E;
    
    
    /* 最优估计 */
    float angle_err = acc_angle - gyro_angle;
 
    param->angle = gyro_angle + k_0 * angle_err;
    param->Q_bias = param->Q_bias + k_1 * angle_err;
    
    /* 更新后验估计误差协方差 */
    float temp1 = param->PP[0][0];
    float temp2 = param->PP[1][0];

    param->PP[0][0] -= k_0 * temp1;
    param->PP[0][1] -= k_0 * temp2;
    param->PP[1][0] -= k_1 * temp1;
    param->PP[1][1] -= k_1 * temp2;
        
    return param->angle;
}


/* 定义一个卡尔曼滤波器 */
ekf_t ekf = 
{
    KalmanFilter_Init,
    KalmanFilter_apply,
};



//单值卡尔曼滤波


//2. 以高度为例 定义卡尔曼结构体并初始化参数
KFP KFP_height={0.02,0,0,0,0.001,0.543};

/**
 *卡尔曼滤波器
 *@param KFP *kfp 卡尔曼结构体参数
 *   float input 需要滤波的参数的测量值（即传感器的采集值）
 *@return 滤波后的参数（最优值）
 */
 float kalmanFilter(KFP *kfp,float input)
 {
     //预测协方差方程：k时刻系统估算协方差 = k-1时刻的系统协方差 + 过程噪声协方差
     kfp->Now_P = kfp->LastP + kfp->Q;
     //卡尔曼增益方程：卡尔曼增益 = k时刻系统估算协方差 / （k时刻系统估算协方差 + 观测噪声协方差）
     kfp->Kg = kfp->Now_P / (kfp->Now_P + kfp->R);
     //更新最优值方程：k时刻状态变量的最优值 = 状态变量的预测值 + 卡尔曼增益 * （测量值 - 状态变量的预测值）
     kfp->out = kfp->out + kfp->Kg * (input -kfp->out);//因为这一次的预测值就是上一次的输出值
     //更新协方差方程: 本次的系统协方差付给 kfp->LastP 威下一次运算准备。
     kfp->LastP = (1-kfp->Kg) * kfp->Now_P;
     return kfp->out;
 }






#ifndef _LZC_STRUCT_h
#define _LZC_STRUCT_h

#include "common.h"
#include "define.h"


typedef struct
{
	float x;
	float y;
	float z;
}vector3f_t;

typedef struct
{
	int16_t x;
	int16_t y;
	int16_t z;
}vector3i_t;

typedef struct
{
	char x;
	unsigned char z;
}vector2_apriltag;

typedef struct points
{
	int x_point;
	int y_point;
	float distance;
	bool status;//判断该目标点任务是否已经完成
	uint8_t direction;//目标方向，上下左右分别为0，1，2，3
	float angle;
}Target_points;

float invSqrt(float x);
float constrain_float(float amt, float low, float high);
int constrain_int(int amt, int low, int high) ;
int16_t constrain_int16(int16_t amt, int16_t low, int16_t high);
int32_t constrain_int32(int32_t amt, int32_t low, int32_t high);
float fast_atan(float v);
float fast_atan2(float y, float x);
uint32_t sqrt_32_plus(int32_t a,int32_t b);
int distance_calculate(int a1, int a2, int b1, int b2);
int min2(int x, int y);
int distance_calculate_square(int a1, int a2, int b1, int b2);


#endif
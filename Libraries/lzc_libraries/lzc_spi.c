

//-------------------------------------------------------------------------------------------------------------------
//from machine import SPI,Pin
//spi = SPI(30)
//spi.init(1000000,0,0,8,SPI.MSB)
//cs = Pin(("B3",3))
//cs.init(Pin.OUT_PP,Pin.PULL_NONE)
//def write(data):
//    cs.value(0)
//    spi.write(data)
//    cs.value(1)

//def read():
//    cs.value(0)
//    buf = spi.read(1)
//    cs.value(1)
//    return buf
//    
//def readwrite(writebuf,num):
//    cs.value(0)
//    readbuf = bytearray(num)
//    spi.write_readinto(writebuf,readbuf)
//    cs.value(1)
//    return readbuf
//
//
//  #用法：print(readwrite(bytearray([0x11,0x22,0x33]),3))
//  #注意，接收字节必须和发送字节的个数全部一致，单片机与openart的对应字节数必须一致
//-------------------------------------------------------------------------------------------------------------------
//以上复制到openart代码里面就能用  设置波特率1M，最低不要低于10k，不要高于10M，测试一次发送接收5个字节没有问题,在openart接收端会有少部分数据出错（软件纠正就行）


//以下函数是单片机作为从机与openart进行通信的，作为主机情况下请使用逐飞库
#include "lzc_spi.h"

//-------------------------------------------------------------------------------------------------------------------
//  @brief      从机条件下 SPI接收函数（此函数可以用任意IO口模拟SPI时序）
//  @param      
//  @param      CS          选择SPI片选引脚(对应openart的CS)
//  @return    void	
//  @since      v1.0
//  Sample usage:      初始化后就可以直接调用下面的接受发送函数
//-------------------------------------------------------------------------------------------------------------------
void spi1_slave_init(PIN_enum CS_PIN,PIN_enum MOSI_PIN,PIN_enum MISO_PIN,PIN_enum SCK_PIN)
{
	  gpio_init(MOSI_PIN,GPI,0,GPIO_PIN_CONFIG);//MOSI
    gpio_init(SCK_PIN,GPI,0,GPIO_PIN_CONFIG);//SCK
		gpio_init(MISO_PIN,GPO,0,GPIO_PIN_CONFIG);//MISO
		gpio_interrupt_init(CS_PIN,FALLING,GPIO_INT_CONFIG);//CS
}

//-------------------------------------------------------------------------------------------------------------------
//  @brief      从机条件下 SPI接收函数（此函数可以用任意IO口模拟SPI时序）
//  @param      
//  @param      CS          选择SPI片选引脚(对应openart的CS)
//  @param      SCK,MOSI,MISO的定义在lzc_spi.h中
//  @param      readbuf        选择读取字节的数组
//  @param      num        选择读取字节的个数
//  @return    unsigned char			
//  @since      v2.0
//  Sample usage:      spislaveRead（B3,readbuf[2].2）//此函数一次接收2个字节，需要配合选定CS为下降沿中断引脚，对应的openart的write（bytearray[0x11,0x22]）
//-------------------------------------------------------------------------------------------------------------------


void spislaveRead(PIN_enum CS,PIN_enum MOSI,PIN_enum MISO,PIN_enum SCK,int num,unsigned char buf[])
{
		unsigned char i,rrbyte=0,rflag=0;
		int count=0;
		for(int j = 0;j<num;j++)
		{
			if(!(gpio_get(CS)))
			{
				for(i = 0;i<8;i++)
				{
					 rflag=1;
					while(!fast_gpio_get(SCK)&&count<100000)//防止卡在while中
					{
						count++;
					}
					count=0;
					rrbyte = rrbyte<<1;
						if(fast_gpio_get(MOSI))
							rrbyte|=1;			
					while(fast_gpio_get(SCK))//防止卡在while中
						{
						}
				}
				buf[j]=rrbyte;
				rrbyte = 0 ;
			}
		}
		while(!(gpio_get(CS)))//防止卡在while中
		{
		};
}


//-------------------------------------------------------------------------------------------------------------------
//  @brief      从机条件下 SPI接收函数（此函数可以用任意IO口模拟SPI时序）
//  @param      
//  @param      CS          选择SPI片选引脚(对应openart的CS)
//  @param      SCK,MOSI,MISO的定义在lzc_spi.h中
//  @param      byte        选择写入单字节的数据
//  @return     void			
//  @since      v2.0
//  Sample usage:      spislavWrite（D12,D13,D14,D15，0x11）//引脚可以任选，此函数需要配合选定CS为下降沿中断引脚，对应的openart的read
//-------------------------------------------------------------------------------------------------------------------


void spislaveWrite(PIN_enum CS,PIN_enum MOSI,PIN_enum MISO,PIN_enum SCK,unsigned char byte)
{
	 unsigned char i;
	int count=0;
	if(!(gpio_get(CS)))
	{
		for (i=0;i<8;i++)
		{
			while(fast_gpio_get(SCK)&&count<10000)
			{
				count++;
			};
			count=0;
			if(byte&0x80)  fast_gpio_set(MISO,1);
			else           fast_gpio_set(MISO,0);
			while(!(fast_gpio_get(SCK))&&count<10000)
			{
				count++;
			}
			count=0;
			byte<<=1;
		}
	}
		while(!(gpio_get(CS))&&count<50000)
	{
		count++;
	}
}



//-------------------------------------------------------------------------------------------------------------------
//  @brief      从机条件下 SPI接收发送函数（此函数可以用任意IO口模拟SPI时序）
//  @param      SCK,MOSI,MISO的定义在lzc_spi.h中
//  @param      CS          选择SPI片选引脚(对应openart的CS)
//  @param      readbuf        选择读取字节的数组
//  @param      writebuf        选择写入字节的数组
//  @param      num        选择读取/写入字节的个数
//  @return    	unsigned char			
//  @since      v1.0
//  Sample usage:     spislaveRead（B3,readbuf[2],writebuf[2],2）//此函数一次接收发送2个字节，需要配合选定CS为下降沿中断引脚，对应的openart的readwrite（bytearray[0x11,0x22],2）
//-------------------------------------------------------------------------------------------------------------------

void spislaveReadWrite(PIN_enum CS_PIN,PIN_enum MOSI_PIN,PIN_enum MISO_PIN,PIN_enum SCK_PIN,unsigned char readbuf[],unsigned char writebuf[],int num)
{
	unsigned char i,rrbyte=0,rflag=0;
	int count=0;
	for(int j=0;j<num;j++)
	{
		if(!(gpio_get(CS_PIN)))
		{
			for(i = 0;i<8;i++)
			{
				 rflag=1;
				while(!gpio_get(SCK_PIN)&&count<100000)//防止卡在while中
				{
					count++;
				};
				count=0;
				rrbyte = rrbyte<<1;
					if(gpio_get(MOSI_PIN))
						rrbyte|=1;			
				while(gpio_get(SCK_PIN)&&count<10000)//防止卡在while中
				{
					count++;
				}
				count=0;
			}
			readbuf[j] = rrbyte;
			rrbyte = 0;
		}
	}	
	for(int j=0;j<num;j++)
	{
		if(!(gpio_get(CS_PIN)))
		{
			for(i=0;i<8;i++)
			{
				while(gpio_get(SCK_PIN)&&count<10000)//防止卡在while中
				{
					count++;
				};
				count=0;
				if(writebuf[j]&0x80)  gpio_set(MISO_PIN,1);
				else           				gpio_set(MISO_PIN,0);
				while(!(gpio_get(SCK_PIN))&&count<10000)//防止卡在while中
				{
					count++;
				};
				count=0;
				writebuf[j]<<=1;		
			}
		}
	}
	//PRINTF("%d  %d \r\n",count1,count2);
	while(!(gpio_get(CS_PIN))&&count<50000)//防止卡在while中
	{
		count++;
	};
}


#ifndef _LZC_SPI_h
#define _LZC_SPI_h

#include "headfile.h"

#define SCK11   C22
#define MOSI11  C20
#define MISO11  C21

void spi1_slave_init(PIN_enum CS_PIN,PIN_enum MOSI_PIN,PIN_enum MISO_PIN,PIN_enum SCK_PIN);
void spislaveRead(PIN_enum CS,PIN_enum MOSI,PIN_enum MISO,PIN_enum SCK,int num,unsigned char buf[]);
void spislaveWrite(PIN_enum CS,PIN_enum MOSI,PIN_enum MISO,PIN_enum SCK,unsigned char byte);
void spislaveReadWrite(PIN_enum CS_PIN,PIN_enum MOSI_PIN,PIN_enum MISO_PIN,PIN_enum SCK_PIN,unsigned char readbuf[],unsigned char writebuf[],int num);
void spislaveReadBytes(PIN_enum CS,unsigned char readbuf[],int num);
void spislaveWriteBytes(PIN_enum CS,unsigned char writebuf[],int num);
#endif


#include "lzc_pid.h"


/**************************************************************************
函数功能：初始化pid句柄
入口参数：句柄kp，ki，kd
返回  值：无
**************************************************************************/
void PID_init(my_pid_handle *handle,float kp,float ki,float kd)
{
	handle->kp = kp;
	handle->ki = ki;
	handle->kd = kd;
	handle->pwm = 0;
	handle->Bias = 0;
	handle->Integral_bias = 0;
	handle->Last_bias = 0;
	handle->encoder = 0;
}

/**************************************************************************
函数功能：清除该句柄下的pid记录值(除了编码值不清除)
入口参数：pid句柄
返回  值：无
**************************************************************************/
void pid_reinit(my_pid_handle *handle)
{
	handle->pwm = 0;
	handle->Bias = 0;
	handle->Integral_bias = 0;
	handle->Last_bias = 0;
	handle->encoder = 0;
}

/**************************************************************************
函数功能：清除车的储存编码值
入口参数：pid句柄
返回  值：无
**************************************************************************/
void Encoder_reinit()
{
		qtimer_quad_clear(QTIMER_1,QTIMER1_TIMER0_C0);
		qtimer_quad_clear(QTIMER_1,QTIMER1_TIMER2_C2);
		qtimer_quad_clear(QTIMER_2,QTIMER2_TIMER0_C3);
		qtimer_quad_clear(QTIMER_3,QTIMER3_TIMER2_B18);
}

/**************************************************************************
函数功能：获取编码器的值
//  @param      handle			   选择哪个句柄	（枚举变量）
//  @param      whichencoder   选择哪个编码器（枚举变量）
//  @param      choose         选择读到编码器后是否清零，1代表清零，0代表不清零
返回  值：无
使用说明：无
**************************************************************************/
int16 getEncoder(my_pid_handle *handle,Encoder whichencoder,uint8 choose)
{
	int16 getdata = 0;
	switch(whichencoder){
		case left_front_encoder:
		{
			getdata = qtimer_quad_get(QTIMER_1,QTIMER1_TIMER0_C0);
			if(!choose)
				handle->encoder += getdata;
			qtimer_quad_clear(QTIMER_1,QTIMER1_TIMER0_C0);
			return getdata;
		}break;
		case left_rear_encoder:
		{
			getdata = qtimer_quad_get(QTIMER_1,QTIMER1_TIMER2_C2);
			if(!choose)
				handle->encoder += getdata;
			qtimer_quad_clear(QTIMER_1,QTIMER1_TIMER2_C2 );
			return getdata;
		}break;	
		case right_front_encoder:
		{
			getdata = -qtimer_quad_get(QTIMER_2,QTIMER2_TIMER0_C3);
			if(!choose)
				handle->encoder += getdata ;
			qtimer_quad_clear(QTIMER_2,QTIMER2_TIMER0_C3);
			return getdata;
		}break;
		case right_rear_encoder:
		{	
			getdata = -qtimer_quad_get(QTIMER_3,QTIMER3_TIMER2_B18);
			if(!choose)
				handle->encoder += getdata;
			qtimer_quad_clear(QTIMER_3,QTIMER3_TIMER2_B18 );
			return getdata;
		}break;
		 default:assert(0);break;//参数错误，进入断言
		}			
 return 0;
}


/**************************************************************************
函数功能：增量PI控制器
入口参数：编码器测量值，目标速度
返回  值：电机PWM
根据增量式离散PID公式 
pwm+=Kp[e（k）-e(k-1)]+Ki*e(k)+Kd[e(k)-2e(k-1)+e(k-2)]
e(k)代表本次偏差 
e(k-1)代表上一次的偏差  以此类推 
pwm代表增量输出
在我们的速度控制闭环系统里面，只使用PI控制
pwm+=Kp[e（k）-e(k-1)]+Ki*e(k)
**************************************************************************/
int Incremental_PI(my_pid_handle *handle,int Encoder,int Target)
{ 	
	 handle->Bias = Target-Encoder;
	 handle->pwm+=handle->kp*(handle->Bias-handle->Last_bias)+handle->ki*handle->Bias;   //增量式PI控制器
	 handle->Last_bias=handle->Bias;	
	 return handle->pwm;                                           //增量输出
}

//浮点增量式PID
int f_Incremental_PID (my_pid_handle *handle,float Encoder,float Target)
{ 	
	 handle->Bias = Target-Encoder;
	 handle->pwm+=handle->kp*(handle->Bias-handle->Last_bias)+handle->ki*handle->Bias;   //增量式PI控制器
	 handle->Last_bias=handle->Bias;	    
	 return handle->pwm;                                           //增量输出
}

//参数为误差值的增量式PID
int Incremental_PI_bias(my_pid_handle *handle,int bias)
{ 	
	 handle->Bias = bias;
	 handle->pwm+=handle->kp*(handle->Bias-handle->Last_bias)+handle->ki*handle->Bias;   //增量式PI控制器
	 handle->Last_bias=handle->Bias;	
	 return handle->pwm;                                           //增量输出
}
/**************************************************************************
函数功能：位置式PID控制器
入口参数：编码器测量位置信息，目标位置
返回  值：电机PWM
根据位置式离散PID公式 
pwm=Kp*e(k)+Ki*∑e(k)+Kd[e（k）-e(k-1)]
e(k)代表本次偏差 
e(k-1)代表上一次的偏差  
∑e(k)代表e(k)以及之前的偏差的累积和;其中k为1,2,,k;
pwm代表输出
**************************************************************************/
int Position_PID(my_pid_handle *handle,int Encoder,int Target)
{ 	
	 static int num = 0;
	 if(num>4)  num = 0;
   handle->Bias =Target - Encoder;
	 handle->Integral_bias+=handle->Bias;
	 handle->pwm=handle->kp*handle->Bias+handle->ki*handle->Integral_bias+handle->kd*(handle->Bias-handle->Last_bias);       //位置式PID控制器
	 handle->Last_bias=handle->Bias;                                       //保存上一次偏差 
	 num++;
	 return handle->pwm;                                           //增量输出	
}

//参数为误差值的整型位置式pid
int Position_PID_bias(my_pid_handle *handle,int bias)
{ 	
   handle->Bias =bias;
	 handle->Integral_bias+=handle->Bias;
	 handle->pwm=handle->kp*handle->Bias+handle->ki*handle->Integral_bias+handle->kd*(handle->Bias-handle->Last_bias);       //位置式PID控制器
	 handle->Last_bias=handle->Bias;                                       //保存上一次偏差 
	 return handle->pwm;                                           //增量输出	
}

//浮点位置式PID
int f_Position_PID (my_pid_handle *handle,float Encoder,float Target)
{ 	
   handle->Bias =Target - Encoder;
	 handle->Integral_bias+=handle->Bias;
	 handle->pwm=handle->kp*handle->Bias+handle->ki*handle->Integral_bias+handle->kd*(handle->Bias-handle->Last_bias);       //位置式PID控制器
	 handle->Last_bias=handle->Bias;         
	 return handle->pwm;                                           //增量输出
}

//参数为误差值的浮点位置式PID
int f_Position_PID_bias(my_pid_handle *handle,float bias)
{ 	
   handle->Bias =bias;
	 handle->Integral_bias+=handle->Bias;
	 handle->pwm=handle->kp*handle->Bias+handle->ki*handle->Integral_bias+handle->kd*(handle->Bias-handle->Last_bias);       //位置式PID控制器
	 handle->Last_bias=handle->Bias;         
	 return handle->pwm;                                           //增量输出
}


#ifndef __DEFINE_H
#define __DEFINE_H

#define G		    		9.80665f		       		 // m/s^2	
#define RadtoDeg    57.295779513f				//弧度到角度 (弧度 * 180/3.1415)
#define DegtoRad    0.0174533f				//角度到弧度 (角度 * 3.1415/180)
#define Acc_Gain  	0.0002440f				//加速度变成G (初始化加速度满量程-+8g LSBa = 2*8/65535.0)
#define Gyro_Gain 	0.0609756f				//角速度变成度 (初始化陀螺仪满量程+-2000 LSBg = 2*2000/65535.0)
#define Gyro_Gr	    0.0010641f			  //角速度变成弧度(3.1415/180 * LSBg)
#define PI 					3.1415926f		

#define slope  			0.00212738439306359f			//斜率
#define intercept   0.944364161849705f			//截距

#define X_max_encoder (700-intercept)slope  //场地最大编码值
#define Y_max_encoder (500-intercept)slope  //场地最大编码值

#define Two_angle_pid

#define CameraMiddle_to_axis 18 

#define DIR_MOTOL

#define NewMainBoard

#define Offset 1
#define Exclusion_algorithm        //加上算法优化

#define Complementary //使用互补滤波
#define ICM20602	//使用icm20602

//定义oled显示哪个
//#define OLED_ENCODER

//#define OLED_ANGLE
//#define OLED_POSITION	
//#define OLED_APRILTAG


//定义是否需要pithc，roll角
//#define pitch_roll

#endif
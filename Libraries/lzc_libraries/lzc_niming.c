#include "lzc_niming.h"

/**************************************************************************
函数功能：匿名上位机通信协议函数，用于显示波形并且调参，显示速度编码值的波形
入口参数：上位机所需显示的曲线数据(四个轮编码值)，该函数在motol_start()中使用
返回  值：无
使用说明：该函数在motol_start中调用，并且在匿名上位机中打开查看波形，详细请看使用说明
**************************************************************************/
void NimingOutput_Speed(int32 L_SE,int32 L_XE,int32 R_SE,int32 R_XE)
{
	uint8 data_buf[35];
	uint8 CNT=0;
	uint8 sumcheck = 0;
	uint8 addcheck = 0;
	int16 L_S_Tar =  L_S_forward_Spd;
	int16 L_X_Tar =  L_X_forward_Spd;
	int16 R_S_Tar =  R_S_forward_Spd;
	int16 R_X_Tar =  R_X_forward_Spd;
	data_buf[CNT++] = 0xaa;
	data_buf[CNT++] = 0xff;
	data_buf[CNT++] = 0xf1;
	data_buf[CNT++] = 24;
	data_buf[CNT++] = BYTE0(L_SE);
	data_buf[CNT++] = BYTE1(L_SE);
	data_buf[CNT++] = BYTE2(L_SE);
	data_buf[CNT++] = BYTE3(L_SE);
	data_buf[CNT++] = BYTE0(L_S_Tar);
	data_buf[CNT++] = BYTE1(L_S_Tar);
	data_buf[CNT++] = BYTE0(L_XE);
	data_buf[CNT++] = BYTE1(L_XE);
	data_buf[CNT++] = BYTE2(L_XE);
	data_buf[CNT++] = BYTE3(L_XE);
	data_buf[CNT++] = BYTE0(L_X_Tar);
	data_buf[CNT++] = BYTE1(L_X_Tar);
	data_buf[CNT++] = BYTE0(R_SE);
	data_buf[CNT++] = BYTE1(R_SE);
	data_buf[CNT++] = BYTE2(R_SE);
	data_buf[CNT++] = BYTE3(R_SE);
	data_buf[CNT++] = BYTE0(R_S_Tar);
	data_buf[CNT++] = BYTE1(R_S_Tar);
	data_buf[CNT++] = BYTE0(R_XE);
	data_buf[CNT++] = BYTE1(R_XE);
	data_buf[CNT++] = BYTE2(R_XE);
	data_buf[CNT++] = BYTE3(R_XE);
	data_buf[CNT++] = BYTE0(R_X_Tar); 
	data_buf[CNT++] = BYTE1(R_X_Tar); 
	for(uint8 i=0; i < (data_buf[3]+4); i++)
	{
		sumcheck += data_buf[i]; //从帧头开始，对每一字节进行求和，直到DATA区结束
		addcheck += sumcheck; 		//每一字节的求和操作，进行一次sumcheck的累加
	}
	data_buf[CNT++] = sumcheck;
	data_buf[CNT++] = addcheck;
	uart_putbuff(USART_1,data_buf,CNT);
}


/**************************************************************************
函数功能：匿名上位机通信协议函数，用于显示波形并且调参，显示位置编码值的波形
入口参数：上位机所需显示的曲线数据(四个轮编码值)，该函数在motol_start()中使用
返回  值：无
使用说明：该函数在motol_start中调用，并且在匿名上位机中打开查看波形，详细请看使用说明
**************************************************************************/
void NimingOutput_Position(int32 L_SE,int32 L_XE,int32 R_SE,int32 R_XE)
{
	uint8 data_buf[40];
	uint8 CNT=0;
	uint8 sumcheck = 0;
	uint8 addcheck = 0;
	int32 L_S_Tar =  target_left_front_position;
	int32 L_X_Tar =  target_left_rear_position;
	int32 R_S_Tar =  target_right_front_position;
	int32 R_X_Tar =  target_right_rear_position;
	data_buf[CNT++] = 0xaa;
	data_buf[CNT++] = 0xff;
	data_buf[CNT++] = 0xf1;
	data_buf[CNT++] = 32;
	data_buf[CNT++] = BYTE0(L_SE);
	data_buf[CNT++] = BYTE1(L_SE);
	data_buf[CNT++] = BYTE2(L_SE);
	data_buf[CNT++] = BYTE3(L_SE);
	data_buf[CNT++] = BYTE0(L_S_Tar);
	data_buf[CNT++] = BYTE1(L_S_Tar);
	data_buf[CNT++] = BYTE2(L_S_Tar);
	data_buf[CNT++] = BYTE3(L_S_Tar);
	data_buf[CNT++] = BYTE0(L_XE);
	data_buf[CNT++] = BYTE1(L_XE);
	data_buf[CNT++] = BYTE2(L_XE);
	data_buf[CNT++] = BYTE3(L_XE);
	data_buf[CNT++] = BYTE0(L_X_Tar);
	data_buf[CNT++] = BYTE1(L_X_Tar);
	data_buf[CNT++] = BYTE2(L_X_Tar);
	data_buf[CNT++] = BYTE3(L_X_Tar);
	data_buf[CNT++] = BYTE0(R_SE);
	data_buf[CNT++] = BYTE1(R_SE);
	data_buf[CNT++] = BYTE2(R_SE);
	data_buf[CNT++] = BYTE3(R_SE);
	data_buf[CNT++] = BYTE0(R_S_Tar);
	data_buf[CNT++] = BYTE1(R_S_Tar);
	data_buf[CNT++] = BYTE2(R_S_Tar);
	data_buf[CNT++] = BYTE3(R_S_Tar);
	data_buf[CNT++] = BYTE0(R_XE);
	data_buf[CNT++] = BYTE1(R_XE);
	data_buf[CNT++] = BYTE2(R_XE);
	data_buf[CNT++] = BYTE3(R_XE);
	data_buf[CNT++] = BYTE0(R_X_Tar); 
	data_buf[CNT++] = BYTE1(R_X_Tar); 
	data_buf[CNT++] = BYTE2(R_X_Tar);
	data_buf[CNT++] = BYTE3(R_X_Tar);
	for(uint8 i=0; i < (data_buf[3]+4); i++)
	{
		sumcheck += data_buf[i]; //从帧头开始，对每一字节进行求和，直到DATA区结束
		addcheck += sumcheck; 		//每一字节的求和操作，进行一次sumcheck的累加
	}
	data_buf[CNT++] = sumcheck;
	data_buf[CNT++] = addcheck;
	uart_putbuff(USART_1,data_buf,CNT);
}


/**************************************************************************
函数功能：匿名上位机的波形输出欧拉角
入口参数：无
返回  值：无
使用说明：使用匿名上位机v7，在主函数中调用此函数发送即可，详细请看使用说明
**************************************************************************/
void NimingOutput_Angle()
{
	uint8 data_buf[13];
	uint8 sumcheck = 0;
	uint8 addcheck = 0;
	int16 rol = angle[0]*100;
//	int16 pit = angle[1]*100;
	int16 pit = Target_angle*100;//这里替换成了目标yaw，调参用
	int16 yaw = angle[2]*100;

	data_buf[0] = 0xaa;
	data_buf[1] = 0xff;
	data_buf[2] = 0x03;
	data_buf[3] = 7;
	data_buf[4] = BYTE0(rol);
	data_buf[5] = BYTE1(rol);
	data_buf[6] = BYTE0(pit);
	data_buf[7] = BYTE1(pit);
	data_buf[8] = BYTE0(yaw);
	data_buf[9] = BYTE1(yaw);
	data_buf[10] = 1;//状态位，随便给
	for(uint8 i=0; i < (data_buf[3]+4); i++)
	{
		sumcheck += data_buf[i]; //从帧头开始，对每一字节进行求和，直到DATA区结束
		addcheck += sumcheck; //每一字节的求和操作，进行一次sumcheck的累加
	}
	data_buf[11] = sumcheck;
	data_buf[12] = addcheck;
	uart_putbuff(USART_1,data_buf,13);
}


/**************************************************************************
函数功能：匿名上位机的波形输出角加速度
入口参数：无
返回  值：无
使用说明：使用匿名上位机v7，在主函数中调用此函数发送即可，详细请看使用说明
**************************************************************************/
void NimingOutput_Acc()
{
	uint8 data_buf[13];
	uint8 sumcheck = 0;
	uint8 addcheck = 0;
	uint8 CNT = 0;
	int16 acc_x = icm_acc_x;
	int16 acc_y = icm_acc_y;
	int16 acc_z = icm_acc_z;
	
	data_buf[CNT++] = 0xAA;
	data_buf[CNT++] = 0xFF;
	data_buf[CNT++] = 0xF2;
	data_buf[CNT++] = 6;
	data_buf[CNT++] = BYTE0(acc_x);
	data_buf[CNT++] = BYTE1(acc_x);
	data_buf[CNT++] = BYTE0(acc_y);
	data_buf[CNT++] = BYTE1(acc_y);
	data_buf[CNT++] = BYTE0(acc_z);
	data_buf[CNT++] = BYTE1(acc_z);
	for(uint8 i=0; i < (data_buf[3]+4); i++)
	{
		sumcheck += data_buf[i]; //从帧头开始，对每一字节进行求和，直到DATA区结束
		addcheck += sumcheck; //每一字节的求和操作，进行一次sumcheck的累加
	}
	data_buf[CNT++] = sumcheck;
	data_buf[CNT++] = addcheck;
	uart_putbuff(USART_1,data_buf,CNT);
}


/**************************************************************************
函数功能：匿名上位机的波形输出角加速度(AHRS算法)
入口参数：无
返回  值：无
使用说明：使用匿名上位机v7，在主函数中调用此函数发送即可，详细请看使用说明
**************************************************************************/
void NimingOutput_AHRS()
{
	uint8 data_buf[13];
	uint8 sumcheck = 0;
	uint8 addcheck = 0;
	uint8 CNT = 0;
	int16 x = ahrs_angle.x*100;
	int16 y = ahrs_angle.y*100;
	int16 z = ahrs_angle.z*100;
	
	data_buf[CNT++] = 0xAA;
	data_buf[CNT++] = 0xFF;
	data_buf[CNT++] = 0x03;
	data_buf[CNT++] = 7;
	data_buf[CNT++] = BYTE0(x);
	data_buf[CNT++] = BYTE1(x);
	data_buf[CNT++] = BYTE0(y);
	data_buf[CNT++] = BYTE1(y);
	data_buf[CNT++] = BYTE0(z);
	data_buf[CNT++] = BYTE1(z);
	data_buf[CNT++] = 1;
	for(uint8 i=0; i < (data_buf[3]+4); i++)
	{
		sumcheck += data_buf[i]; //从帧头开始，对每一字节进行求和，直到DATA区结束
		addcheck += sumcheck; //每一字节的求和操作，进行一次sumcheck的累加
	}
	data_buf[CNT++] = sumcheck;
	data_buf[CNT++] = addcheck;
	uart_putbuff(USART_1,data_buf,CNT);
}


/**************************************************************************
函数功能：匿名上位机通信协议函数，用于显示波形并且调参，显示速度编码值的波形
入口参数：上位机所需显示的曲线数据(四个轮编码值)，该函数在motol_start()中使用
返回  值：无
使用说明：该函数在motol_start中调用，并且在匿名上位机中打开查看波形，详细请看使用说明
**************************************************************************/
void NimingOutput_AngleSped_pid(int32 L_SE,int32 L_XE,int32 R_SE,int32 R_XE,int angle_Speed)
{
	uint8 data_buf[35];
	uint8 CNT=0;
	uint8 sumcheck = 0;
	uint8 addcheck = 0;
	int16 L_S_Tar =  angle_Speed;
	int16 L_X_Tar =  angle_Speed;
	int16 R_S_Tar =  -angle_Speed;
	int16 R_X_Tar =  -angle_Speed;
	data_buf[CNT++] = 0xaa;
	data_buf[CNT++] = 0xff;
	data_buf[CNT++] = 0xf1;
	data_buf[CNT++] = 24;
	data_buf[CNT++] = BYTE0(L_SE);
	data_buf[CNT++] = BYTE1(L_SE);
	data_buf[CNT++] = BYTE2(L_SE);
	data_buf[CNT++] = BYTE3(L_SE);
	data_buf[CNT++] = BYTE0(L_S_Tar);
	data_buf[CNT++] = BYTE1(L_S_Tar);
	data_buf[CNT++] = BYTE0(L_XE);
	data_buf[CNT++] = BYTE1(L_XE);
	data_buf[CNT++] = BYTE2(L_XE);
	data_buf[CNT++] = BYTE3(L_XE);
	data_buf[CNT++] = BYTE0(L_X_Tar);
	data_buf[CNT++] = BYTE1(L_X_Tar);
	data_buf[CNT++] = BYTE0(R_SE);
	data_buf[CNT++] = BYTE1(R_SE);
	data_buf[CNT++] = BYTE2(R_SE);
	data_buf[CNT++] = BYTE3(R_SE);
	data_buf[CNT++] = BYTE0(R_S_Tar);
	data_buf[CNT++] = BYTE1(R_S_Tar);
	data_buf[CNT++] = BYTE0(R_XE);
	data_buf[CNT++] = BYTE1(R_XE);
	data_buf[CNT++] = BYTE2(R_XE);
	data_buf[CNT++] = BYTE3(R_XE);
	data_buf[CNT++] = BYTE0(R_X_Tar); 
	data_buf[CNT++] = BYTE1(R_X_Tar); 
	for(uint8 i=0; i < (data_buf[3]+4); i++)
	{
		sumcheck += data_buf[i]; //从帧头开始，对每一字节进行求和，直到DATA区结束
		addcheck += sumcheck; 		//每一字节的求和操作，进行一次sumcheck的累加
	}
	data_buf[CNT++] = sumcheck;
	data_buf[CNT++] = addcheck;
	uart_putbuff(USART_1,data_buf,CNT);
}



/**************************************************************************
函数功能：匿名上位机通信协议函数，用于显示波形并且调参，显示速度编码值的波形
入口参数：上位机所需显示的曲线数据(四个轮编码值)，该函数在motol_start()中使用
返回  值：无
使用说明：该函数在motol_start中调用，并且在匿名上位机中打开查看波形，详细请看使用说明
**************************************************************************/
void NimingOutput_AdjSped_pid(int32 L_SE,int32 L_XE,int32 R_SE,int32 R_XE,int tarls,int tarlx,int tarrs,int tarrx)
{
	uint8 data_buf[35];
	uint8 CNT=0;
	uint8 sumcheck = 0;
	uint8 addcheck = 0;
	int16 L_S_Tar =  tarls;
	int16 L_X_Tar =  tarlx;
	int16 R_S_Tar =  tarrs;
	int16 R_X_Tar =  tarrx;
	data_buf[CNT++] = 0xaa;
	data_buf[CNT++] = 0xff;
	data_buf[CNT++] = 0xf1;
	data_buf[CNT++] = 24;
	data_buf[CNT++] = BYTE0(L_SE);
	data_buf[CNT++] = BYTE1(L_SE);
	data_buf[CNT++] = BYTE2(L_SE);
	data_buf[CNT++] = BYTE3(L_SE);
	data_buf[CNT++] = BYTE0(L_S_Tar);
	data_buf[CNT++] = BYTE1(L_S_Tar);
	data_buf[CNT++] = BYTE0(L_XE);
	data_buf[CNT++] = BYTE1(L_XE);
	data_buf[CNT++] = BYTE2(L_XE);
	data_buf[CNT++] = BYTE3(L_XE);
	data_buf[CNT++] = BYTE0(L_X_Tar);
	data_buf[CNT++] = BYTE1(L_X_Tar);
	data_buf[CNT++] = BYTE0(R_SE);
	data_buf[CNT++] = BYTE1(R_SE);
	data_buf[CNT++] = BYTE2(R_SE);
	data_buf[CNT++] = BYTE3(R_SE);
	data_buf[CNT++] = BYTE0(R_S_Tar);
	data_buf[CNT++] = BYTE1(R_S_Tar);
	data_buf[CNT++] = BYTE0(R_XE);
	data_buf[CNT++] = BYTE1(R_XE);
	data_buf[CNT++] = BYTE2(R_XE);
	data_buf[CNT++] = BYTE3(R_XE);
	data_buf[CNT++] = BYTE0(R_X_Tar); 
	data_buf[CNT++] = BYTE1(R_X_Tar); 
	for(uint8 i=0; i < (data_buf[3]+4); i++)
	{
		sumcheck += data_buf[i]; //从帧头开始，对每一字节进行求和，直到DATA区结束
		addcheck += sumcheck; 		//每一字节的求和操作，进行一次sumcheck的累加
	}
	data_buf[CNT++] = sumcheck;
	data_buf[CNT++] = addcheck;
	uart_putbuff(USART_1,data_buf,CNT);
}


/**************************************************************************
函数功能：匿名上位机通信协议函数，用于显示波形并且调参，显示位置编码值的波形
入口参数：上位机所需显示的曲线数据(四个轮编码值)，该函数在motol_start()中使用
返回  值：无
使用说明：该函数在motol_start中调用，并且在匿名上位机中打开查看波形，详细请看使用说明
**************************************************************************/
void NimingOutput_PosSped_pid(int32 L_SE,int32 L_XE,int32 R_SE,int32 R_XE)
{
	uint8 data_buf[40];
	uint8 CNT=0;
	uint8 sumcheck = 0;
	uint8 addcheck = 0;
	int32 L_S_Tar =  Tar_LS_RX;
	int32 L_X_Tar =  Tar_LX_RS;
	int32 R_S_Tar =  Tar_LX_RS;
	int32 R_X_Tar =  Tar_LS_RX;
	data_buf[CNT++] = 0xaa;
	data_buf[CNT++] = 0xff;
	data_buf[CNT++] = 0xf1;
	data_buf[CNT++] = 32;
	data_buf[CNT++] = BYTE0(L_SE);
	data_buf[CNT++] = BYTE1(L_SE);
	data_buf[CNT++] = BYTE2(L_SE);
	data_buf[CNT++] = BYTE3(L_SE);
	data_buf[CNT++] = BYTE0(L_S_Tar);
	data_buf[CNT++] = BYTE1(L_S_Tar);
	data_buf[CNT++] = BYTE2(L_S_Tar);
	data_buf[CNT++] = BYTE3(L_S_Tar);
	data_buf[CNT++] = BYTE0(L_XE);
	data_buf[CNT++] = BYTE1(L_XE);
	data_buf[CNT++] = BYTE2(L_XE);
	data_buf[CNT++] = BYTE3(L_XE);
	data_buf[CNT++] = BYTE0(L_X_Tar);
	data_buf[CNT++] = BYTE1(L_X_Tar);
	data_buf[CNT++] = BYTE2(L_X_Tar);
	data_buf[CNT++] = BYTE3(L_X_Tar);
	data_buf[CNT++] = BYTE0(R_SE);
	data_buf[CNT++] = BYTE1(R_SE);
	data_buf[CNT++] = BYTE2(R_SE);
	data_buf[CNT++] = BYTE3(R_SE);
	data_buf[CNT++] = BYTE0(R_S_Tar);
	data_buf[CNT++] = BYTE1(R_S_Tar);
	data_buf[CNT++] = BYTE2(R_S_Tar);
	data_buf[CNT++] = BYTE3(R_S_Tar);
	data_buf[CNT++] = BYTE0(R_XE);
	data_buf[CNT++] = BYTE1(R_XE);
	data_buf[CNT++] = BYTE2(R_XE);
	data_buf[CNT++] = BYTE3(R_XE);
	data_buf[CNT++] = BYTE0(R_X_Tar); 
	data_buf[CNT++] = BYTE1(R_X_Tar); 
	data_buf[CNT++] = BYTE2(R_X_Tar);
	data_buf[CNT++] = BYTE3(R_X_Tar);
	for(uint8 i=0; i < (data_buf[3]+4); i++)
	{
		sumcheck += data_buf[i]; //从帧头开始，对每一字节进行求和，直到DATA区结束
		addcheck += sumcheck; 		//每一字节的求和操作，进行一次sumcheck的累加
	}
	data_buf[CNT++] = sumcheck;
	data_buf[CNT++] = addcheck;
	uart_putbuff(USART_1,data_buf,CNT);
}


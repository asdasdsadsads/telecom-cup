#ifndef __JY61_H
#define __JY61_H

struct SAcc
{
	short a[3];
	short T;
};
struct SGyro
{
	short w[3];
	short T;
};
struct SAngle
{
	short Angle[3];
	short T;
};
struct SAcc 		stcAcc;
struct SGyro 		stcGyro;
struct SAngle 	stcAngle;
 
unsigned char YAWCMD[3] = {0XFF,0XAA,0X52};
unsigned char ACCCMD[3] = {0XFF,0XAA,0X67};
unsigned char SLEEPCMD[3] = {0XFF,0XAA,0X60};
unsigned char UARTMODECMD[3] = {0XFF,0XAA,0X61};
unsigned char IICMODECMD[3] = {0XFF,0XAA,0X62};
								
#endif

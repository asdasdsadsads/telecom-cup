#ifndef _LZC_ANGLE_h
#define _LZC_ANGLE_h

#include "headfile.h"


extern uint8_t stata;
extern uint8_t RX_BUF[50];
extern uint8_t data_buf[50];
extern  int16_t rpy[3];
extern float angle[3];


void ImuCalculate_Complementary();
void mpu6050_uart_init();
void ReadMPU6050(void);
void send_Instruction(void);
uint8_t CHeck(uint8_t *data);
void CopeSerial2Data(unsigned char ucData);
void printf_data();
void MPU6050_getdata();
void calibration();
void NimingOutput_Angle();
void NimingOutput_Acc();

#endif
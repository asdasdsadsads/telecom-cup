#include "lzc_angle.h"
#include <stdio.h>



float R = 0.98f;													//互补系数，越低跟随度越低，滤波效果越低
const float fRad2Deg = 57.295779513f;		 //弧度换算角度乘的系数（180/Π）
const float dt = 0.01f;									 //时间周期(此次我10ms在中断中调用一次该函数,在主函数初始化中设置)
float angle[3] = {0};											//0、1、2对应pitch、roll、yaw

/**************************************************************************
函数功能：将mpu6050读取的角加速度、角速度转化为欧拉角
入口参数：无
返回  值：无
使用说明：此函数在定时器1中断中使用，如需外界获取角度，请调用angle[i]
**************************************************************************/
void ImuCalculate_Complementary()//互补滤波算法
{  
		ins_update();
    static float angle_last[3]={0};
		float increase[3] = {0,0,0};
		char PitchStr[20],RollStr[20],YawStr[20];
		
		increase[0] = gyro_vector.x*dt;
		if(fabs(increase[0])<0.02) increase[0]=0;
    angle[0] = R*(angle_last[0]+increase[0]) 
              + (1-R)*fRad2Deg*atan(acc_vector.x/acc_vector.z);
    angle_last[0] = angle[0];										//处理pitch
    
		increase[1] = gyro_vector.y*dt;
		if(fabs(increase[1])<0.0) increase[1]=0;
		angle[1] = R*(angle_last[1]+increase[1])
              + (1-R)*fRad2Deg*atan(acc_vector.y/acc_vector.z);
    angle_last[1] = angle[1];											//处理roll
		
		increase[2] = gyro_vector.z*dt;
		if(fabs(increase[2])<0.02) increase[2]=0;
    angle[2] = angle_last[2]+increase[2];
    angle_last[2] = angle[2];											//处理yaw
		
#ifdef OLED_ANGLE
		sprintf(PitchStr,"pitch = %.2f",angle[0]);
		sprintf(RollStr,"roll = %.2f",angle[1]);
		sprintf(YawStr,"yaw = %.2f",angle[2]);
		oled_p8x16str(2,0,PitchStr);
		oled_p8x16str(2,2,RollStr);
		oled_p8x16str(2,4,YawStr);
#endif

//		NimingOutput_Angle();
}


/**************************************************************************
函数功能：匿名上位机的波形输出欧拉角
入口参数：无
返回  值：无
使用说明：使用匿名上位机v7，在主函数中调用此函数发送即可，详细请看使用说明
**************************************************************************/
void NimingOutput_Angle()
{
	uint8 data_buf[13];
	uint8 sumcheck = 0;
	uint8 addcheck = 0;
	int16 rol = angle[0]*100;
	int16 pit = angle[1]*100;
	int16 yaw = angle[2]*100;

	data_buf[0] = 0xaa;
	data_buf[1] = 0xff;
	data_buf[2] = 0x03;
	data_buf[3] = 7;
	data_buf[4] = BYTE0(rol);
	data_buf[5] = BYTE1(rol);
	data_buf[6] = BYTE0(pit);
	data_buf[7] = BYTE1(pit);
	data_buf[8] = BYTE0(yaw);
	data_buf[9] = BYTE1(yaw);
	data_buf[10] = 1;//状态位，随便给
	for(uint8 i=0; i < (data_buf[3]+4); i++)
	{
		sumcheck += data_buf[i]; //从帧头开始，对每一字节进行求和，直到DATA区结束
		addcheck += sumcheck; //每一字节的求和操作，进行一次sumcheck的累加
	}
	data_buf[11] = sumcheck;
	data_buf[12] = addcheck;
	uart_putbuff(USART_1,data_buf,13);
}


/**************************************************************************
函数功能：匿名上位机的波形输出角加速度
入口参数：无
返回  值：无
使用说明：使用匿名上位机v7，在主函数中调用此函数发送即可，详细请看使用说明
**************************************************************************/
void NimingOutput_Acc()
{
	uint8 data_buf[13];
	uint8 sumcheck = 0;
	uint8 addcheck = 0;
	uint8 CNT = 0;
	int16 acc_x = icm_acc_x;
	int16 acc_y = icm_acc_y;
	int16 acc_z = icm_acc_z;
	
	data_buf[CNT++] = 0xAA;
	data_buf[CNT++] = 0xFF;
	data_buf[CNT++] = 0xF2;
	data_buf[CNT++] = 6;
	data_buf[CNT++] = BYTE0(acc_x);
	data_buf[CNT++] = BYTE1(acc_x);
	data_buf[CNT++] = BYTE0(acc_y);
	data_buf[CNT++] = BYTE1(acc_y);
	data_buf[CNT++] = BYTE0(acc_z);
	data_buf[CNT++] = BYTE1(acc_z);
	for(uint8 i=0; i < (data_buf[3]+4); i++)
	{
		sumcheck += data_buf[i]; //从帧头开始，对每一字节进行求和，直到DATA区结束
		addcheck += sumcheck; //每一字节的求和操作，进行一次sumcheck的累加
	}
	data_buf[CNT++] = sumcheck;
	data_buf[CNT++] = addcheck;
	uart_putbuff(USART_1,data_buf,CNT);
}





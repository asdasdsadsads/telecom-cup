#ifndef _LZC_PID_h
#define _LZC_PID_h

#include "headfile.h"
typedef struct pid_handle{
float kp;
float ki;
float kd;
float Bias,Last_bias,Integral_bias;
int pwm;
int encoder;
}my_pid_handle;


typedef enum
{left_front_encoder,left_rear_encoder,right_front_encoder,right_rear_encoder}Encoder;//这个结构体记录的是四个编码值


int16 getEncoder(my_pid_handle *handle,Encoder whichencoder,uint8 choose);
void pid_reinit(my_pid_handle *handle);
void Encoder_reinit();
void PID_init(my_pid_handle *handle,float kp,float ki,float kd);
int Incremental_PI (my_pid_handle *handle,int Encoder,int Target);
int Position_PID (my_pid_handle *handle,int Encoder,int Target);
int f_Position_PID(my_pid_handle *handle,float Encoder,float Target);
int Incremental_PI_bias(my_pid_handle *handle,int bias);
int f_Incremental_PID(my_pid_handle *handle,float Encoder,float Target);
int f_Position_PID_bias(my_pid_handle *handle,float bias);
int Position_PID_bias (my_pid_handle *handle,int bias);

#endif

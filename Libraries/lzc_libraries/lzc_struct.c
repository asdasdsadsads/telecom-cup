#include "lzc_struct.h"

float invSqrt(float x)
{
    float xhalf = 0.5f * x;
    int i = *(int*)&x;          // get bits for floating value
    i =  0x5f375a86 - (i>>1);    // gives initial guess
    x = *(float*)&i;            // convert bits back to float
    x = x * (1.5f - xhalf*x*x); // Newton step
    return x;
}

float constrain_float(float amt, float low, float high) 
{
	return ((amt)<(low)?(low):((amt)>(high)?(high):(amt)));
}

int constrain_int(int amt, int low, int high) 
{
	// the check for NaN as a float prevents propogation of
	// floating point errors through any function that uses
	// constrain_float(). The normal float semantics already handle -Inf
	// and +Inf
	return ((amt)<(low)?(low):((amt)>(high)?(high):(amt)));
}

// constrain a int16_t value
int16_t constrain_int16(int16_t amt, int16_t low, int16_t high) 
{
	return ((amt)<(low)?(low):((amt)>(high)?(high):(amt)));
}

// constrain a int32_t value
int32_t constrain_int32(int32_t amt, int32_t low, int32_t high) 
{
	return ((amt)<(low)?(low):((amt)>(high)?(high):(amt)));
}


float fast_atan(float v)
{
    float v2 = v*v;
    return (v*(1.6867629106f + v2*0.4378497304f)/(1.6867633134f + v2));
}


#define FAST_ATAN2_PIBY2_FLOAT  1.5707963f
float fast_atan2(float y, float x)
{
        float atan;
        float z;
	if (x == 0.0f) {
       if (y > 0.0f) {
           return FAST_ATAN2_PIBY2_FLOAT;
       }
       if (y == 0.0f) {
           return 0.0f;
       }
       return -FAST_ATAN2_PIBY2_FLOAT;
   }

   z = y/x;
   if (fabs( z ) < 1.0f) {
       atan = z / (1.0f + 0.28f * z * z);
       if (x < 0.0f) {
           if (y < 0.0f) {
               return atan - PI;
           }
           return atan + PI;
       }
   } else {
       atan = FAST_ATAN2_PIBY2_FLOAT - (z / (z * z + 0.28f));
       if (y < 0.0f) {
           return atan - PI;
       }
   }
   return atan;
}


//防止溢出sqrt32函数
uint32_t sqrt_32_plus(int32_t a,int32_t b)
{
	uint32_t data=0;
	a = a/100; a=a*a;
	b = b/100; b=b*b;
	data = 100*sqrt(a+b);
	return data;
}

//两点之间距离计算
int distance_calculate(int a1, int a2, int b1, int b2)
{
    return sqrtf((a1 - a2) * (a1 - a2) + (b1 - b2) * (b1 - b2));
}

int distance_calculate_square(int a1, int a2, int b1, int b2)
{
    return (a1 - a2) * (a1 - a2) + (b1 - b2) * (b1 - b2);
}

//求两个数之间的最小值
int min2(int x, int y)
{
    if (x < y)
        return x;
    else
        return y;
}
#ifndef __LZC_NIMING_H
#define __LZC_NIMING_H
#include "headfile.h"

void NimingOutput_Angle();
void NimingOutput_Acc();
void NimingOutput_Speed(int32 L_SE,int32 L_XE,int32 R_SE,int32 R_XE);
void NimingOutput_Position(int32 L_SE,int32 L_XE,int32 R_SE,int32 R_XE);
void NimingOutput_AHRS();
void NimingOutput_AngleSped_pid(int32 L_SE,int32 L_XE,int32 R_SE,int32 R_XE,int angle_Speed);
void NimingOutput_AdjSped_pid(int32 L_SE,int32 L_XE,int32 R_SE,int32 R_XE,int tarls,int tarlx,int tarrs,int tarrx);
void NimingOutput_PosSped_pid(int32 L_SE,int32 L_XE,int32 R_SE,int32 R_XE);

#endif
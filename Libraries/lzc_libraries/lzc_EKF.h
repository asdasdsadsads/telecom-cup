#ifndef	 LZC_EKF_H
#define	 LZC_EKF_H

#include "headfile.h"


typedef struct
{
    float angle;         //卡尔曼输出角度
    float dt;            //两次卡尔曼滤波之间的时间间隔 单位s
    float Q_angle;       //陀螺仪角度噪声的协方差
    float Q_gyro;        //陀螺仪角速度漂移噪声的协方差
    float Q_bias;        //陀螺仪漂移
    float R_angle;       //加速度计角度噪声协方差
    float PP[2][2];      //协方差矩阵
       
}ekf_param_t;


typedef struct
{
    void (* init)(ekf_param_t *param, float dt, float Q, float R);
    float (* get_angle)(ekf_param_t *param, float acc_angle, float gyro);
}ekf_t;

typedef struct 
{
    float LastP;//上次估算协方差 初始化值为0.02
    float Now_P;//当前估算协方差 初始化值为0
    float out;//卡尔曼滤波器输出 初始化值为0
    float Kg;//卡尔曼增益 初始化值为0
    float Q;//过程噪声协方差 初始化值为0.001
    float R;//观测噪声协方差 初始化值为0.543
}KFP;//Kalman Filter parameter

int height;
int kalman_height=0;

extern ekf_t ekf;

#endif